/*
 * Copyright (c) 2019 Parameswaran Raman, Sriram Srinivasan, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#include <iostream>
using scalar=double;
using std::to_string;
using std::ofstream;
#include <tbb/tbb.h>
#include <tbb/compat/thread>
#include <chrono>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include "../Logger.hpp"
#include "DSMLRUpdater.hpp"
#include "DSMLRDataOption.hpp"
#include "PrimalParam.hpp"
#include "MulticlassDataLoader.hpp"

DSMLRDataOption* option_;
MetaData* metadata_;
Logger* logger_ = Logger::get_logger();


void calc_f1_measures(size_t *tp,
    size_t *fp,
    size_t *fn,
    scalar &micro_f1,
    scalar &macro_f1,
    size_t &nr_precision,
    size_t &dr_precision,
    size_t &dr_recall,
    size_t &num_labels){

  for(size_t k = 0; k < num_labels; ++k){
    nr_precision += tp[k];
    dr_precision += tp[k] + fp[k];
    dr_recall += tp[k] + fn[k];

    scalar p = 1.0;
    scalar r = 1.0;

    if(tp[k]+fp[k] > 0)
      p = (scalar)tp[k]/(tp[k] + fp[k]);
    if(tp[k]+fn[k] > 0)
      r = (scalar)tp[k]/(tp[k] + fn[k]);

    if(p or r)
      macro_f1 += (2*p*r)/(p+r);
  }
  macro_f1 /= num_labels;
  scalar p = (scalar) nr_precision / dr_precision;
  scalar r = (scalar) nr_precision / dr_recall;
  if(p or r)
    micro_f1 = (2 * p * r) / (p + r);
  logger_->info("train precision: " + to_string(p) 
      + ", train recall: " + to_string(r));
  return;
}



void compute_local_f1(size_t K,
    size_t num_parts,
    const vector<Instance> &data,
    const vector<ai_plus>  &a, scalar &micro_f1,
    scalar &macro_f1,
    size_t *local_buf){

  // Collect f1 stats
  //size_t local_buf[3*K];
  //std::fill(local_buf, local_buf + (3*K), SCALAR_ZERO);
  size_t *tp=&local_buf[0];
  size_t *fp=&local_buf[K];
  size_t *fn=&local_buf[2*K];

  for(auto &x: data){
    const size_t yi = x.label_;
    const size_t local_i = x.id_/num_parts;
    const size_t yhati = a[local_i].yhat_;

    if(yi == yhati) 
      tp[yhati]++;
    else {
      fp[yhati]++;
      fn[yi]++;
    }
  }

  size_t nr_precision = 0;
  size_t dr_precision = 0;
  size_t dr_recall = 0;

  calc_f1_measures(tp, fp, fn,
      micro_f1, macro_f1,
      nr_precision, dr_precision, dr_recall, K);

  return;
}

scalar compute_local_obj(MetaData*& metadata,
    Data*& data,
    vector<IParam*> w,
    vector<ai_plus> vec_ai){

  // Compute loss and objective function on train data 
  scalar loss = SCALAR_ZERO;
  size_t local_N = data->local_train_num_instances_;
  size_t N = metadata->global_train_num_instances_;
  size_t D = metadata->global_num_features_;

  for(size_t local_i = 0; local_i < local_N; ++local_i){
    const ai_plus ai = vec_ai[local_i];
    //logger_->info("<params> ai: " + to_string(ai.ai_));
    //std::cout << "in compute_local_obj, ai: " << ai.ai_ << std:: endl;
    loss += log(ai.ai_) + ai.max_fx_ - ai.fxy_;
  }
  loss /= N;

  scalar reg = SCALAR_ZERO;
  size_t local_K = w.size();
  //BUGBUG: do the below norm computation
  //more efficiently using std:: operations
  for (size_t k = 0; k < local_K; ++k) {
    for (size_t i = 0; i < D; ++i) {
      //reg +=  (w[k][i] * w[k][i]);
      reg +=  (((PrimalParam*) w[k])->get_param()[i]) * (((PrimalParam*) w[k])->get_param()[i]);
    }
  }
  reg *= 0.5*option_->lambda_;

  scalar obj = loss + reg;

  logger_->info("local loss: " + to_string(loss));
  logger_->info("local regularizer: " + to_string(reg));
  logger_->info("local obj = loss + regularizer: " + to_string(obj));

  return obj;
}

void compute_statistics(vector<IParam*> saved_columns, 
    DSMLRUpdater* updater, 
    scalar optimization_elapsed_secs, 
    size_t iter_num){
  std::cout << "inside dsmlrbody::compute_statistics() #1" << std::endl; 

  DSMLRDataOption &option = *option_;

  scalar train_micro_f1 = SCALAR_ZERO;
  scalar train_macro_f1 = SCALAR_ZERO;
  scalar test_micro_f1 = SCALAR_ZERO;
  scalar test_macro_f1 = SCALAR_ZERO;

  // compute local obj on each thread
  scalar local_obj = compute_local_obj(metadata_, updater->data_, saved_columns, updater->vec_ai_);

  // aggregate local obj per machine 
  scalar global_obj = local_obj;


  //Compute local train f1 stats on each thread
  const int K = metadata_->global_num_classes_;
  size_t *local_buf = new size_t[3*K];
  int i = 0;
  std::fill(local_buf, local_buf + (3*K), SCALAR_ZERO);
  compute_local_f1(K, updater->num_parts, (updater->data_)->local_train_instances_, updater->vec_ai_, train_micro_f1, train_macro_f1, &local_buf[i*3*K]);

  //Compute local test f1 stats on each thread
  std::fill(local_buf, local_buf + (3*K), SCALAR_ZERO);
  compute_local_f1(K, updater->num_parts, (updater->data_)->local_test_instances_, updater->vec_ai_test_, test_micro_f1, test_macro_f1, &local_buf[i*3*K]);

  // Print statistics 
  //const size_t optimization_elapsed_secs_ = -1;
  //const size_t iter_num = -1;
  logger_->info("grepthis,"
      + to_string(iter_num) + ","
      + to_string(global_obj) + ","
      + to_string(optimization_elapsed_secs) + ","
      + to_string(train_macro_f1) + ","
      + to_string(train_micro_f1) + ","
      + to_string(test_macro_f1) + ","
      + to_string(test_micro_f1)
      );


  //reset vec_ai before next iteration
  //also a place to test exact bi computation version for now
  size_t local_N = (updater->data_)->local_train_num_instances_;
  size_t local_Ntest = (updater->data_)->local_test_num_instances_;

  bool succeed = (updater->data_)->apply_to_train_block(
      [&](Instance & instance)->bool{
      const Instance &xi = instance;
      const size_t local_i = xi.id_/updater->num_parts;

      // Compute bi exactly once testing is done 
      //scalar &bi = (updater->local_params)[local_i];
      //bi = -log((updater->vec_ai_)[local_i].ai_) - (updater->vec_ai_)[local_i].max_fx_;

      (updater->vec_ai_)[local_i].reset(K);
      });


}



//==========
void compute_global_f1(size_t K, scalar &micro_f1,
    scalar &macro_f1,
    size_t *local_buf){

  size_t *tp = &local_buf[0];
  size_t *fp = &local_buf[K];
  size_t *fn = &local_buf[2*K];

  size_t nr_precision = 0;
  size_t dr_precision = 0;
  size_t dr_recall = 0;

  calc_f1_measures(tp, fp, fn,
      micro_f1, macro_f1,
      nr_precision, dr_precision, dr_recall, K);

}


void compute_local_f1(size_t K,
    size_t num_parts,
    const vector<Instance> &data,
    const vector<ai_plus>  &a,
    size_t *local_buf){

  // Collect f1 stats
  //size_t local_buf[3*K];
  //std::fill(local_buf, local_buf + (3*K), SCALAR_ZERO);
  size_t *tp=&local_buf[0];
  size_t *fp=&local_buf[K];
  size_t *fn=&local_buf[2*K];

  for(auto &x: data){
    const size_t yi = x.label_;
    const size_t local_i = x.id_/num_parts;
    const size_t yhati = a[local_i].yhat_;

    if(yi == yhati) 
      tp[yhati]++;
    else {
      fp[yhati]++;
      fn[yi]++;
    }
  }

  return;
}



void compute_statistics(vector<IParam*> saved_columns, 
    vector<DSMLRUpdater*> &updaters_, 
    scalar optimization_elapsed_secs, 
    size_t iter_num){
  std::cout << "inside dsmlrbody::compute_statistics() #2" << std::endl; 

  DSMLRDataOption &option = *option_;

  scalar train_micro_f1 = SCALAR_ZERO;
  scalar train_macro_f1 = SCALAR_ZERO;
  scalar test_micro_f1 = SCALAR_ZERO;
  scalar test_macro_f1 = SCALAR_ZERO;

  // compute local obj on each thread_id
  scalar local_obj[option_->num_threads_];
  for (size_t i = 0; i < option.num_threads_; i++) {
    DSMLRUpdater* updater = (DSMLRUpdater*) updaters_[i];
    local_obj[i] = compute_local_obj(metadata_, updater->data_, saved_columns, updater->vec_ai_);
    logger_->debug("local obj: " + to_string(local_obj[i]),i);
  }

  // aggregate local obj per machine 
  scalar machine_local_obj = std::accumulate(local_obj, local_obj + option.num_threads_, 0.0);

  // compute global obj 
  scalar global_obj = machine_local_obj;

  //Compute local train f1 stats on each thread
  const int K = metadata_->global_num_classes_;
  size_t *local_buf = new size_t[option.num_threads_*3*K];
  std::fill(local_buf, local_buf + (option.num_threads_*3*K), SCALAR_ZERO);
  for (size_t i = 0; i < option.num_threads_; i++) {
    DSMLRUpdater* updater = (DSMLRUpdater*) updaters_[i];
    compute_local_f1(K, updater->num_parts, (updater->data_)->local_train_instances_, updater->vec_ai_, &local_buf[i*3*K]);
  }

  // aggregate from each thread for each machine
  // colwise sum
  for (size_t j = 0; j < 3*K; j++){
    for (size_t i = 0; i < option.num_threads_; i++) {
      local_buf[j] += local_buf[i*3*K + j];
    }
  }

  //Compute global train f1 stats from the machines
  compute_global_f1(K, train_micro_f1, train_macro_f1, &local_buf[0]);

  //Compute local test f1 stats on each thread
  std::fill(local_buf, local_buf + (option.num_threads_*3*K), SCALAR_ZERO);
  for (size_t i = 0; i < option.num_threads_; i++) {
    DSMLRUpdater* updater = (DSMLRUpdater*) updaters_[i];
    compute_local_f1(K, updater->num_parts, (updater->data_)->local_test_instances_, updater->vec_ai_test_, &local_buf[i*3*K]);
  }

  // aggregate from each thread for each machine
  // colwise sum
  for (size_t j = 0; j < 3*K; j++){
    for (size_t i = 0; i < option.num_threads_; i++) {
      local_buf[j] += local_buf[i*3*K + j];
    }
  }

  //Compute global test f1 stats from the machines
  compute_global_f1(K, test_micro_f1, test_macro_f1, &local_buf[0]);

  // Print statistics 
  //const size_t optimization_elapsed_secs_ = -1;
  //const size_t iter_num = -1;
  logger_->info("grepthis,"
      + to_string(iter_num) + ","
      + to_string(global_obj) + ","
      + to_string(optimization_elapsed_secs) + ","
      + to_string(train_macro_f1) + ","
      + to_string(train_micro_f1) + ","
      + to_string(test_macro_f1) + ","
      + to_string(test_micro_f1)
      );


  //reset vec_ai before next iteration
  //also a place to test exact bi computation version for now
  for (size_t i = 0; i < option.num_threads_; i++) {
    DSMLRUpdater* updater = (DSMLRUpdater*) updaters_[i];
    size_t local_N = (updater->data_)->local_train_num_instances_;
    size_t local_Ntest = (updater->data_)->local_test_num_instances_;

    bool succeed = (updater->data_)->apply_to_train_block(
        [&](Instance & instance)->bool{
        const Instance &xi = instance;
        const size_t local_i = xi.id_/updater->num_parts;

        // Compute bi exactly once testing is done 
        //scalar &bi = (updater->local_params)[local_i];
        //bi = -log((updater->vec_ai_)[local_i].ai_) - (updater->vec_ai_)[local_i].max_fx_;

        (updater->vec_ai_)[local_i].reset(K);
        });
  }


}



//===================






tbb::atomic<bool> train_ready;
tbb::atomic<int> train_done;
tbb::atomic<int> test_done;
tbb::atomic<bool> test_ready;
tbb::atomic<int> num_pops;

void updater_fn(tbb::concurrent_queue<IParam*>* queues, int thread_id, DSMLRUpdater* updater_ptr){
  DSMLRUpdater &updater = *updater_ptr;
  int num_threads = option_->num_threads_;
  int num_iter = option_->timeouts_[0];
  updater.initialize(0,1,thread_id,num_threads);
  for (int i = 0; i < num_iter; i++) {
    if(queues[thread_id].unsafe_size() == 0){
      std::cout<<"error. queue empty.\n";
    }
    while(!train_ready){
      std::this_thread::yield();
    }
    //updater.iter_num_ = i;
    updater.prepare_update();
    int num_up = 0;
    while(train_ready && num_up < metadata_->global_num_classes_){
      IParam* p;
      if(!queues[thread_id].try_pop(p)){
        logger_->error("pop failed", thread_id);
        continue;
      }
      PrimalParam *pp = (PrimalParam*) p;
      logger_->debug("train:"+to_string(pp->get_col_index()), thread_id);
      updater.update(p);
      queues[(thread_id+1)%num_threads].push(p);
      num_pops++;
      num_up++;
    }

    train_done++;
    while(!test_ready){
      std::this_thread::yield();
    }

    updater.prepare_test();

    num_up = 0;
    while(test_ready && num_up < metadata_->global_num_classes_){
      IParam* p;
      if(!queues[thread_id].try_pop(p)){
        logger_->error("pop failed", thread_id);
        std::this_thread::yield();
        continue;
      }
      //  updater.test(p);
      logger_->debug("test:"+to_string(((PrimalParam*)p)->get_col_index()), thread_id);
      updater.test(p);
      queues[(thread_id+1)%num_threads].push(p);
      num_pops++;
      num_up++;
    }

    test_done++;
  }

}




int main(int argc, char **argv)
{

PrimalParam param(1348182);
const long long ml = sizeof(int) * 1 + param.get_bytes() * 100;
const int mi = sizeof(int) * 1 + param.get_bytes() * 100;
cout<<"Long: "<<ml<<", Int: "<<mi<<endl;

/*  train_ready= false;
  train_done = 0;
  test_done = 0;
  test_ready =false;
  num_pops = 0;

  option_ = new DSMLRDataOption();
  if (option_->parse_command(argc, argv) == false) {
    cout<<"ERROR: problem parsing command."<<endl;
    exit(2);
  }
  if(option_->debug_){
    logger_->enable_debug();
  }
  metadata_ = new MetaData();
  MulticlassDataLoader* loader_ = new MulticlassDataLoader();
  loader_->read_meta_data(metadata_, option_->train_file_name_, option_->test_file_name_, option_->meta_file_name_);
  int seed = option_->seed_ + 1 * 131 + 139;
  PrimalParamGenerator* v_p_gen_ = new PrimalParamGenerator(metadata_->global_num_features_, seed);
  tbb::concurrent_queue<IParam*>* queues = new tbb::concurrent_queue<IParam*>[option_->num_threads_];
  for (int i = 0; i < metadata_->global_num_classes_; i++) {
    queues[i%option_->num_threads_].push(v_p_gen_->generate_parameter(i));
  }

  vector<DSMLRUpdater*> updaters(option_->num_threads_);
  for (int i = 0; i < option_->num_threads_; i++) {
    updaters[i] = new DSMLRUpdater(option_, metadata_, loader_);
  }
  std::thread *updater_threads = new std::thread[option_->num_threads_];
  for (int i = 0; i < option_->num_threads_; i++) {
    updater_threads[i] = std::thread(updater_fn, queues, i, updaters[i]);
  }

  scalar optimization_elapsed_secs = 0.0;
  for (int iter_num = 0; iter_num < option_->timeouts_[0]; iter_num++) {
    using std::chrono::duration_cast;
    using std::chrono::milliseconds;
    using std::chrono::steady_clock;
    steady_clock::time_point optimization_begin = steady_clock::now();

    // pass to update wk 
    train_ready = true;
    while(num_pops < metadata_->global_num_classes_*option_->num_threads_){
      std::this_thread::yield();
      //std::this_thread::sleep_for(std::chrono::milliseconds(5000));
    }
    train_ready = false;
    while(train_done < option_->num_threads_){
      std::this_thread::yield();
    }
    logger_->debug("Pops:"+to_string(num_pops), -1);
    train_done = 0;
    num_pops = 0;
    test_ready = true;
    while(num_pops < metadata_->global_num_classes_*option_->num_threads_){
      std::this_thread::yield();
    }
    test_ready = false;
    while(test_done < option_->num_threads_){
      std::this_thread::yield();
    }
    num_pops = 0;
    test_done = 0;
    vector<IParam*> params;
    for(int i = 0 ; i < option_->num_threads_ ; i++){
      IParam* p;
      while(queues[i].try_pop(p)){
        params.push_back(p);
      }
    }

    steady_clock::time_point optimization_end = steady_clock::now();
    optimization_elapsed_secs += 
      static_cast<scalar>
      (duration_cast<milliseconds>(optimization_end - optimization_begin).count())
      / 1000;


    compute_statistics(params, updaters, optimization_elapsed_secs, iter_num);
    int j = 0;
    for(IParam* p : params){
      queues[j].push(p);
      j = (j+1)%option_->num_threads_;
    }

  }

  for (int i = 0; i < option_->num_threads_; i++) {
    updater_threads[i].join();
  }

*/
  /*
     updater.initialize(0,1,0,1);
     for (int i = 0; i < option_->timeouts_[0]; i++) {
     updater.iter_num_ = i;

/*if(i > 0){
// pass to compute vec_ai 
for(IParam* p : params){
updater.prepare_test();
PrimalParam *pp = (PrimalParam*) p;
const size_t k = pp->get_col_index();
scalar* wk = pp->get_param();

bool succeed =  (updater.data_)->apply_to_train_block(
[&](Instance & instance)->bool{

const Instance &xi = instance;
//const size_t local_i = x.id_/num_parts;
const size_t local_i = xi.id_;

// update vec_ai (used during training) 
updater.compute_local_ai(instance, updater.vec_ai_[local_i], params);

return true;
});
}
// pass to update bi
for(size_t local_i = 0; local_i < (updater.data_)->local_train_num_instances_; ++local_i){
scalar &bi = updater.local_params[local_i];
bi = -log(updater.vec_ai_[local_i].ai_) - updater.vec_ai_[local_i].max_fx_;
std::cout << "bi for i: " << local_i << " = " << bi << std::endl;
}
}* /

/*for(IParam* p : params){
PrimalParam *pp = (PrimalParam*) p;
pp->print(); 
}* /

updater.prepare_test();

// pass to update wk 
for(IParam* p : params){
PrimalParam *pp = (PrimalParam*) p;
pp->print();
updater.update(p);
pp->print();
}

//bi update happens here
for(size_t local_i = 0; local_i < (updater.data_)->local_train_num_instances_; ++local_i){
scalar &bi = updater.local_params[local_i];
bi = -log(updater.vec_ai_[local_i].ai_) - updater.vec_ai_[local_i].max_fx_;
std::cout << "bi for i: " << local_i << " = " << bi << ", iter: " << i << std::endl;
}


updater.prepare_test();

for(IParam* p : params){
std::cout << "I am here\n";
//  updater.test(p);
updater.testtest(p);
}
compute_statistics(params, &updater);
}
*/
return 0;
}
