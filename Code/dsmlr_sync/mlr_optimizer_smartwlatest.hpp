/*
 * Copyright (c) 2019 Parameswaran Raman, Shin Matsushima, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef DSMLR_MLR_OPTIMIZER_SMARTWLATEST_HPP_
#define DSMLR_MLR_OPTIMIZER_SMARTWLATEST_HPP_

//#include "mlr.hpp"

#include "optimizer.hpp"
#include "data.hpp"
#include <string>

namespace dsmlr
{

class MlrOptimizerSmartWlatest :public Optimizer
{

 public:
  MlrOptimizerSmartWlatest(Option& option,
                        Data& data) :
    Optimizer(option, data)
{ }

    virtual ~MlrOptimizerSmartWlatest()
  { }

#if 0
 protected:

    vector<scalar> train_predictions_;
    vector<scalar> test_predictions_;
    vector<scalar> partialsum_;

    vector<double> train_predictions_sumexp_;
    vector<double> temporal_predictions_buffer_;
    bool have_test_;

    const size_t PARAM_BYTENUM = sizeof(scalar) * 3;
    const size_t CHUNK_SIZE = PARAM_BYTENUM * option_.chunk_factor_;

    double threshold = option_.threshold_;
    double expthreshold = exp(threshold);

    scalar* getparam_val(vector<char>& buf, size_t index) {
        return reinterpret_cast<scalar *>(&buf[index*PARAM_BYTENUM]);
    }

    scalar* getparam_valprevitr(vector<char>& buf, size_t index) {
        return reinterpret_cast<scalar *>(&buf[index*PARAM_BYTENUM + sizeof(scalar)]);
    }

    scalar* getparam_gradprevitr(vector<char>& buf, size_t index) {
        return reinterpret_cast<scalar *>(&buf[index*PARAM_BYTENUM + 2*sizeof(scalar)]);
    }

    inline void my_approx(double x, double &f, double &g)
    {
        if(iter_num_ != 1){
            threshold = 10000;
            expthreshold = exp(threshold);
            //print_log(format("iter_num: %d threshold: %d expthreshold: %f") % iter_num_ % threshold % expthreshold);
        }
        
        if(x < threshold)
        {
          f = exp(x);
          g = f;
        }
        else
        {
           f = (x-threshold)*expthreshold + expthreshold;
           g = expthreshold;
        }
    }



    virtual bool init_local() {
        size_t total_buffer_size;
        auto& env = Environment::get_instance();
        const int numtasks = env.get_numtasks();

        total_buffer_size = num_per_task(data_.get_global_num_classes(), numtasks)*data_.get_global_num_features()*PARAM_BYTENUM;
        // Extend it so that buffersize can be divided by CHUNK_SIZE
        total_buffer_size = (1 + total_buffer_size / CHUNK_SIZE) * CHUNK_SIZE;
        msg_primal_buf_.resize(total_buffer_size);
        msg_recv_buf_.resize(CHUNK_SIZE);

        char *msg_ptr = &(msg_primal_buf_.front());
        scalar *scalar_ptr = reinterpret_cast<scalar *>(msg_ptr);
        for (size_t i=0; i< data_.get_local_num_classes() *data_.get_global_num_features(); ++i) {
            *scalar_ptr = 0.0;
            scalar_ptr++;
            *scalar_ptr = 0.0;
            scalar_ptr++;
            *scalar_ptr = 0.0;
            scalar_ptr++;
        }

        msg_dual_buf_.resize(data_.get_local_train_num_instances()*PARAM_BYTENUM);
        msg_ptr = &(msg_dual_buf_.front());
        scalar_ptr = reinterpret_cast<scalar *>(msg_ptr);
        for (size_t i=0; i< data_.get_local_train_num_instances(); ++i) {
            *scalar_ptr = log(1.0/data_.get_global_num_classes());
            scalar_ptr++;
            *scalar_ptr = 0.0;
            scalar_ptr++;
            *scalar_ptr = 0.0;
            scalar_ptr++;
        }

        assert(data_.get_local_train_num_instances() > 0);

        partialsum_.assign(data_.get_local_train_num_instances(),0.0);

        // assign memory needed for testing
        train_predictions_.assign(data_.get_local_train_num_instances()
                                  *data_.get_global_num_classes(), 0.0);
        test_predictions_.assign(data_.get_local_test_num_instances()
                                 *data_.get_global_num_classes(), 0.0);

        train_predictions_sumexp_.assign(data_.get_local_train_num_instances(),0.0);

        temporal_predictions_buffer_.assign(data_.get_local_num_classes()*2 ,0.0);

        if(data_.get_local_test_num_instances() == 0)
            have_test_=false;
        else
            have_test_=true;

        return true;
    }

    virtual bool prepare_optimization() {
        scalar value = 0.0;
        auto& env = Environment::get_instance();
        const int numtasks = env.get_numtasks();

        for(int i=0; i<data_.get_local_train_num_instances(); i++) {
            //if(iter_num_ > 1) {
            if(iter_num_ > 3) {
                scalar &bi= *(getparam_val(msg_dual_buf_, i));
                bi = - log( partialsum_[i] );
            }
            partialsum_[i] = 0.0;
        }
        
        if(iter_num_%option_.proj_period_==0 && option_.proj_==true) {          
          train_predictions_sumexp_.assign(data_.get_local_train_num_instances(),0.0);
        }

        if(iter_num_%option_.svrg_period_==0 && option_.svrg_==true) {

            //Prepare primal params
            char *msg_ptr = &(msg_primal_buf_.front());
            scalar *scalar_ptr = reinterpret_cast<scalar *>(msg_ptr);
            for (size_t i=0; i< num_per_task(data_.get_global_num_classes(), numtasks)*data_.get_global_num_features(); ++i) {
                value = *scalar_ptr;
                scalar_ptr++;
                *scalar_ptr = value;        scalar_ptr++;
                *scalar_ptr = 0.0;
                scalar_ptr++;
            }

            //Prepare dual params
            msg_ptr = &(msg_dual_buf_.front());
            scalar_ptr = reinterpret_cast<scalar *>(msg_ptr);
            for (size_t i=0; i< data_.get_local_train_num_instances(); ++i) {
                value = *scalar_ptr;
                scalar_ptr++;
                *scalar_ptr = value;
                scalar_ptr++;
                *scalar_ptr = 0.0;
                scalar_ptr++;
            }
        }
        return true;
    }

    virtual bool communicate(int recv_from, int send_to) {

        auto& env = Environment::get_instance();
        const int numtasks = env.get_numtasks();
        size_t total_buffer_size = num_per_task(data_.get_global_num_classes(), numtasks)*data_.get_global_num_features()*PARAM_BYTENUM;
        // Extend it so that buffersize can be divided by CHUNK_SIZE
        total_buffer_size = (1 + total_buffer_size / CHUNK_SIZE) * CHUNK_SIZE;
        size_t numsplits = total_buffer_size/CHUNK_SIZE;

        //Communicate
        {
            for(size_t j=0; j<numsplits; ++j) {
                MPI_Request req;
                MPI_Status stat;
                MPI_Isend(&msg_primal_buf_[CHUNK_SIZE*j], CHUNK_SIZE, MPI_CHAR,
                          send_to, 0,
                          MPI_COMM_WORLD, &req);
                MPI_Recv(&msg_recv_buf_[0], CHUNK_SIZE, MPI_CHAR,
                         recv_from, 0,
                         MPI_COMM_WORLD, &stat);
                MPI_Wait(&req, &stat);
                memcpy(&msg_primal_buf_[CHUNK_SIZE*j], &msg_recv_buf_[0], CHUNK_SIZE);

            }
        }

        return true;
    }

    virtual bool process_block(size_t block_index, size_t inner_iter_num) {

        // learning rates
        scalar lrate = option_.primal_rate_;
        if(option_.decay_) {
            // iter num has to be discounted for iterations for projection and as it begins with 1.
            // also we want to be exact the same as sgd-mlr, eta_t = eta/max(1, sqrt(t)), meaning that eta0 = eta1 = eta
            //
            // calculate revised iter_num_ (which updates only at t=1, 3, 5, .. instead of t=1,2,3,4, .. This is to make
            // it match the 0-delay older implementation.
            size_t revised_iter_num = iter_num_ + (iter_num_-1);
            //lrate /= sqrt(std::max(1.0, static_cast<double>(iter_num_ - iter_num_ / option_.proj_period_ ) - 1.0 ));
            lrate /= sqrt(std::max(1.0, static_cast<double>(revised_iter_num - revised_iter_num / option_.proj_period_ ) - 1.0 ));
        }
        // necessary constants
        const scalar lambda = option_.reg_param_;
        const scalar n = data_.get_global_train_num_instances();
        const size_t D=data_.get_global_num_features();
        const size_t K=data_.get_global_num_classes();
        const size_t local_num_classes=data_.get_local_num_classes();
        vector<size_t> &nnz_col = *data_.get_nnz_col();
        double *xTw = &temporal_predictions_buffer_[0];
        double *xTwtilde = &temporal_predictions_buffer_[0] + local_num_classes;

        auto& env = Environment::get_instance();
        const int numtasks = env.get_numtasks();
        bool succeed = false;

        //do exact A computation in first iteration (= iter 2 because in exact A scheme we update W and update A in 
        //alternate iterations)
        if(iter_num_ == 2){
          succeed =  data_.apply_to_refresh_block(block_index,
                                                [&](Instance & instance)->bool {

            /////// Here we do:
            // 1.  Given an Instance x, take inner products <wk, x> where local_min_label <= k <= local_max_label
            // 2.  Calculate stochastic gradients
            //      stoch_grad_w = (yik !=k) ?  lambda/N w + exp(wkxi + logai) / N  xi
            //                               :  lambda/N w + (exp(wkxi + logai) / N - 1/N )xi
            //      stoch_grad_b =  (exp(wkxi + logai) / N - 1/(NK) )
            // 3.  Update b[i] and w[k] where local_min_label <= k <= local_max_label
            //      w[k] -= eta * stoch_grad_w * x
            //      b[i] -= eta * stoch_grad_b

            const Instance &xi=instance;
            const label_t yi=xi.label_;
            const size_t i=xi.id_;
            const size_t local_i = i / numtasks;
            double expf = 0.0, expg = 0.0;

            scalar &bi= *(getparam_val(msg_dual_buf_, local_i));
            scalar &btildei= *(getparam_valprevitr(msg_dual_buf_, local_i));
            scalar &grad_btildei= *(getparam_gradprevitr(msg_dual_buf_, local_i));

            vector<char> &w=msg_primal_buf_;
            const size_t min_label = local_num_classes * block_index;
            const size_t upbd_label = std::min(local_num_classes * (block_index+1), K) ;

            // 1.  Compute inner products <wk, x>, <wtildek, x> and new <wtildek,x>
            //     where local_min_label <= k <= local_max_label
            std::fill(xTw, xTw+local_num_classes, 0.0);
            if(option_.svrg_==true) 
              std::fill(xTwtilde, xTwtilde+local_num_classes, 0.0);
            // std::fill(xTw.begin(), xTw.begin()+local_num_classes, SCALAR_ZERO);
            // std::fill(xTwtilde.begin(), xTwtilde.begin()+local_num_classes, SCALAR_ZERO);
            for(size_t it=0; it< xi.idx_.size(); ++it)
            {
              const size_t j = xi.idx_[it];
              const scalar xij = xi.val_[it];
              for(size_t k=min_label; k<upbd_label; ++k) {
                const size_t local_k = k-min_label;
                const scalar wkj = *(getparam_val(w, local_k*D+j));
                const scalar wtildekj = *(getparam_valprevitr(w, local_k*D+j));
                    xTw[local_k] += wkj * xij;
                    if(option_.svrg_==true) 
                      xTwtilde[local_k] += wtildekj * xij;
              }
            }
            
            for(size_t k=min_label; k<upbd_label; ++k) {
              const size_t local_k = k-min_label;
              train_predictions_sumexp_[local_i] += exp(xTw[local_k]);
              //my_approx(xTw[local_k], expf, expg);
              //train_predictions_sumexp_[local_i] += expf;
            }
            bi = - log( train_predictions_sumexp_[local_i] );
            
            
            if(option_.svrg_==true) {
              for(size_t k=min_label; k<upbd_label; ++k)
              {              
                const size_t local_k = k-min_label;
                scalar sg_w = (yi!=k) ? 0.0 : -1.0/n ;
                scalar sg_wtilde = (yi!=k) ? 0.0 : -1.0/n ;
                my_approx(xTw[local_k] +bi, expf, expg);
                sg_w += expg/n;
                my_approx(xTwtilde[local_k] +btildei, expf, expg);
                sg_wtilde += expg/n;
                  
                for(size_t it=0; it<xi.idx_.size(); ++it) {
                  const size_t j = xi.idx_[it];
                  const scalar xij = xi.val_[it];
                  const scalar omega_j = nnz_col[j];
                  scalar &wkj = *(getparam_val(w, local_k*D+j));
                  const scalar wtildekj = *(getparam_valprevitr(w, local_k*D+j));
                  scalar &grad_wtildekj = *(getparam_gradprevitr(w, local_k*D+j));
                  grad_wtildekj += sg_wtilde * xij;
                }
              }
              for(size_t k=min_label; k<upbd_label; ++k){
                const size_t local_k = k-min_label;
                //  scalar sg_bi = exp(xTw[local_k]+bi)/n ;
                my_approx(xTw[local_k]+bi, expf, expg);
                scalar sg_bi = expg/n;
                
              //  scalar sg_btildei = exp(xTwtilde[local_k]+btildei)/n;
                my_approx(xTwtilde[local_k]+btildei, expf, expg);
                scalar sg_btildei = expg/n;                
                grad_btildei += sg_btildei;
              }
            }
            return true;

        });
        }
        else{
        
//        if(iter_num_ == 1)
//            option_.repeat_block_ = 5;
//        else
//            option_.repeat_block_ = 1;     
       
        if(iter_num_ != 1)
            option_.repeat_block_ = 1;
        

        for(size_t repeat_itr=0; repeat_itr < option_.repeat_block_; ++repeat_itr) {
            /////// Here we do:
            // 1.  Given an Instance x, take inner products <wk, x> where local_min_label <= k <= local_max_label
            // 2.  Calculate stochastic gradients
            //      stoch_grad_w = (yik !=k) ?  lambda/N w + exp(wkxi + logai) / N  xi
            //                               :  lambda/N w + (exp(wkxi + logai) / N - 1/N )xi
            //      stoch_grad_b =  (exp(wkxi + logai) / N - 1/(NK) )
            // 3.  Update b[i] and w[k] where local_min_label <= k <= local_max_label
            //      w[k] -= eta * stoch_grad_w * x
            //      b[i] -= eta * stoch_grad_b


            vector<char> &w=msg_primal_buf_;
            const size_t min_label = local_num_classes * block_index;
            const size_t upbd_label = std::min(local_num_classes * (block_index+1), K) ;

            // 1.  Compute inner products <wk, x>, <wtildek, x> and new <wtildek,x>
            //     where local_min_label <= k <= local_max_label
            std::fill(xTw, xTw+local_num_classes, 0.0);
            if(option_.svrg_==true)
                std::fill(xTwtilde, xTwtilde+local_num_classes, 0.0);
            // std::fill(xTw.begin(), xTw.begin()+local_num_classes, SCALAR_ZERO);
            // std::fill(xTwtilde.begin(), xTwtilde.begin()+local_num_classes, SCALAR_ZERO);
            data_.shuffle_train_instances_order();
            for(size_t k=min_label; k<upbd_label; ++k) {
                succeed =  data_.apply_to_train_block(block_index,
                [&](Instance & instance)->bool {

                    const Instance &xi=instance;
                    const label_t yi=xi.label_;
                    const size_t i=xi.id_;
                    const size_t local_i = i / numtasks;

                    scalar &bi= *(getparam_val(msg_dual_buf_, local_i));
                    scalar &btildei= *(getparam_valprevitr(msg_dual_buf_, local_i));
                    scalar &grad_btildei= *(getparam_gradprevitr(msg_dual_buf_, local_i));

                    const size_t local_k = k-min_label;
                    xTw[local_k] =0.0;
                    if(option_.svrg_==true)
                        xTwtilde[local_k] =0.0;

                    for(size_t it=0; it< xi.idx_.size(); ++it) {
                        const size_t j = xi.idx_[it];
                        const scalar xij = xi.val_[it];
                        const scalar wkj = *(getparam_val(w, local_k*D+j));
                        const scalar wtildekj = *(getparam_valprevitr(w, local_k*D+j));
                        xTw[local_k] += wkj * xij;
                        if(option_.svrg_==true)
                            xTwtilde[local_k] += wtildekj * xij;
                    }
                    scalar sg_w = (yi!=k) ? 0.0 : -1.0/n ;
                    scalar sg_wtilde = (yi!=k) ? 0.0 : -1.0/n ;

                    //compute running partialsum of exp(wkxi) using latest wk
                    double tmpf = 0.0, tmpg = 0.0;
                    my_approx(xTw[local_k], tmpf, tmpg);
                    partialsum_[local_i] += tmpf;

                    double expf = 0.0, expg = 0.0;
                    my_approx(bi, expf, expg);
                    expf *= tmpf;
                    sg_w += (expg*tmpg)/n;

                    if(option_.svrg_==true) {
                        my_approx(xTwtilde[local_k] +btildei, expf, expg);
                        sg_wtilde += expg/n;
                    }

                    for(size_t it=0; it<xi.idx_.size(); ++it) {
                        const size_t j = xi.idx_[it];
                        const scalar xij = xi.val_[it];
                        const scalar omega_j = nnz_col[j];
                        scalar &wkj = *(getparam_val(w, local_k*D+j));
                        const scalar wtildekj = *(getparam_valprevitr(w, local_k*D+j));
                        scalar &grad_wtildekj = *(getparam_gradprevitr(w, local_k*D+j));
                        if(option_.svrg_==true) {
                            wkj -= lrate * (lambda * wkj/omega_j + (sg_w - sg_wtilde ) * xij + grad_wtildekj/omega_j ) ;
                        } else {
                            double delta = (lambda * wkj/omega_j + sg_w * xij ) ;
                            wkj -= lrate * (lambda * wkj/omega_j + sg_w * xij ) ;
                        }
                    }

                    return true;

                });
            }

        }
        }
        return succeed;
    }

    virtual bool prepare_test() {

        // initialize all predictions to be zero
        std::fill(train_predictions_.begin(), train_predictions_.end(),
                  SCALAR_ZERO);

        std::fill(test_predictions_.begin(), test_predictions_.end(),
                  SCALAR_ZERO);

        return true;

    }

    virtual bool test_on_block(size_t block_index) {
        //const scalar global_num_instances = data_.get_global_train_num_instances();
        const size_t global_num_features = data_.get_global_num_features();
        const size_t global_num_classes = data_.get_global_num_classes();
        const size_t local_num_classes = data_.get_local_num_classes();

        auto& env = Environment::get_instance();
        const int numtasks = env.get_numtasks();

        // on training data
        data_.apply_to_refresh_block(block_index,
        [&](Instance & instance)->bool {
            /////// Here we do:
            // 1.  Given an Instance xi, take inner products <wk, xi> where local_min_label <= k <= local_max_label
            // 2.  Fill train_predictions_[local_i*K + k]

            size_t D=global_num_features;
            size_t K=global_num_classes;
            Instance &xi = instance;
            size_t i=xi.id_;
            size_t local_i = i /numtasks;
            size_t min_label = local_num_classes * block_index;
            size_t upbd_label = std::min(local_num_classes * (block_index+1), global_num_classes) ;
            vector<char> &w=msg_primal_buf_;

            for(size_t it=0; it< xi.idx_.size(); ++it) {
                const size_t j = xi.idx_[it];
                const scalar xij = xi.val_[it];
                for(size_t k=min_label; k<upbd_label; ++k) {
                    const size_t local_k = k-min_label;
                    //train_predictions_[local_i*K+k] += w[local_k*D+j].value * xij;
                    train_predictions_[local_i*K+k] += *(getparam_val(w, local_k*D+j)) * xij;
                }
            }

            return true;
        });

        // on test data

        data_.apply_to_test_block(block_index,
        [&](Instance & instance)->bool {
            size_t D=global_num_features;
            size_t K=global_num_classes;
            Instance &xi = instance;
            size_t i=xi.id_;
            size_t local_i = i /numtasks;
            size_t min_label = local_num_classes * block_index;
            size_t upbd_label = std::min(local_num_classes * (block_index+1), global_num_classes) ;
            vector<char> &w=msg_primal_buf_;

            for(size_t it=0; it< xi.idx_.size(); ++it) {
                const size_t j = xi.idx_[it];
                const scalar xij = xi.val_[it];
                for(size_t k=min_label; k<upbd_label; ++k) {
                    const size_t local_k = k-min_label;
                    //test_predictions_[local_i*K+k] += w[local_k*D+j].value * xij;
                    test_predictions_[local_i*K+k] += *(getparam_val(w, local_k*D+j)) * xij;
                }
            }

            return true;
        });

        return true;

    }


    virtual bool summarize_test(size_t iter_num) {
        //print_log(format("summarize_test, threshold: %f") % threshold );

        // Here we calculate
        //   0.5 lambda sum_k ||w||^2
        // + 1/N sum_i sum_k - yik wkTxi + ai*exp(wkTxi) -logai -1)

        // and
        //   0.5 lambda sum_k ||w||^2
        // + 1/N sum_i - sum_k yik wkTxi + log sum_k exp(wkTxi)

        // by here each process should have stored the value of wkTxi in
        // train_predictions_ for all k with respect to local xis.

        // local_loss = 1/N [sum_{i \in my process} (sum_{k for all} - yik wkTxi + ai*exp(wkTxi) ) -logai -1 ]
        // local_loss_primal_dual = 1/N [sum_{i \in my process} (sum_{k for all} - yik wkTxi + ai*exp(wkTxi) ) -logai -1 ]
        // local_regularizer = 0.5 lambda sum_{k \in my process} ||w||^2

        // local_obj = local_loss + local_regularizer
        // global_obj = sum_{each process} local_obj

        auto& env = Environment::get_instance();
        const int numtasks = env.get_numtasks();

        const scalar reg_param = option_.reg_param_;
        const size_t global_train_num_instances = data_.get_global_train_num_instances();

        double local_loss = 0.0;
        double local_loss_primal_dual = 0.0;
        double local_regularizer = 0.0;
        double local_primal_dual_obj = 0.0;
        double local_primal_obj = 0.0;
        double expf = 0.0, expg = 0.0;

        const size_t K= data_.get_global_num_classes();
        // to calulate f1 measures
        size_t local_buf[3*K];
        std::fill(local_buf, local_buf+(3*K), 0);
        size_t *tp=&local_buf[0];
        size_t *fp=&local_buf[K];
        size_t *fn=&local_buf[2*K];

for(auto &x: data_.local_train_instances_) {
            const size_t yi = x.label_;
            const size_t local_i= x.id_ / numtasks ;
            const scalar logai = *(getparam_val(msg_dual_buf_, local_i));
            //          const scalar label_score = train_predictions_[local_i * K + yi];
            scalar max_score = train_predictions_[local_i * K + 0];
            size_t hatyi = 0;

            for(size_t k=0; k < K; ++k) {
                scalar wkTxi = train_predictions_[local_i * K + k];
                if(wkTxi > max_score) {
                    max_score = wkTxi;
                    hatyi=k;
                }
            }
            if(yi == hatyi) {
                tp[hatyi]++;
            } else {
                fp[hatyi]++;
                fn[yi]++;
            }

            scalar sumexp_wkTxi_minus_max_score = SCALAR_ZERO;
            for(size_t k=0; k < K; ++k) {
                scalar wkTxi = train_predictions_[local_i * K + k];
                //sumexp_wkTxi_minus_max_score += exp(wkTxi-max_score) ;
                my_approx(wkTxi-max_score, expf, expg);
                sumexp_wkTxi_minus_max_score += expf ;
                if(yi == k) {
                    local_loss += -wkTxi ;
                    //local_loss_primal_dual += -wkTxi + exp(wkTxi+logai) - logai - 1;
                    my_approx(wkTxi+logai, expf, expg);
                    local_loss_primal_dual += -wkTxi + expf - logai - 1;
                } else {
                    //local_loss_primal_dual += exp(wkTxi+logai);
                    my_approx(wkTxi+logai, expf, expg);
                    local_loss_primal_dual += expf;
                }
            }
            local_loss += log(sumexp_wkTxi_minus_max_score) + max_score ;
        }
        local_loss /= global_train_num_instances;
        local_loss_primal_dual /= global_train_num_instances;

        // calculate regularizer
        for (size_t i=0; i<num_per_task(data_.get_global_num_classes(), numtasks)*data_.get_global_num_features(); ++i) {
            local_regularizer += *(getparam_val(msg_primal_buf_, i)) * *(getparam_val(msg_primal_buf_, i));
        }
        local_regularizer *= 0.5*reg_param;
        local_primal_obj = local_loss + local_regularizer;
        local_primal_dual_obj = local_loss_primal_dual + local_regularizer;


        print_log(format("local loss (primaldual vs primal): %f > %f")
                  % local_loss_primal_dual % local_loss);
        print_log(format("local reg: %f") % local_regularizer );
        print_log(format("local obj (primal-dual vs primal): %f > %f" )
                  % local_primal_dual_obj % local_primal_obj);

        scalar train_micro_f1 = SCALAR_ZERO;
        scalar train_macro_f1 = SCALAR_ZERO;
        size_t train_nr_precision = 0;
        size_t train_dr_precision = 0;
        size_t train_dr_recall = 0;

        size_t global_buf[3*K];
        std::fill(global_buf, global_buf+(3*K), 0);
        MPI_Allreduce(&local_buf[0], &global_buf[0], 3*K,
                      MPI_INT, MPI_SUM, MPI_COMM_WORLD);

        tp=&global_buf[0];
        fp=&global_buf[K];
        fn=&global_buf[2*K];

        calc_f1_measures(tp, fp, fn,
                         train_micro_f1, train_macro_f1,
                         train_nr_precision, train_dr_precision, train_dr_recall);

        master_log(format("train correct classification: %d (/ %d)")
                   % train_nr_precision % global_train_num_instances);
        double global_primal_obj = 0.0;
        double global_primal_dual_obj = 0.0;

        MPI_Allreduce(&local_primal_obj, &global_primal_obj, 1,
                      MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Allreduce(&local_primal_dual_obj, &global_primal_dual_obj, 1,
                      MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

        // MPI_Allreduce(&local_train_miss_count, &global_train_miss_count, 1,
        //               MPI_SIZE_T, MPI_SUM, MPI_COMM_WORLD);

        // MPI_Allreduce(&local_test_miss_count, &global_test_miss_count, 1,
        //               MPI_SIZE_T, MPI_SUM, MPI_COMM_WORLD);

        // master_log(format("train miss: %f / %f (%f)")
        //            % global_train_miss_count % data_.get_global_train_num_instances()
        //            % (static_cast<scalar>(global_train_miss_count)
        //               / data_.get_global_train_num_instances() ));

        // master_log(format("test miss: %f / %f (%f)")
        //            % global_test_miss_count % data_.get_global_test_num_instances()
        //            % (static_cast<scalar>(global_test_miss_count)
        //               / data_.get_global_test_num_instances() ));

        if(have_test_) {

            // for test
            std::fill(local_buf, local_buf+(3*K), 0);
            tp=&local_buf[0];
            fp=&local_buf[K];
            fn=&local_buf[2*K];

for(auto &x: data_.local_test_instances_) {
                const size_t yi = x.label_;
                const size_t local_i= x.id_ / numtasks ;
                size_t hatyi = 0;
                scalar max_score = test_predictions_[local_i * K + 0 ];
                for(size_t k=0; k < K; ++k ) {
                    scalar wkTxi = test_predictions_[local_i * K + k];
                    if(wkTxi > max_score) {
                        max_score = wkTxi;
                        hatyi = k;
                    }
                }
                if(yi == hatyi) {
                    tp[hatyi]++;
                } else {
                    fp[hatyi]++;
                    fn[yi]++;
                }
            }

            std::fill(global_buf, global_buf+(3*K), 0);
            MPI_Allreduce(&local_buf[0], &global_buf[0], 3*K,
                          MPI_INT, MPI_SUM, MPI_COMM_WORLD);
            tp=&global_buf[0];
            fp=&global_buf[K];
            fn=&global_buf[2*K];

            scalar test_micro_f1 = SCALAR_ZERO;
            scalar test_macro_f1 = SCALAR_ZERO;
            size_t test_nr_precision = 0;
            size_t test_dr_precision = 0;
            size_t test_dr_recall = 0;

            calc_f1_measures(tp, fp, fn,
                             test_micro_f1, test_macro_f1,
                             test_nr_precision, test_dr_precision, test_dr_recall);

            master_log(format("grepthis %d %f %f %f %f %f %f")
                       % iter_num
                       % global_primal_obj
                       % optimization_elapsed_secs_
                       % train_macro_f1
                       % train_micro_f1
                       % test_macro_f1
                       % test_micro_f1
                      );
        } else
            master_log(format("grepthis %d %f %f %f %f")
                       % iter_num
                       % global_primal_obj
                       % optimization_elapsed_secs_
                       % train_macro_f1
                       % train_micro_f1
                      );

        return true;

    }
#endif
};
}


#endif
