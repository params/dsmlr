/*
 * Copyright (c) 2019 Parameswaran Raman, Sriram Srinivasan, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef MULTICLASS_DATALOADER
#define MULTICLASS_DATALOADER

#include <iostream>
#include <vector>
#include <fstream>
#include <random>
#include <boost/tokenizer.hpp>
#include "../nomad.hpp"
#include "../Logger.hpp"

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::vector;
using std::ifstream;
using std::ios;

using rng_type = std::mt19937_64;
rng_type rng_;

typedef int label_t;
typedef int idx_t;
typedef double val_t;

// struct to hold data points in 
// sparse nnz format 
typedef struct{
  size_t id_;
  label_t label_;
  vector<idx_t> idx_;
  vector<val_t> val_;
} Instance;

// struct to hold global dataset statistics
struct MetaData{
  size_t global_num_classes_;
  size_t global_num_features_;
  size_t global_train_num_instances_;
  size_t global_test_num_instances_;
  vector<size_t> global_train_num_nnz_col_;

  MetaData(): global_num_classes_(0),
  global_num_features_(0),
  global_train_num_instances_(0),
  global_test_num_instances_(0){}

  ~MetaData() {global_train_num_nnz_col_.clear();}

  vector<size_t>* get_nnz_col(){
    return &global_train_num_nnz_col_;
  }

  size_t get_omega_j(const size_t j){
    return global_train_num_nnz_col_[j];
  }

  void to_string(){
    cout << "N: " << global_train_num_instances_ 
      << ", NTest: " << global_test_num_instances_ 
      << ", D: " << global_num_features_ 
      << ", K: " << global_num_classes_ << endl;
  }
};


// struct to hold local dataset statistics
// (typically on each thread or process)
struct Data{
  size_t local_num_classes_;
  size_t local_train_num_instances_;
  size_t local_test_num_instances_;

  //BUGBUG: remember to work with references for these vectors
  //(eg. getter/setters) 
  vector<size_t> local_train_num_nnz_col_;
  vector<size_t> local_train_instances_index_;
  vector<Instance> local_train_instances_;
  vector<Instance> local_test_instances_;

  Data(): local_num_classes_(0),
  local_train_num_instances_(0),
  local_test_num_instances_(0){}

  ~Data(){
    local_train_num_nnz_col_.clear();
    local_train_instances_index_.clear();
    local_train_instances_.clear();
    local_test_instances_.clear();
  }

  bool have_test(){
    return (local_test_num_instances_ != 0);
  }

  //BUGBUG: fix random generator 
  void shuffle_train_instances_order(){
    std::shuffle(local_train_instances_index_.begin(),
        local_train_instances_index_.end(),
        rng_);
  }

  bool apply_to_train_block(std::function<bool(Instance &)> func){
    for(size_t& instance_index : local_train_instances_index_){
      if(!func(local_train_instances_[instance_index])){
        return false;
      }
    }
    return true;
  }

  bool apply_to_refresh_block(std::function<bool(Instance &)> func){
    for(Instance& instance : local_train_instances_){
      if(!func(instance)){
        return false;
      }
    }
    return true;
  }

  bool apply_to_test_block(std::function<bool(Instance &)> func){
    for(Instance& instance : local_test_instances_){
      if(!func(instance)){
        return false;
      }
    }
    return true;
  }
};

// DataLoader class
// only contains utility methods
class MulticlassDataLoader{
  private:
    Logger *logger_;
  public:
    MulticlassDataLoader(){
      logger_ = Logger::get_logger();
    }

    ~MulticlassDataLoader(){
    }

    long get_num_from_line(std::ifstream& mfs);

    //template <typename file_itype>
    bool read_meta_data(MetaData*& metadata,
        string train_file_name, 
        string test_file_name,
        string meta_file_name);

    void read_data(MetaData*& metadata, Data*& data, const size_t num_parts, 
        const string& data_file, const size_t min_instance_index, 
        bool train);

    void read_global_nnz(MetaData*& metadata, Data*& data, const size_t num_parts, 
        const string& data_file, const size_t min_instance_index, 
        bool train);

    void prepare_for_local_read(MetaData*& metadata, 
        Data*& data, 
        const size_t num_parts, 
        const size_t part_index, 
        bool train);
};




/////////////////////////////////////
/// Function definitions go here   //
/////////////////////////////////////

// returns how much of the given quantity each MPI process will be assigned
size_t num_per_task(size_t size, size_t numtasks){
  return static_cast<size_t>(std::ceil(static_cast<double>(size) / numtasks));
}

long MulticlassDataLoader::get_num_from_line(std::ifstream& mfs){
  string line;
  getline(mfs, line);
  return stoul(line);
}

// read the metadata first 
//template <typename file_itype>
bool MulticlassDataLoader::read_meta_data(MetaData*& metadata,
    string train_file_name, 
    string test_file_name,
    string meta_file_name){

  std::ifstream mfs;
  mfs.open(meta_file_name, std::ifstream::in);        
  if(mfs.good()){
    logger_->info("Reading from meta_file: " + meta_file_name);

    metadata->global_train_num_instances_ = get_num_from_line(mfs);
    metadata->global_test_num_instances_ = get_num_from_line(mfs);
    metadata->global_num_features_ = get_num_from_line(mfs);
    metadata->global_num_classes_ = get_num_from_line(mfs);
    string nnz;
    getline(mfs, nnz);
    boost::char_separator<char> sep(" \t:");
    boost::tokenizer<boost::char_separator<char>> tokens(nnz, sep);
    size_t count = 0;
    metadata->global_train_num_nnz_col_.resize(metadata->global_num_features_);
    size_t col_index = 0;
    for(const auto& t : tokens){
      metadata->global_train_num_nnz_col_[col_index++] = stoul(t);
    }
    logger_->info("global # classes: " + to_string(metadata->global_num_classes_));
    logger_->info("global # features: " + to_string(metadata->global_num_features_)) ;
    logger_->info("global # training instances: " + to_string(metadata->global_train_num_instances_));
    if(!test_file_name.size()==0){
      logger_->info("global # test instances: " + to_string(metadata->global_test_num_instances_));
    }
 
    mfs.close();  
    return true;
  }
  mfs.close();  

  logger_->info("First read of training and test file");
  logger_->info("Reading train data file: " +  train_file_name);

  // Do the first sweep to gather meta information about the dataset
  std::ifstream ifs;
  ifs.open(train_file_name, std::ifstream::in);        
  string line;
  long line_num = 0;
  while(getline(ifs, line)){
    line_num++;
    if(line_num%1000000 == 0){
      logger_->info("Num lines read: " + to_string(line_num));
    }
    boost::char_separator<char> sep(" \t:");
    boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
    size_t count = 0;
    for(const auto& t : tokens){
      count++;
      if(count == 1){
        size_t yi = stoul(t);
        if(yi > metadata->global_num_classes_)
          metadata->global_num_classes_ = yi;
        continue;
      }
      if(count%2){
        ;
      }else{
        unsigned long idx = stoul(t);
        if(idx > metadata->global_num_features_){
          metadata->global_num_features_ = idx;
          metadata->global_train_num_nnz_col_.resize(metadata->global_num_features_);
        }
        metadata->global_train_num_nnz_col_[idx-1]++;
      }
    }
    metadata->global_train_num_instances_++;
  }
  ifs.close();

  if(test_file_name.size()==0){
    logger_->info("No test data is specified.");
  }else{
    logger_->info("Reading test data file: " +  test_file_name);
    // Do the first sweep to gather meta information about the dataset
    ifs.open(test_file_name, std::ifstream::in);       
    line_num = 0; 
    while(getline(ifs, line)){
    line_num++;
    if(line_num%1000000 == 0){
      logger_->info("Num lines read: " + to_string(line_num));
    }
      boost::char_separator<char> sep(" \t:");
      boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
      size_t count = 0;
      for(const auto& t : tokens){
        count++;
        if(count == 1){
          size_t yi = stoul(t);
          if(yi > metadata->global_num_classes_)
            metadata->global_num_classes_ = yi;
          continue;
        }
        if(count%2){
          ;
        }else{
          unsigned long idx = stoul(t);
          if(idx > metadata->global_num_features_){
            metadata->global_num_features_ = idx;
          }
        }
      }
      metadata->global_test_num_instances_++;
    }
    ifs.close();
  }
  metadata->global_num_classes_++; // if class begins with 0.

  logger_->info("global # classes: " + to_string(metadata->global_num_classes_));
  logger_->info("global # features: " + to_string(metadata->global_num_features_)) ;
  logger_->info("global # training instances: " + to_string(metadata->global_train_num_instances_));
  if(!test_file_name.size()==0){
    logger_->info("global # test instances: " + to_string(metadata->global_test_num_instances_));
  }
 
  ofstream::openmode mode = ofstream::out;
  ofstream ofs(meta_file_name, mode);

  logger_->info("Writing to meta_file: " + meta_file_name);


  ofs<<metadata->global_train_num_instances_<<"\n"<<metadata->global_test_num_instances_<<"\n"<<metadata->global_num_features_<<"\n"<<metadata->global_num_classes_<<"\n";
  for (auto& d: metadata->global_train_num_nnz_col_){
    ofs<<d<<" ";
  }
  ofs.close();
  //Second sweep to update global nnz count
  //BUGBUG: if not passing meta-data info explicity, how to avoid this?
/*  metadata->global_train_num_nnz_col_.resize(metadata->global_num_features_);
  ifs.open(train_file_name, std::ifstream::in);        
  while(getline(ifs, line)){
    boost::char_separator<char> sep(" \t:");
    boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
    size_t count = 0;
    for(const auto& t : tokens){
      count++;
      if(count == 1){
        continue;
      }
      if(count%2){
        ;
      }else{
        unsigned long idx = stoul(t);
        //update global nnz count per column
        metadata->global_train_num_nnz_col_[idx-1]++;
      }
    }
  }
  ifs.close();*/
  //std::cout << "global train num nnz: ";
  //for(size_t j=0; j<metadata->global_num_features_; ++j)
  // std::cout << " " << metadata->global_train_num_nnz_col_[j];
  //std::cout << std::endl; 
  
  return true;
}

//read the local data on each thread or process
//BUGBUG: part_index is probably not needed?
//it can be computed on the fly
//(= instance_index / num_parts) 
void MulticlassDataLoader::read_data(MetaData*& metadata, 
    Data*& data, 
    const size_t num_parts, 
    const string& data_file, 
    const size_t min_instance_index, 
    bool train=true){

  logger_->info("actual reading now"); 
  std::ifstream ifs;
  ifs.open(data_file, std::ifstream::in);        
  string line;

  // read the only part each process need.
  for(size_t instance_index=0; instance_index < min_instance_index; ++instance_index){
    getline(ifs, line);
  }
  //logger_->info("min_instance_index: " + to_string(min_instance_index) + ", num_parts: " + to_string(num_parts));

  for(size_t instance_index=min_instance_index; ; ++instance_index){

    if(!getline(ifs, line))            
      break;
    if(instance_index % num_parts != min_instance_index)
      continue;          

    boost::char_separator<char> sep(" \t:");
    boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
    size_t count = 0;
    size_t local_instance_index = instance_index/num_parts;
    //logger_->info("instance_index: " + to_string(instance_index) + ", local index: " + to_string(local_instance_index));

    if(train)
      data->local_train_instances_[local_instance_index].id_ = instance_index;
    else
      data->local_test_instances_[local_instance_index].id_ = instance_index;

    //logger_->info("line: " + line);

    for(auto beg=tokens.begin(); beg!=tokens.end(); ++beg){
      const auto& t = *beg;
      count++;
      if(count == 1){
        size_t yi = stoul(t);
        if(train){
          data->local_train_instances_[local_instance_index].label_= (label_t) yi ;
        }else{
          data->local_test_instances_[local_instance_index].label_= (label_t) yi ;
        }
        continue;
      }
      if(count%2){
        if(train){
          data->local_train_instances_[local_instance_index].val_.push_back(stod(t));
        }else{
          data->local_test_instances_[local_instance_index].val_.push_back(stod(t));
        }
      }else{
        unsigned long idx = stoul(t); // assume that it begins with 1
        if(train){
          data->local_train_instances_[local_instance_index].idx_.push_back(idx-1);
          data->local_train_num_nnz_col_[idx-1]++;
        }else{
          data->local_test_instances_[local_instance_index].idx_.push_back(idx-1);
        }              
      }            
    }
  }

  ifs.close();
  if(train)
    logger_->info("reading train finished. acquired number of instances " 
        + to_string(data->local_train_instances_.size()));
  else
    logger_->info("reading test finished. acquired number of instances " 
        + to_string(data->local_test_instances_.size()));
  return;
}

// setting up some variables before reading local data 
void MulticlassDataLoader::prepare_for_local_read(MetaData*& metadata, 
    Data*& data, 
    const size_t num_parts, 
    const size_t part_index, 
    bool train=true){

  //Each thread has to read its part of data now
  size_t min_instance_index = part_index;
  if(train){
    data->local_train_num_instances_ = metadata->global_train_num_instances_ / num_parts;
    if(metadata->global_train_num_instances_ % num_parts > part_index )
      data->local_train_num_instances_++;

    data->local_train_instances_.resize(data->local_train_num_instances_);
    data->local_train_instances_index_.resize(data->local_train_num_instances_);
    iota(data->local_train_instances_index_.begin(), data->local_train_instances_index_.end(), 0);

    data->local_num_classes_ = num_per_task(metadata->global_num_classes_, num_parts);
    data->local_train_num_nnz_col_.resize(metadata->global_num_features_);
    logger_->info("local # of instances: " + to_string(data->local_train_num_instances_) 
        + " (start with " + to_string(min_instance_index) + " and obtain every " 
        + to_string(num_parts) + " lines ");
    logger_->info("local # of classes: " + to_string(data->local_num_classes_));
  }
  else{
    data->local_test_num_instances_ = metadata->global_test_num_instances_ / num_parts;
    if(metadata->global_test_num_instances_ % num_parts > part_index)
      data->local_test_num_instances_++;
    data->local_test_instances_.resize(data->local_test_num_instances_);

    logger_->info("local # of instances for test: " 
        + to_string(data->local_test_num_instances_) 
        + " (start with " + to_string(min_instance_index) 
        + " and obtain every " + to_string(num_parts) 
        + " lines ");
  }
}


#endif
