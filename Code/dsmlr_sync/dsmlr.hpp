/*
 * Copyright (c) 2019 Parameswaran Raman, Shin Matsushima, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef DSMLR_HPP_
#define DSMLR_HPP_


#include "mpi.h"

#include <cstdio>
#include <string>
#include <random>
#include <vector>
#include <iostream>

#include <boost/format.hpp>


namespace dsmlr{ 

  // BUGBUG the definition of MPI_SIZE_T as MPI_UNIT64_T is somewhat arbitrary
#define MPI_SIZE_T MPI_UINT64_T
#define MPI_SCALAR MPI_DOUBLE
  
  typedef double scalar;
  const scalar SCALAR_ZERO = static_cast<scalar>(0.0);

  using std::string;
  using rng_type = std::mt19937_64;

  using boost::format;

  const int MASTER_RANK = 0;

  using std::vector;
  using std::pair;

  /// A singleton class that describes the process environment
  class Environment{
    
  public:
    /// Retrieves the desired singleton class
    static Environment& get_instance(){
      static Environment instance;
      return instance;
    }

    ~Environment(){
      MPI_Finalize();
    }

    /// prints the log
    void print_log(string message){
      std::cout << boost::format("[rank: %03d] %s") % rank_ % message << std::endl;
    }

    /// prints the log
    void print_error(string message){
      std::cerr << boost::format("[rank: %03d] %s") % rank_ % message << std::endl;
    }

    /// retrieves RNG
    rng_type& get_rng(){
      return rng_;
    }

    /// returns true if this is the master task
    bool is_master(){
      return rank_ == MASTER_RANK;
    }

    /// returns the number of MPI tasks
    int get_numtasks(){
      return numtasks_;
    }

    /// returns the MPI rank of the process
    int get_rank(){
      return rank_;
    }

  private:
    /// number of MPI tasks
    int numtasks_;
    /// the MPI rank of the process
    int rank_;
    /// length of the hostname
    int hostname_len_;
    /// hostname
    char hostname_[MPI_MAX_PROCESSOR_NAME];
    /// random number generator
    rng_type rng_;

    /// creates the singleton environment
    Environment(){
      // Initialize MPI and retrieve processor info

      MPI_Init(nullptr, nullptr);
      MPI_Comm_rank(MPI_COMM_WORLD, &rank_);
      MPI_Comm_size(MPI_COMM_WORLD, &numtasks_);
      MPI_Get_processor_name(hostname_, &hostname_len_);

      print_log((boost::format("processor name: %s, number of tasks: %d, rank: %d")
                 % hostname_ % numtasks_ % rank_).str());
    }

    Environment(Environment const&); // don't implement
    void operator=(Environment const&); // don't implement
  };

  /// convenience function for Environment::get_instance().log(message);
  void print_log(string message){
    Environment::get_instance().print_log(message);
  }
  
  /// convenience function for Environment::get_instance().log(message);
  void print_log(boost::basic_format<char> message){
    print_log(message.str());
  }

  /// convenience function for Environment::get_instance().error(message);
  void print_error(string message){
    Environment::get_instance().print_error(message);
  }
  
  /// convenience function for Environment::get_instance().error(message);
  void print_error(boost::basic_format<char> message){
    print_error(message.str());
  }

  /// only the master process prints the log
  template<typename T>
  void master_log(T message){
    if(Environment::get_instance().is_master()){
      print_log(message);
    }
  }

  /// only the master process prints the error
  template<typename T>
  void master_error(T message){
    if(Environment::get_instance().is_master()){
      print_error(message);
    }
  }
  /// returns how much of the given quantity each MPI process will be assigned
  size_t num_per_task(size_t size, size_t numtasks){
    return static_cast<size_t>(std::ceil(static_cast<double>(size) / numtasks));
  }

  void calc_f1_measures(size_t *tp,
                        size_t *fp,
                        size_t *fn,
                        scalar &micro_f1,
                        scalar &macro_f1,
                        size_t &nr_precision,
                        size_t &dr_precision,
                        size_t &dr_recall,
                        size_t &num_labels){
    
    for(size_t k = 0; k < num_labels; ++k){
      nr_precision += tp[k];
      dr_precision += tp[k] + fp[k];
      dr_recall += tp[k] + fn[k];
      
      scalar p = 1.0;
      scalar r = 1.0;
        
      if(tp[k]+fp[k] > 0)
        p = (scalar)tp[k]/(tp[k] + fp[k]);
      if(tp[k]+fn[k] > 0)
        r = (scalar)tp[k]/(tp[k] + fn[k]);
      
      if(p or r)
        macro_f1 += (2*p*r)/(p+r);
    }
    macro_f1 /= num_labels;
    scalar p = (scalar) nr_precision / dr_precision;
    scalar r = (scalar) nr_precision / dr_recall;
    if(p or r)
      micro_f1 = (2 * p * r) / (p + r);
    master_log(format("train precision: %f, train recall: %f") % p % r);
    return;
  }
}

#endif
