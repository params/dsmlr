/*
 * Copyright (c) 2019 Parameswaran Raman, Shin Matsushima, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef MLR_OPTIMIZER_HPP_
#define MLR_OPTIMIZER_HPP_

#include "optimizer.hpp"
#include "data.hpp"
#include <string>
// #include <tuple>

// using namespace std;

namespace dsmlr{

  const scalar minus_infty = -std::numeric_limits<scalar>::max();

  class ai_plus{
    
  public:
    scalar ai_;
    scalar max_fx_;
    scalar fxy_;
    size_t yhat_;
    
    ai_plus(){
      reset();
    }
    
    void reset(void){
      ai_ = SCALAR_ZERO;
      max_fx_ = minus_infty;
      fxy_ = minus_infty;
      yhat_ = 0;
    }
    
  };
  
  class MlrOptimizer :public Optimizer{

  public:
    MlrOptimizer(Option& option,
                 Data& data) :
      Optimizer(option, data){ }
    
    ~MlrOptimizer(){
      
      vec_ai_.clear();
      vec_ai_test_.clear();
    }
    
  private:
    
    vector<ai_plus> vec_ai_;
    vector<ai_plus> vec_ai_test_;
    
    scalar compute_xtw_k(const Instance &xi, const size_t local_k){
      
      vector<char> &w = msg_primal_buf_;
      scalar dot = SCALAR_ZERO;
      const size_t omega_i = xi.idx_.size();
      for(size_t it = 0; it < omega_i; ++it){
        const size_t j = xi.idx_[it];
        const scalar xij = xi.val_[it];
        const scalar wkj = *(getparam_val(w, local_k * D + j));
        dot += wkj * xij;
      }
      return dot;
    }
    

    // Note that ai = \sum_{k so far} exp( <w_k, x_i> - max_xTwk). This
    // function maintains the invariant on ai. Also it ensures that we
    // never compute exp on an positive number, thus avoiding overflow.
    // 
    // case 1:
    // max_xTwk_local > max_xTwk
    // ai = ai*exp(max_xTwk - max_xTwk_local) + \sum_{k in this block} exp(<w_k, x_i> - max_xTwk_local)
    // Update max_xTwk = max_xTwk_local
    // 
    // case 2:
    // max_xTwk_local <= max_xTwk
    // ai = ai + \sum_{k in this block} exp(<w_k, x_i> - max_xTwk)
    //
    
    void compute_local_ai(const Instance &xi, ai_plus &ai){
      
      const label_t yi = xi.label_;
      bool fxy_flag = false;
      if(first_k <= yi and yi <= last_k)
        fxy_flag = true;
      
      vector<scalar> vec_xTwk(last_k - first_k, SCALAR_ZERO);
      scalar xTwk;
      
      scalar max_fx = minus_infty;
      size_t argmax_local_k = 0;
      
      for(size_t local_k = 0; local_k < last_k - first_k; ++local_k){
        xTwk = compute_xtw_k(xi, local_k);
        vec_xTwk[local_k] = xTwk;
        
        if(xTwk > max_fx){
          max_fx = xTwk;
          argmax_local_k = local_k;
        }
        if(fxy_flag and yi == first_k + local_k)
          ai.fxy_ = xTwk;
      }
      
      if(max_fx > ai.max_fx_){
        ai.ai_ *= exp(ai.max_fx_ - max_fx);
        ai.max_fx_ = max_fx;
        ai.yhat_ = argmax_local_k + first_k;
      }

      for(size_t local_k = 0; local_k < last_k - first_k; ++local_k)
        ai.ai_ += exp(vec_xTwk[local_k] - ai.max_fx_);
      return;
    }

    bool init_local(){

      vec_ai_.resize(local_N);
      vec_ai_test_.resize(local_Ntest);
      return true;
    }

    bool communicate(int recv_from, int send_to){
      
      const size_t primal_buffer_size = msg_primal_buf_.size();
      assert(primal_buffer_size % chunk_size_ == 0);
      const size_t num_chunks = primal_buffer_size/chunk_size_;
      
      //Communicate
      for(size_t j = 0; j < num_chunks; ++j){
        MPI_Request req;
        MPI_Status stat;
        MPI_Isend(&msg_primal_buf_[chunk_size_ * j],
                  chunk_size_,
                  MPI_CHAR,
                  send_to,
                  0,
                  MPI_COMM_WORLD,
                  &req);
        MPI_Recv(&msg_recv_buf_[0],
                 chunk_size_,
                 MPI_CHAR,
                 recv_from,
                 0,
                 MPI_COMM_WORLD,
                 &stat);
        MPI_Wait(&req, &stat);
        memcpy(&msg_primal_buf_[chunk_size_ * j], &msg_recv_buf_[0], chunk_size_);
      }
      return true;
    }


    bool pre_bulk_sync(void){

      // svnvish: BUGBUG
      // how can this be done with std::fill?
      for(size_t i = 0; i < local_N; i++)
        vec_ai_[i].reset();
      return true;
    }

    bool bulk_sync_block(size_t block_index){
      
      // Figure out which labels we own
      first_k = block_index * local_K;
      last_k = std::min(first_k + local_K, K);

      return data_.apply_to_refresh_block(block_index,
                                          [&](Instance & instance)->bool{
                                            
                                            const Instance &xi = instance;
                                            const size_t local_i = get_local_idx(xi); 
                                            compute_local_ai(instance, vec_ai_[local_i]);                                            
                                            return true;
                                          });
      
    }
    
    bool post_bulk_sync(void){      

      for(size_t local_i = 0; local_i < local_N; ++local_i){
        scalar &bi = *(getparam_val(msg_dual_buf_, local_i));
        bi = -log(vec_ai_[local_i].ai_) - vec_ai_[local_i].max_fx_;
      }
      return true;
    }
    
    
    bool pre_optimize(void){

      return true;
    }
    
    bool optimize_block(size_t block_index, size_t inner_iter_num){
      
      // First figure out which labels we own
      first_k = block_index * local_K;
      last_k = std::min(first_k + local_K, K);
      
      // learning rate
      scalar eta = option_.eta_/sqrt(std::max(1.0, static_cast<scalar>(iter_num_)));

      bool succeed = false;
      vector<char> &w = msg_primal_buf_;
      size_t repeats = 1;
      if(option_.smart_init_ && iter_num_==0)
          repeats = option_.inner_repeat_;
      
      for(size_t repeat_itr = 0; repeat_itr < repeats; ++repeat_itr){
        if(option_.shuffle_)
          data_.shuffle_train_instances_order();
        for(size_t local_k = 0; local_k < last_k - first_k; ++local_k){
          size_t k = first_k + local_k;
          succeed =  data_.apply_to_train_block(block_index,
                                                [&](Instance & instance)->bool{
                                                  
                                                  const Instance &xi = instance;
                                                  const size_t local_i = get_local_idx(xi);
                                                  scalar &bi = *(getparam_val(msg_dual_buf_, local_i));
                                                  
                                                  scalar dot = compute_xtw_k(instance, local_k);
                                                  const label_t yi = xi.label_;
                                                  scalar sg_w = (yi != k) ? 0.0 : -1.0/N;

                                                  sg_w += safe_exp_grad(dot+bi)/N;

                                                  for(size_t it = 0; it < xi.idx_.size(); ++it){
                                                    const size_t j = xi.idx_[it];
                                                    const scalar xij = xi.val_[it];
                                                    const scalar omega_j = data_.get_omega_j(j);
                                                    scalar &wkj = *(getparam_val(w, local_k * D + j));
                                                    wkj -= eta * (lambda_ * wkj/omega_j + sg_w * xij);
                                                  }
                                                  return true;
                                                });
          if(!succeed)
            return false;
        }
      }
      return true;
    }
    
    bool post_optimize(void){      
      return true;
    }
    
    bool pre_test(void){
      
      // svnvish: BUGBUG
      // how can this be done with std::fill?
      for(size_t i = 0; i < local_N; i++)
        vec_ai_[i].reset();
      for(size_t i=0; i < local_Ntest; i++)
        vec_ai_test_[i].reset();
      return true;
    }

    bool test_block(size_t block_index){
      
      // Figure out which labels we own
      first_k = block_index * local_K;
      last_k = std::min(first_k + local_K, K);

      // on training data
      bool success = data_.apply_to_refresh_block(block_index,
                                                  [&](Instance & instance)->bool{

                                                    const Instance &xi = instance;
                                                    const size_t local_i = get_local_idx(xi);
                                                    compute_local_ai(instance, vec_ai_[local_i]);
                                                    return true;
                                                  });

      if(!success)
        return false;
      // on test data
      success = data_.apply_to_test_block(block_index,
                                          [&](Instance & instance)->bool{

                                            const Instance &xi = instance;
                                            const size_t local_i = get_local_idx(xi);
                                            compute_local_ai(instance, vec_ai_test_[local_i]);
                                            return true;
                                          });
      return success;
    }

    void compute_f1(const vector<Instance> &data,
                    const vector<ai_plus>  &a,
                    scalar  &micro_f1, 
                    scalar  &macro_f1){
      
      
      // Collect f1 stats
      size_t local_buf[3*K];
      std::fill(local_buf, local_buf + (3*K), SCALAR_ZERO);
      size_t *tp=&local_buf[0];
      size_t *fp=&local_buf[K];
      size_t *fn=&local_buf[2*K];

      for(auto &x: data){
        const size_t yi = x.label_;
        const size_t local_i = get_local_idx(x);
        const size_t yhati = a[local_i].yhat_;
        
        if(yi == yhati) 
          tp[yhati]++;
        else {
          fp[yhati]++;
          fn[yi]++;
        }
      }
      
      //Compute global f1 stats
      size_t global_buf[3*K];
      std::fill(global_buf, global_buf + (3*K), SCALAR_ZERO);
      MPI_Allreduce(&local_buf[0], &global_buf[0], 3*K,
                    MPI_SIZE_T, MPI_SUM, MPI_COMM_WORLD);
      
      tp = &global_buf[0];
      fp = &global_buf[K];
      fn = &global_buf[2*K];
     
      size_t nr_precision = 0;
      size_t dr_precision = 0;
      size_t dr_recall = 0;

      calc_f1_measures(tp, fp, fn,
                       micro_f1, macro_f1,
                       nr_precision, dr_precision, dr_recall, K);

      print_log(format("nr_pr: %d, dr_pr: %d, dr_re: %d, micro_f1: %f, macro_f1: %f") 
                % nr_precision % dr_precision % dr_recall % micro_f1 % macro_f1);
      
      return;
    }

    bool post_test(size_t iter_num){

      // Compute loss and objective function on train data 
      scalar loss = SCALAR_ZERO;
      for(size_t local_i = 0; local_i < local_N; ++local_i){
        const ai_plus ai = vec_ai_[local_i];
        loss += log(ai.ai_) + ai.max_fx_ - ai.fxy_;
      }
      
      loss /= N;

      scalar reg = SCALAR_ZERO;
      for (size_t i = 0; i < D * (last_k - first_k); ++i) {
        scalar wik = *(getparam_val(msg_primal_buf_, i));
        reg +=  wik * wik;
      }
      reg *=0.5* lambda_;
      
      scalar obj = loss + reg;
          
      print_log(format("local loss: %f") % loss);
      print_log(format("local regularizer: %f") % reg);
      print_log(format("local obj = loss + regularizer: %f") % obj);
      
      //Compute global primal obj
      scalar global_obj = SCALAR_ZERO;
      MPI_Allreduce(&obj, &global_obj, 1,
                    MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      

      scalar train_micro_f1 = SCALAR_ZERO;
      scalar train_macro_f1 = SCALAR_ZERO;
      scalar test_micro_f1 = SCALAR_ZERO;
      scalar test_macro_f1 = SCALAR_ZERO;
      
      //Compute f1 scores for train
      compute_f1(data_.local_train_instances_, vec_ai_, train_micro_f1, train_macro_f1);
      
      //Compute f1 for test
      compute_f1(data_.local_test_instances_, vec_ai_test_, test_micro_f1, test_macro_f1);
      
      master_log(format("grepthis %d %f %f %f %f %f %f")
                 % iter_num
                 % global_obj
                 % optimization_elapsed_secs_
                 % train_macro_f1
                 % train_micro_f1
                 % test_macro_f1
                 % test_micro_f1
                 );
      
      return true;
    }

  };
}
#endif
