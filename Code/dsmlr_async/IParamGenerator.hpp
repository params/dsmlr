/*
 * Copyright (c) 2019 Parameswaran Raman, Sriram Srinivasan, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef PARAM_GENERATOR
#define PARAM_GENERATOR

#include "IParam.hpp"

/*
 * This is an interface used to generate global parameter
 * when on demand. For example check VectorParamGenerator
 * in VectorParam.hpp
 */
class IParamGenerator{
    public:
        //This should create a blank Param to which data 
        //can be unloaded using unpack() in IParam.
        virtual IParam* generate_parameter() = 0;
};

#endif
