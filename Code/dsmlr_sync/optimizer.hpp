/*
 * Copyright (c) 2019 Parameswaran Raman, Shin Matsushima, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef OPTIMIZER_HPP_
#define OPTIMIZER_HPP_

#include <sstream>
#include <chrono>

#include "dsmlr.hpp"
#include "option.hpp"
#include "data.hpp"

namespace dsmlr{

  /// Distributed Doubly Separable Optimization problem Optimizer
  class Optimizer{

  public:
    Optimizer(Option& option, Data& data) :
      option_(option),
      chunk_size_(sizeof(scalar) * option_.chunk_size_),
      data_(data){
      

      assert(local_N > 0);
      size_t primal_buffer_size = local_K * D * sizeof(scalar);
      // Extend it so that buffersize can be divided by chunk_size
      primal_buffer_size = (1 + primal_buffer_size / chunk_size_) * chunk_size_;
      // svnvish: BUGBUG
      // not sure why this is not the same as the line above
      // primal_buffer_size = std::ceil(primal_buffer_size/chunk_size) * chunk_size; 
      
      msg_primal_buf_.resize(primal_buffer_size);
      std::fill(msg_primal_buf_.begin(), msg_primal_buf_.end(), 0);
      
      
      msg_recv_buf_.resize(chunk_size_);
      std::fill(msg_recv_buf_.begin(), msg_recv_buf_.end(), 0);

      size_t dual_buffer_size = local_N * sizeof(scalar);
      msg_dual_buf_.resize(dual_buffer_size);
      scalar* buf_begin = reinterpret_cast<scalar *>(&(msg_dual_buf_.front()));
      scalar* buf_end = buf_begin + local_N;
      scalar value = log(1.0/data_.get_global_num_classes());
      std::fill(buf_begin, buf_end, value);

    }

    virtual ~Optimizer(){
      msg_primal_buf_.clear();
      msg_dual_buf_.clear();
      msg_recv_buf_.clear();
    }
    
    /** 
     * Runs optimizer with one delay
     * 
     * @return true if succeed, false otherwise
     */
    bool one_delay(){

      using std::chrono::duration_cast;
      using std::chrono::milliseconds;
      using std::chrono::steady_clock;

      auto& env = Environment::get_instance();
      const int numtasks = env.get_numtasks();
      const int rank = env.get_rank();

      if(!init()){
        return false;
      }
      
      const size_t recv_from = (rank + 1) % numtasks;
      const size_t send_to = (rank + numtasks - 1) % numtasks;

      for(iter_num_ = 0; iter_num_ < option_.max_iter_; iter_num_++){
        
        master_log(format("Starting iteration: %d") % iter_num_);
        
        steady_clock::time_point optimization_begin = steady_clock::now();
        
        if(iter_num_ != 0 and iter_num_%option_.bs_period_==0){

            if(!pre_bulk_sync()) 
              return false;
            
            // executes bulk synchronization
            for(int inner_iter_num = 0; inner_iter_num < numtasks; inner_iter_num++){
              
              size_t block_index = (rank + inner_iter_num) % numtasks;
              
              // communicate parameters
              if(!communicate(recv_from, send_to))
                return false;
              
              // executes bulk synchronization 
              if(!bulk_sync_block(block_index))
                return false;
            }
            
            if(!post_bulk_sync())
              return false;
            
        }
        
        if(!pre_optimize())
          return false;
       
        // executes stratified SG updates
        for(int inner_iter_num = 0; inner_iter_num < numtasks; inner_iter_num++){
          
          size_t block_index = (rank + inner_iter_num) % numtasks;
          
          // communicate parameters
          if(!communicate(recv_from, send_to))
            return false;
          
          // excutes learning process
          if(!optimize_block(block_index, inner_iter_num))
            return false;
          
        }
        
        if(!post_optimize())
          return false;
        
        steady_clock::time_point optimization_end = steady_clock::now();
        optimization_elapsed_secs_ += 
          static_cast<scalar>
          (duration_cast<milliseconds>(optimization_end - optimization_begin).count())
          / 1000;
        
        if(iter_num_ % option_.eval_period_ == 0){

          pre_test();
          
          for(int inner_iter_num = 0; inner_iter_num < numtasks; inner_iter_num++){
            size_t block_index = (rank + inner_iter_num) % numtasks;
            // communicate parameters
            if(!communicate(recv_from, send_to))
              return false;

            if(!test_block(block_index))
              return false;
          }

          // aggregate results from all processes and report
          post_test(iter_num_);
        }
      }

      return true;
    }

    /** 
     * Runs optimizer with zero delay
     * 
     * @return true if succeed, false otherwise
     */
    bool zero_delay(){

      using std::chrono::duration_cast;
      using std::chrono::milliseconds;
      using std::chrono::steady_clock;

      auto& env = Environment::get_instance();
      const int numtasks = env.get_numtasks();
      const int rank = env.get_rank();

      if(!init()){
        return false;
      }
      
      const size_t recv_from = (rank + 1) % numtasks;
      const size_t send_to = (rank + numtasks - 1) % numtasks;

      for(iter_num_ = 0; iter_num_ < option_.max_iter_; iter_num_++){
        
        master_log(format("Starting iteration: %d") % iter_num_);
        
        steady_clock::time_point optimization_begin = steady_clock::now();
        
        if(!pre_optimize())
          return false;

        if(!pre_bulk_sync())
          return false;
        
        // executes stratified SG updates
        for(int inner_iter_num = 0; inner_iter_num < numtasks; inner_iter_num++){
          
          size_t block_index = (rank + inner_iter_num) % numtasks;
          
          // communicate parameters
          if(!communicate(recv_from, send_to))
            return false;
          
          // excutes learning process
          if(!optimize_block(block_index, inner_iter_num))
            return false;
          
          // executes bulk synchronization 
          if(!bulk_sync_block(block_index))
            return false;
        }
        
        if(!post_optimize())
          return false;

        if(!post_bulk_sync())
          return false;
        
        steady_clock::time_point optimization_end = steady_clock::now();
        optimization_elapsed_secs_ += 
          static_cast<scalar>
          (duration_cast<milliseconds>(optimization_end - optimization_begin).count())
          / 1000;
        
        if(iter_num_ % option_.eval_period_ == 0){

          pre_test();
          
          for(int inner_iter_num = 0; inner_iter_num < numtasks; inner_iter_num++){
            size_t block_index = (rank + inner_iter_num) % numtasks;
            // communicate parameters
            if(!communicate(recv_from, send_to))
              return false;

            if(!test_block(block_index))
              return false;
          }

          // aggregate results from all processes and report
          post_test(iter_num_);
        }
      }

      return true;
    }

    /** 
     * Runs optimizer with mixed delay (1delay for iter0 and 0delay from iter1 onwards)
     * 
     * @return true if succeed, false otherwise
     */
    bool mixed_delay(){

      using std::chrono::duration_cast;
      using std::chrono::milliseconds;
      using std::chrono::steady_clock;

      auto& env = Environment::get_instance();
      const int numtasks = env.get_numtasks();
      const int rank = env.get_rank();

      if(!init()){
        return false;
      }
      
      const size_t recv_from = (rank + 1) % numtasks;
      const size_t send_to = (rank + numtasks - 1) % numtasks;

      for(iter_num_ = 0; iter_num_ < option_.max_iter_; iter_num_++){
        
        master_log(format("Starting iteration: %d") % iter_num_);
        
        steady_clock::time_point optimization_begin = steady_clock::now();
        
        if(iter_num_==1){

            if(!pre_bulk_sync()) 
              return false;
            
            // executes bulk synchronization
            for(int inner_iter_num = 0; inner_iter_num < numtasks; inner_iter_num++){
              
              size_t block_index = (rank + inner_iter_num) % numtasks;
              
              // communicate parameters
              if(!communicate(recv_from, send_to))
                return false;
              
              // executes bulk synchronization 
              if(!bulk_sync_block(block_index))
                return false;
            }
            
            if(!post_bulk_sync())
              return false;
            
        }
        
        if(!pre_optimize())
          return false;

        if(iter_num_ > 1)
          if(!pre_bulk_sync())
            return false;

       
        // executes stratified SG updates
        for(int inner_iter_num = 0; inner_iter_num < numtasks; inner_iter_num++){
          
          size_t block_index = (rank + inner_iter_num) % numtasks;
          
          // communicate parameters
          if(!communicate(recv_from, send_to))
            return false;
          
          // excutes learning process
          if(!optimize_block(block_index, inner_iter_num))
            return false;
          
          if(iter_num_ > 1)
            // executes bulk synchronization 
            if(!bulk_sync_block(block_index))
              return false;
          
        }
        
        if(!post_optimize())
          return false;
        
        if(iter_num_ > 1)
          if(!post_bulk_sync())
            return false;
        
        steady_clock::time_point optimization_end = steady_clock::now();
        optimization_elapsed_secs_ += 
          static_cast<scalar>
          (duration_cast<milliseconds>(optimization_end - optimization_begin).count())
          / 1000;
        
        if(iter_num_ % option_.eval_period_ == 0){

          pre_test();
          
          for(int inner_iter_num = 0; inner_iter_num < numtasks; inner_iter_num++){
            size_t block_index = (rank + inner_iter_num) % numtasks;
            // communicate parameters
            if(!communicate(recv_from, send_to))
              return false;

            if(!test_block(block_index))
              return false;
          }

          // aggregate results from all processes and report
          post_test(iter_num_);
        }
      }

      return true;
    }



  protected:

    // svnvish: BUGBUG
    // some variables have an underscore and others don't
    // make it consistent
    
    const int numtasks = Environment::get_instance().get_numtasks();
    
    Option& option_;

    // How many bytes transmitted per MPI send/receive
    const size_t chunk_size_; 

    // Regularization constant 
    const scalar lambda_ = option_.lambda_;

    // Threshold for exp calculation
    // svnvish: BUGBUG revisit later if needed 
    const scalar threshold = 2;

    Data& data_;

    // Total number of train data points
    size_t N = data_.get_global_train_num_instances(); 
    // Total number of features
    size_t D = data_.get_global_num_features();
    // Total number of classes
    size_t K = data_.get_global_num_classes(); 

    // Number of train data points in this processor
    size_t local_N = data_.get_local_train_num_instances(); 
    // Number of classes assigned to this process
    size_t local_K = data_.get_local_num_classes(); 

    // Total number of test data
    size_t Ntest = data_.get_global_test_num_instances();
    // Number in this machine
    size_t local_Ntest = data_.get_local_test_num_instances(); 

    // Storage for primal parameters
    vector<char> msg_primal_buf_;
    // Storage for dual parameters
    vector<char> msg_dual_buf_;
    // Extra buffer to store data in chunks
    vector<char> msg_recv_buf_;


    // Note:
    // 1. We assume that contiguous blocks of classes are assigned to each
    // processor
    // 2. If last_k == K, then last_k - first_k may not be equal to
    // local_K. This is because we cannot assume that number of classes
    // is divisible by number of processors. 
    // 3. Classes which derive from the optimizer have to maintain these
    // variables. They will change when you communicate primal
    // parameters 
    
    // First class label assigned to this processor
    // note use of lower case k
    size_t first_k; 
    // Last class label assigned to this processor
    size_t last_k;  

    size_t iter_num_;
    
    // elapsed seconds for optimization
    double optimization_elapsed_secs_ = 0.0;
    
    /// initialize local variables
    virtual bool init_local() = 0;
    
    /** 
     * Communicate (nomadic) parameters 
     * 
     * @param recv_from rank of the machine to receive message from
     * @param send_to rank of the machine to send message to 
     * 
     * @return true if succeed
     */
    virtual bool communicate(int recv_from, int send_to) = 0;

    /** 
     * Prepare for optimization
     * 
     * @return true if succeed, false otherwise
     */
    virtual bool pre_optimize() = 0;

    /** 
     * Run updates on a specified block
     * 
     * @param block_index index of the block to be processed
     * 
     * @return true if succeed, false otherwise
     */
    virtual bool optimize_block(size_t block_index, size_t inner_iter_num) = 0;

    /** 
     * Clean up after optimization
     * 
     * @return true if succeed, false otherwise
     */
    virtual bool post_optimize(void) = 0;
    
    /** 
     * Prepare for bulk Synchronization
     * 
     * @return true if succeed, false otherwise
     */
    virtual bool pre_bulk_sync(void) = 0;

    /** 
     * Run bulk synchronization on a given block
     * 
     * @param block_index index of the block to be processed
     * 
     * @return true if succeed, false otherwise
     */
    virtual bool bulk_sync_block(size_t block_index) = 0;

    /** 
     * Clean up after bulk synchronization 
     * 
     * @return true if succeed, false otherwise
     */
    virtual bool post_bulk_sync(void) = 0;

    /** 
     * Prepare for testing
     * 
     * @return true if succeed, false otherwise
     */
    virtual bool pre_test(void) = 0;

    /** 
     * Test on a specified block
     * 
     * @param block_index index of the block to be tested
     * 
     * @return true if succeed, false otherwise
     */
    virtual bool test_block(size_t block_index) = 0;

    /** 
     * This is run after processing test blocks
     * 
     * @return true if succeed, false otherwise
     */
    virtual bool post_test(size_t iter_num) = 0;

    /** 
     * initialize the solution
     * 
     * @return true if succeed, false otherwise
     */
    bool init(){
      print_log("initializing local variables");

      if(!init_local()){
        print_log("init_local() failed, returning false");
        return false;
      }
      
      return true;
    }
    
    inline scalar* getparam_val(vector<char>& buf, size_t index){
      return reinterpret_cast<scalar *>(&buf[index*sizeof(scalar)]);
    }
    
    inline void setparam_val(vector<char>& buf, size_t index, scalar value){
      scalar* scalar_ptr = reinterpret_cast<scalar *>(&buf[index*sizeof(scalar)]);
      *scalar_ptr = value;
      return;
    }

    // Compute the gradient of exp(x) in a safe way
    // We expect the threshold to be a small value close to 0.
    inline scalar safe_exp_grad(scalar x){
      
      return exp(std::min(x, threshold));
    }
    
    inline size_t get_local_idx(const Instance &xi){
      return xi.id_/numtasks;
    }
    
  };
  
}

#endif
