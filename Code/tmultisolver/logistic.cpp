/*
 * Copyright (c) 2019 Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#include "logistic.hpp"

namespace logisticloss{
  
  // loss_grad - Evaluates logistic loss and its gradient. 
  
  // Input Parameters:
  // fx_array - function prediction
  // labels_array - labels
  // n - size of arrays
  // user - user defined application context 

  // Output parameters:
  // grad_array - coefficients of the gradient 
  // loss function value 

  PetscErrorCode loss_grad(PetscScalar *fx_array, 
                           PetscScalar *labels_array, 
                           PetscScalar *grad_array, 
                           PetscInt    n,
                           PetscScalar *loss,
                           AppCtx      *user) {
    *loss = 0.0;
    // Compute function and gradient coefficients
    for (int i = 0; i < n; i++) {
      PetscInt start = i * user->nclasses;
      PetscInt end = (i+1) * user->nclasses;
      PetscScalar fx_max = 0.0;
      for (PetscInt idx = start; idx < end; idx++)
        if (fx_array[idx] > fx_max)
          fx_max = fx_array[idx];
      PetscScalar fx_sum = 0.0;
      for(PetscInt idx = start; idx < end; idx++) {
        grad_array[idx] = exp(fx_array[idx] - fx_max);
        fx_sum += grad_array[idx];
      }
      PetscInt lidx = labels_array[i] + start;
      *loss -= (fx_array[lidx] - log(fx_sum) - fx_max);
      for(PetscInt idx = start; idx < end; idx++)
        grad_array[idx] /= fx_sum * user->m;
      grad_array[lidx] -= 1.0/user->m;
    }
    *loss /= user->m;
    return 0;
  }
  
  PetscErrorCode orig_loss(PetscScalar *fx_array, 
                           PetscScalar *labels_array, 
                           PetscInt    n, 
                           PetscScalar *loss,
                           AppCtx      *user) {
    *loss = 0.0;
    // Compute function value
    for (int i = 0; i < n; i++) {
      PetscInt start = i * user->nclasses;
      PetscInt end = (i+1) * user->nclasses;
      PetscScalar fx_max = 0.0;
      for(PetscInt idx = start; idx < end; idx++)
        if(fx_array[idx] > fx_max)
          fx_max = fx_array[idx];
      PetscScalar fx_sum = 0.0;
      for(PetscInt idx = start; idx < end; idx++) {
        fx_sum += exp(fx_array[idx] - fx_max);
      }
      PetscInt lidx = labels_array[i] + start;
      *loss -= (fx_array[lidx] - log(fx_sum) - fx_max);
    }
    
    *loss /= user->m;

    return 0;
  }

  PetscErrorCode init(AppCtx *user) { 
    return 0;
  }

  PetscErrorCode finalize(void) {
    return 0;
  }
}
