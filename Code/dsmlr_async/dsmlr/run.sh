#! /bin/bash

mpirun -np 1 ./dsmlr_double --train ~/data/CLEF/train2.txt --test ~/data/CLEF/test2.txt --meta ~/data/CLEF/meta.txt --nthreads 4 -t 20 -l 0.0001 -e 100 --info false --debug false|grep grep
