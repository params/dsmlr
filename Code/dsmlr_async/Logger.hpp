/*
 * Copyright (c) 2019 Parameswaran Raman, Sriram Srinivasan, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef CUSTOM_LOGGER
#define CUSTOM_LOGGER

#include <iostream>
#include "tbb/tbb.h"
#include <tbb/compat/thread>
#include <mutex>

using std::cout;
using std::endl;
using std::string;

class Logger{
    private:
        static Logger* logger;
        int MACHINE_NUM;
        bool info_;
        bool debug_;
        std::mutex print;
        Logger():MACHINE_NUM(-1),info_(true),debug_(false){}
        void log(string type, string message, int thread_id){
            if(type == "m" && MACHINE_NUM != 0){
                return;
            }
            if(type == "o"){
                print.lock();
                cout<<message<<"\n";
                print.unlock();
                return;
            }
            print.lock();
            cout<<type<<" : Thread id: "<<thread_id<<", Machine: "<<MACHINE_NUM<<", Message: "<<message<<"\n"<<std::flush;
            print.unlock();
        }
    public:
        static Logger* get_logger(){
            return logger;
        }

        static void set_machine_num(int machine_num){
            logger->MACHINE_NUM = machine_num;
        }

        void info(string message){
            info( message, -999);
        }

        void warn(string message){
            warn( message, -999);
        }

        void debug(string message){
            debug(message, -999);
        }

        void print_master(string message){
            info( message, -999);
        }

        void error(string message){
            error(message, -999);
        }

        void enable_info(){
            info_ = true;
        }

        void disable_info(){
            info_ = false;
        }

        void disable_debug(){
            debug_ = false;
        }

        void enable_debug(){
            debug_ = true;
        }

        void info(string message, int thread_id){
            if(!info_){
                return;
            }
            log("INFO", message, thread_id);
        }

        void debug(string message, int thread_id){
            if(!debug_){
                return;
            }
            log("DEBUG", message, thread_id);
        }

        void print_master(string message, int thread_id){
            log("m", message, thread_id);
        }

        void output(string message){
            log("o", message, -998);
        }

        void warn(string message, int thread_id){
            log("WARNING", message, thread_id);
        }

        void error(string message, int thread_id){
            log("ERROR", message, thread_id);
        }

};

Logger* Logger::logger = new Logger();

#endif
