/*
 * Copyright (c) 2019 Parameswaran Raman, Sriram Srinivasan, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef DSMLR_UPDATER
#define DSMLR_UPDATER

#include <random>
#include <sstream>
#include <tbb/tbb.h>
#include <tbb/compat/thread>
#include "../Logger.hpp"
#include "../IUpdater.hpp"
#include "PrimalParam.hpp"
#include "MulticlassDataLoader.hpp"
#include "DSMLRDataOption.hpp"

// BUGBUG the definition of MPI_SIZE_T as MPI_UNIT64_T is somewhat arbitrary
#define MPI_SIZE_T MPI_UINT64_T
#define MPI_SCALAR MPI_DOUBLE

const scalar SCALAR_ZERO = static_cast<scalar>(0.0);
const scalar minus_infty = -std::numeric_limits<scalar>::max();

using std::string;
using rng_type = std::mt19937_64;

// Helper class for ai computation
class ai_plus{
  public:
    scalar ai_;
    scalar max_fx_;
    scalar fxy_;
    size_t yhat_;
    //vector<scalar> vec_xTwk;

    /*ai_plus(size_t K){
      reset(K);
      }*/

    void reset(size_t K){
      ai_ = SCALAR_ZERO;
      max_fx_ = minus_infty;
      fxy_ = minus_infty;
      yhat_ = 0;
      //vec_xTwk.resize(K);
      //std::fill(vec_xTwk.begin(), vec_xTwk.end(), 0.0);
    }
};

class DSMLRUpdater:public IUpdater{
  private:
    Logger *logger_;
    DSMLRDataOption* option_;
    MetaData* metadata_ = nullptr;
    MulticlassDataLoader* loader_ = nullptr;

    size_t min_row_index;
    size_t part_index;

    //scalar* local_params;
    int num_threads;
    int num_tasks;
    int task_id;
    int thread_id;
    size_t dim;
    size_t local_num_updates;
    int* col_update_counts;

    double train_sum_squared_error;
    int train_count_error;
    double test_sum_squared_error;
    int test_count_error;

    // Total number of train data points
    size_t N;

    // Total number of test data
    size_t Ntest;

    // Total number of features
    size_t D;

    // Total number of classes
    size_t K;

    // Number of classes assigned to this processor
    size_t local_K;

    // Number of train data points in this processor
    size_t local_N;

    // Number of test data points in this processor 
    size_t local_Ntest;

    // Figure out which classes we own
    // (needed during testing phase)
    size_t first_k = part_index * local_K;
    size_t last_k = std::min(first_k + local_K, K);

    // Threshold for exp calculation
    const scalar threshold = 2;

    // Helper methods for the updater 
    scalar compute_xtw_k(const Instance &xi, scalar* wk){
      scalar dot = SCALAR_ZERO;
      const size_t omega_i = xi.idx_.size();
      for(size_t it = 0; it < omega_i; ++it){
        const size_t j = xi.idx_[it];
        const scalar xij = xi.val_[it];
        const scalar wkj = wk[j];
        dot += wkj * xij;
      }
      return dot;
    }

    /*scalar compute_xtw_k_new(const Instance &xi, const size_t local_k, vector<IParam*> params){
      PrimalParam *p = (PrimalParam*) params[local_k];

    // get pointer to wk
    scalar* wk = p->get_param();
    scalar dot = SCALAR_ZERO;
    const size_t omega_i = xi.idx_.size();
    for(size_t it = 0; it < omega_i; ++it){
    const size_t j = xi.idx_[it];
    const scalar xij = xi.val_[it];
    const scalar wkj = wk[j];
    dot += wkj * xij;
    }
    return dot;
    }*/

    scalar compute_xtw_k_new(const Instance &xi, PrimalParam* p){
      // get pointer to wk
      scalar* wk = p->get_param();
      size_t local_k = p->get_col_index();
      scalar dot = SCALAR_ZERO;
      const size_t omega_i = xi.idx_.size();
      for(size_t it = 0; it < omega_i; ++it){
        const size_t j = xi.idx_[it];
        const scalar xij = xi.val_[it];
        const scalar wkj = wk[j];
        dot += wkj * xij;
      }
      return dot;
    }

    inline scalar* getparam_val(vector<char>& buf, size_t index){
      return reinterpret_cast<scalar *>(&buf[index*sizeof(scalar)]);
    }

    inline void setparam_val(vector<char>& buf, size_t index, scalar value){
      scalar* scalar_ptr = reinterpret_cast<scalar *>(&buf[index*sizeof(scalar)]);
      *scalar_ptr = value;
      return;
    }

    // Compute the gradient of exp(x) in a safe way
    // We expect the threshold to be a small value close to 0.
    inline scalar safe_exp_grad(scalar x){
      return exp(std::min(x, threshold));
    }

    inline size_t get_local_idx(const Instance &xi){
      //return xi.id_/numtasks;
      return xi.id_/num_parts;
    }

  public:
    DSMLRUpdater(DSMLRDataOption* option, 
        MetaData* metadata, 
        MulticlassDataLoader* data_loader): option_(option), 
    metadata_(metadata), 
    loader_(data_loader){
      N = metadata_->global_train_num_instances_;
      Ntest = metadata_->global_test_num_instances_;
      D = metadata_->global_num_features_;
      K = metadata_->global_num_classes_;
      data_ = new Data();

    }

    ~DSMLRUpdater(){
      delete[] col_update_counts;
      delete[] local_params;
      delete data_;
      vec_ai_.clear();
      vec_ai_test_.clear();
    }

    Data* data_ = nullptr;
    scalar* local_params;
    vector<ai_plus> vec_ai_;
    vector<ai_plus> vec_ai_test_;
    size_t num_parts;

    // Note that ai = \sum_{k so far} exp( <w_k, x_i> - max_xTwk). This
    // function maintains the invariant on ai. Also it ensures that we
    // never compute exp on an positive number, thus avoiding overflow.
    // 
    // case 1:
    // max_xTwk_local > max_xTwk
    // ai = ai*exp(max_xTwk - max_xTwk_local) + 
    //                \sum_{k in this block} exp(<w_k, x_i> - max_xTwk_local)
    // Update max_xTwk = max_xTwk_local
    // 
    // case 2:
    // max_xTwk_local <= max_xTwk
    // ai = ai + \sum_{k in this block} exp(<w_k, x_i> - max_xTwk)
    //

    //BUGBUG: making this public for debugging (change it back)
/*    void prepare_local_ai(const Instance &xi, ai_plus &ai, scalar *wk, size_t k){

      const label_t yi = xi.label_;
      scalar max_fx = minus_infty;
      size_t argmax_local_k = 0;

      scalar xTwk;
      xTwk =  compute_xtw_k(xi, wk);
      ai.vec_xTwk[k] = xTwk;

      if(xTwk > max_fx){
        max_fx = xTwk;
        argmax_local_k = k;
      }

      if(yi == k)
        ai.fxy_ = xTwk;

      if(max_fx > ai.max_fx_){
        ai.ai_ *= exp(ai.max_fx_ - max_fx);
        ai.max_fx_ = max_fx;
        ai.yhat_ = argmax_local_k;
      }

      ai.ai_ += exp(ai.vec_xTwk[k] - ai.max_fx_);

      return;
    }
*/
    // Note: this is the version without delta-update
    // (this will only work with task-scheduling, ie. all classes updated
    // equal # of times per iteration)
    void compute_local_ai(const Instance &xi, ai_plus &ai, PrimalParam* param){
      size_t local_k = param->get_col_index(); 
      const label_t yi = xi.label_;
      scalar xTwk;

      scalar max_fx = minus_infty;
      size_t argmax_local_k = 0;

      //        std::cout << "ai.ai_: " << ai.ai_ << std::endl;

      xTwk = compute_xtw_k_new(xi, param);
      //ai.vec_xTwk[local_k] = xTwk;

      if(xTwk > max_fx){
        max_fx = xTwk;
        argmax_local_k = local_k;
      }
      if(yi == local_k)
        ai.fxy_ = xTwk;

      if(max_fx > ai.max_fx_){
        ai.ai_ *= exp(ai.max_fx_ - max_fx);
        ai.max_fx_ = max_fx;
        ai.yhat_ = argmax_local_k;
      }

      //        std::cout << "maxfx: " << ai.max_fx_ << std::endl;
      //        std::cout << "vec xtwk: ";
      //        std::cout << " " << ai.vec_xTwk[local_k]; 
      //ai.ai_ += exp(ai.vec_xTwk[local_k] - ai.max_fx_);
      ai.ai_ += exp(xTwk - ai.max_fx_);
      //        std::cout << "\n\n";
      return;
    }

    // Note: this is the version **WITH** delta-update
    // (this should work with task-scheduling as well as task-stealing, 
    // ie. classes getting updated unequal # of times per iteration)
/*    void compute_local_ai_delta(const Instance &xi, ai_plus &ai, PrimalParam* param){
      size_t local_k = param->get_col_index(); 
      const label_t yi = xi.label_;
      scalar xTwk;
      scalar delta;

      scalar max_fx = minus_infty;
      size_t argmax_local_k = 0;

      //        std::cout << "ai.ai_: " << ai.ai_ << std::endl;

      xTwk = compute_xtw_k_new(xi, param);
      //delta = xTwk - ai.vec_xTwk[local_k];
      
      //if(delta > max_fx){
      if(xTwk > max_fx){
        max_fx = xTwk;
        argmax_local_k = local_k;
      }
      if(yi == local_k)
        ai.fxy_ = xTwk;

      if(max_fx > ai.max_fx_){
        ai.ai_ *= exp(ai.max_fx_ - max_fx);
        ai.max_fx_ = max_fx;
        ai.yhat_ = argmax_local_k;
      }

      //        std::cout << "maxfx: " << ai.max_fx_ << std::endl;
      //        std::cout << "vec xtwk: ";
      //        std::cout << " " << ai.vec_xTwk[local_k]; 
      //ai.ai_ += exp(ai.vec_xTwk[local_k] - ai.max_fx_);
      ai.ai_ += exp(xTwk - ai.max_fx_) - ai.vec_xTwk[local_k];
      //        std::cout << "\n\n";
      //if (xi.id_ < 3){
      //std::cout << "i: " << xi.id_ << ", k: " << local_k << ", xTwk: " << xTwk 
      //          << ", ai.vec_xtwk: " << ai.vec_xTwk[local_k] 
      //          << ", ai.max_fx_: " << ai.max_fx_ << ", ai.ai_: " << ai.ai_ << std::endl;
      //}
      ai.vec_xTwk[local_k] = exp(xTwk - ai.max_fx_);
      //if (xi.id_ < 3)
      //std::cout << "for k: " << local_k << ", setting ai.vec_xTwk to: " << ai.vec_xTwk[local_k] << std::endl;
      return;
    }
*/
    void initialize(int task_id, int num_tasks, int thread_id, int num_threads){
      logger_ = Logger::get_logger();
      this->task_id = task_id;
      this->thread_id = thread_id;
      this->num_tasks = num_tasks;
      this->num_threads = num_threads;

      // copy some essential parameters explicitly
      local_num_updates = 0;
      train_sum_squared_error = 0.0;
      train_count_error = 0;
      test_sum_squared_error = 0.0;
      test_count_error = 0;

      num_parts = num_tasks * num_threads;
      part_index = task_id * num_threads + thread_id;
      size_t min_instance_index = part_index;

      auto msg = boost::format("rank: %d, thread_index: %d, part_index: %d") % task_id % thread_id % part_index;
      logger_->info(msg.str(), thread_id);

      ///////////////////////////////////////////////////
      // Each thread has to read its part of data now  //
      ///////////////////////////////////////////////////

      // load train
      // BUGBUG: add error check flag after loading
      // see MFUpdater.hpp
      logger_->info("going to read local data ", thread_id);
      loader_->prepare_for_local_read(metadata_, data_, num_parts, part_index);
      logger_->info("local vector size: " + to_string(data_->local_train_instances_.size()), thread_id);
      loader_->read_data(metadata_, data_, num_parts, 
          option_->train_file_name_, min_instance_index);

      // load test 
      loader_->prepare_for_local_read(metadata_, data_, num_parts, 
          part_index, false);
      loader_->read_data(metadata_, data_, num_parts, 
          option_->test_file_name_, min_instance_index, false);

      //BUGBUG: Move this into loader
      //This wont work
      //metadata_->global_train_num_nnz_col_.resize(D);
      /*MPI_Allreduce(&data_->local_train_num_nnz_col_[0], 
        &metadata_->global_train_num_nnz_col_[0], 
        D, MPI_SIZE_T, MPI_SUM, MPI_COMM_WORLD);*/
      /*for(size_t i=0; i < D; i++)
        metadata_->global_train_num_nnz_col_[i] = data_->local_train_num_nnz_col_[i];
        data_->local_train_num_nnz_col_.clear();*/

      local_N = data_->local_train_num_instances_;
      local_Ntest = data_->local_test_num_instances_;

      //init local params here
      local_params = new scalar[local_N];
      std::fill(local_params, local_params+local_N, log(1.0/K));

      // initialize vec_ai and vec_ai_test 
      vec_ai_.resize(local_N);
      vec_ai_test_.resize(local_Ntest);

      for(size_t i = 0; i < local_N; i++)
        vec_ai_[i].reset(K);
      for(size_t i = 0; i < local_Ntest; i++)
        vec_ai_test_[i].reset(K);

      col_update_counts = new int[K];
      std::fill_n(col_update_counts, K, 0);

    }
    void finalize(){
      // print to the file
      if (option_->output_path_local_.length() > 0) {

        ofstream::openmode mode = (part_index % num_threads == 0) ? ofstream::out : (ofstream::out | ofstream::app);
        ofstream ofs(option_->output_path_local_ + boost::lexical_cast<string>(task_id), mode);

        logger_->info("min_row_index: " + to_string(min_row_index), thread_id);

        for (int i=0; i < local_N; i++) {
          ofs << local_params[i];
          ofs << endl;
        }
        ofs.close();

      }

    }

    int get_num_data_for_col(int col_index){
      //return train_col_offset[col_index+1] - train_col_offset[col_index];

      //BUGBUG: verify this
      return metadata_->get_omega_j(col_index);
    }

    void accumulate(vector<IUpdater*> *updaters){
    }

    long long get_num_updates(){
      return local_num_updates;
    }

    int get_train_count_error(){
      return train_count_error;
    }

    int get_test_count_error(){
      return test_count_error;
    }

    double get_train_sum_squared_error(){
      return train_sum_squared_error;
    }

    double get_test_sum_squared_error(){
      return test_sum_squared_error;
    }


    size_t iter_num_ = -1;

    void update(IParam *param){
      // update only one col of W (ie: w_k)
      PrimalParam *p = (PrimalParam*) param;

      // get col index of wk
      const size_t k = p->get_col_index();

      // get pointer to wk
      scalar* wk = p->get_param();

      //BUGBUG: verify this 
      scalar eta = option_->eta_/sqrt(std::max(1.0, static_cast<double>(iter_num_)));
      //scalar eta = option_->eta_/sqrt(std::max(1.0, static_cast<scalar>(col_update_counts[k])));
      //const scalar eta = option_->eta_ * 1.5 / 
      //  (1.0 + option_->decay_rate_ * pow(col_update_counts[k] + 1, 1.5));


      bool succeed = false;

      if(option_->shuffle_){
        data_->shuffle_train_instances_order();
      }
      int repeat = 1;
      if (iter_num_ == 0) {
          repeat = option_->inner_repeat_;
      }
      for(int rep = 0 ; rep < repeat ; rep++){
      succeed =  data_->apply_to_train_block(
          [&](Instance & instance)->bool{

          const Instance &xi = instance;
          const size_t local_i = get_local_idx(xi);
          scalar &bi = local_params[local_i];

          scalar dot = compute_xtw_k(instance, wk);
          const label_t yi = xi.label_;
          scalar sg_w = (yi != k) ? 0.0 : -1.0/N;

          sg_w += safe_exp_grad(dot+bi)/N;

          // w update 
          for(size_t it = 0; it < xi.idx_.size(); ++it){
            const size_t j = xi.idx_[it];
            const scalar xij = xi.val_[it];
            const scalar omega_j = metadata_->get_omega_j(j);
            scalar &wkj = wk[j];
            wkj -= eta * (option_->lambda_ * wkj/omega_j + sg_w * xij);
          }

          local_num_updates++;
          return true;
          });
      }

      succeed =  data_->apply_to_train_block(
          [&](Instance & instance)->bool{

          const Instance &xi = instance;
          const size_t local_i = get_local_idx(xi);//.id_/num_parts;
          //const size_t local_i = xi.id_;

          //DEBUGGING
          /*vec_ai_[local_i].vec_xTwk[k] = 0.0;
          if(xi.id_ < 3) 
            std::cout << "in update(), for i: " << xi.id_ << ", k: " << k << ", vec_xTwk[local_k]: " << vec_ai_[local_i].vec_xTwk[k] << std::endl;
          */

          // update vec_ai (used during training) 
          compute_local_ai(instance, vec_ai_[local_i], p);
          //compute_local_ai_delta(instance, vec_ai_[local_i], p);

          return true;
          });

      col_update_counts[k]++;

      //BUGBUG: do you need to keep track of local_updates as well?
      //See MFUpdater.cpp
    }

    void prepare_update(){
      iter_num_++;
      for(size_t i = 0; i < local_N; i++)
        vec_ai_[i].reset(K);
      for(size_t i = 0; i < local_Ntest; i++)
        vec_ai_test_[i].reset(K);

    }

    void post_update(){
         //bi update happens here
        for(size_t local_i = 0; local_i < data_->local_train_num_instances_; ++local_i){
          scalar &bi = local_params[local_i];
          bi = -log(vec_ai_[local_i].ai_) - vec_ai_[local_i].max_fx_;
          //if(local_i < 3)
          //std::cout << "now computing bi for i: " << local_i << " using, vec_ai[local_i].ai_: " << vec_ai_[local_i].ai_ << ", maxfx: " << vec_ai_[local_i].max_fx_ << std::endl;
          // std::cout << "bi for i: " << local_i << " = " << bi << ", iter: " << i << std::endl;
        }
   
    }

    void prepare_test(){
      train_sum_squared_error = 0.0;
      train_count_error = 0;
      test_sum_squared_error = 0.0;
      test_count_error = 0;

      for(size_t i = 0; i < local_N; i++)
        vec_ai_[i].reset(K);
      for(size_t i = 0; i < local_Ntest; i++)
        vec_ai_test_[i].reset(K);

    }

    void test(IParam* iparam){
      PrimalParam * param = (PrimalParam*) iparam;
      size_t iter_num = -1;

      // Compute vec_ai and vec_ai_test which are needed to compute statistics

      // on training data
      bool success = data_->apply_to_refresh_block(
          [&](Instance & instance)->bool{

          const Instance &xi = instance;
          const size_t local_i = get_local_idx(xi);
          compute_local_ai(instance, vec_ai_[local_i], param);
          //          std::cout << "prep_local_ai, ai: " << vec_ai_[local_i].ai_ << std:: endl;
          return true;
          });
      //if(!success)
      //  return false;

      // on test data
      success = data_->apply_to_test_block(
          [&](Instance & instance)->bool{

          const Instance &xi = instance;
          const size_t local_i = get_local_idx(xi);
          compute_local_ai(instance, vec_ai_test_[local_i], param);
          return true;
          });
      //if(!success)
      //  return false;
    }


};

#endif
