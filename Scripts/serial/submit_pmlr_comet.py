import os
import subprocess

queue_name = "ml"

output_prefix = os.getcwd()
data_dir = output_prefix + "/../../Data"

datasets = [
		("CLEF", 
		 data_dir + "/CLEF/train2.txt", 
		 data_dir + "/CLEF/test2.txt", 
		 data_dir + "/CLEF/vali2.txt",
		 data_dir + "/CLEF/model.txt",
         0.0001,
		 "noskip"
                ),
		("NEWS20", 
		 data_dir + "/NEWS20/train2.txt", 
		 data_dir + "/NEWS20/test2.txt", 
		 data_dir + "/NEWS20/vali2.txt",
		 data_dir + "/NEWS20/model.txt",
		 8.8810e-05,
         "noskip"
                ),
		("LSHTC1-small", 
		 data_dir + "/lshtc1-small/train4.txt", 
		 data_dir + "/lshtc1-small/test4.txt", 
		 data_dir + "/lshtc1-small/vali4.txt",
		 data_dir + "/lshtc1-small/model.txt",
         2.2406e-07,
		 "noskip"
                ),
		("wine", 
		 data_dir + "/wine/train2.txt", 
		 data_dir + "/wine/test2.txt", 
		 data_dir + "/wine/vali2.txt",
		 data_dir + "/wine/model.txt",
         0.01,
		 "skip"
                ),
		("glass", 
		 data_dir + "/glass/train2.txt", 
		 data_dir + "/glass/test2.txt", 
		 data_dir + "/glass/vali2.txt",
		 data_dir + "/glass/model.txt",
         0.01,
		 "skip"
                )
]

iters = 100000
walltime = 12
numnodes = 1
numcpus = 1

code_dir = output_prefix + "/../../../mlr/Code/pmlr"
subs_dir = output_prefix + "/auto_subs"
logs_dir = output_prefix + "/logs"

total_jobs = 0
submit = True 

for dataset, train, test, valid, model, reg, skip_flag in datasets:
    if skip_flag == "skip":
	continue
    
    task_name = "pmlr_%s" % (dataset)
    
    if not os.path.exists(subs_dir):
        os.makedirs(subs_dir)
    if not os.path.exists(logs_dir):
        os.makedirs(logs_dir)
    sub_fname = subs_dir + "/%s.sub" % (task_name)
    prog_file = logs_dir + "/%s.progress" % (task_name)
    model_file = logs_dir + "/%s.model" % (task_name)
    
    ofile = open(sub_fname, "w")
    
    ofile.write("#!/bin/bash \n")
    ofile.write("#SBATCH --job-name=%s \n" % (task_name))
    ofile.write("#SBATCH --partition compute \n")
    ofile.write("#SBATCH --output=%s \n" % (prog_file))
    ofile.write("#SBATCH --nodes=%d \n" % (numnodes))
    ofile.write("#SBATCH --ntasks-per-node=%d \n" % (numcpus))
    ofile.write("#SBATCH --export=ALL \n")
    ofile.write("#SBATCH -t %d:00:00 \n\n" % (walltime))
    ofile.write("export MODULEPATH=/share/apps/compute/modulefiles:/share/apps/compute/modulefiles/mpi/.gnu:/opt/modulefiles/applications/.intel:/opt/modulefiles/mpi/.intel:/share/apps/compute/modulefiles:/opt/modulefiles/mpi:/opt/modulefiles/compilers:/opt/modulefiles/applications:/usr/share/Modules/modulefiles:/etc/modulefiles \n\n")
    
    ofile.write("module purge \n")
    ofile.write("module load gnu/4.9.2 \n")
    ofile.write("module load gnutools \n")
    ofile.write("module load mvapich2_ib \n")
    ofile.write("module load cmake \n")
    ofile.write("module load boost \n")
    ofile.write("module load intel/2016.3.210 \n")
    ofile.write("module list \n\n")
    
    ofile.write("export Boost_INCLUDE_DIR=/opt/boost/gnu/mvapich2_ib/include \n")
    ofile.write("export MV2_ENABLE_AFFINITY=0 \n")
    ofile.write("export CC=`which gcc` \n")
    ofile.write("export CXX=`which g++` \n")
    ofile.write("echo $CC \n")
    ofile.write("echo $CXX \n\n")

    ofile.write(code_dir + "/pmlr --train %s --test %s -l %.4e -m %d \n" % (train, test, reg, iters));
    ofile.write("\n")
    ofile.close()
    
    qsub_command = "sbatch %s" % sub_fname
    total_jobs = total_jobs + 1
        
    if submit == True:
        p = subprocess.Popen(qsub_command, shell=True)
        p.communicate()
        print sub_fname + " submitted to " + queue_name
    else:
        print sub_fname + " created (not yet submitted) for " + queue_name
        
print "Total jobs: %d" % (total_jobs) 
