
How to run DSMLR Sync:
======================
$ ./dsmlr --help
supported options:
  --help                    produce help message
  --seed arg (=12345)       seed value of random number generator
  --max_iter arg (=100)     maximum number of iterations
  --eval_period arg (=1)    frequency of evaluation on test data
  -l [ --lambda ] arg (=1)  regularization parameter
  -e [ --eta ] arg (=0.001) learning rate
  --solver arg (=1)         solver type (1=1 delay, 2=0 delay, 3=mixed delay,
                            4=0 delay medium greedy)
  --smart_init arg (=0)     enable smart initialization?
  --inner_repeat arg (=1)   How many rounds of SGD per inner epoch
  --bs_period arg (=1)      frequency of bulk synchronization
  --train arg               file name of training data txt files
  --test arg                file name of testing data txt files
  --chunk_size arg (=100)   how many parameters to communicate per MPI
                            send/receive call

Examples:
========

Below are examples of running dsmlr on the three small datasets: *CLEF*, *NEWS20*, *LSHTC1-small*.

$ mpirun -np 1 ./dsmlr --train ../../Data/CLEF/train2.txt --test ../../Data/CLEF/test2.txt --meta ../../Data/CLEF/meta.txt -l 0.0001 -e 100 --max_iter 10000 --bs_period 1 --eval_period 1 --seed 1987 --solver 1 | grep grep

$ mpirun -np 1 ./dsmlr --train ../../Data/NEWS20/train2.txt --test ../../Data/NEWS20/test2.txt --meta ../../Data/NEWS20/meta.txt -l 8.8810e-05 -e 10000 --max_iter 10000 --bs_period 1 --eval_period 1 --seed 1987 --solver 1 | grep grep

$ mpirun -np 1 ./dsmlr --train ../../Data/LSHTC1-small/train2.txt --test ../../Data/LSHTC1-small/test2.txt --meta ../../Data/LSHTC1-small/meta.txt -l 2.2406e-07 -e 10000 --max_iter 10000 --bs_period 1 --eval_period 1 --seed 1987 --solver 1 | grep grep


