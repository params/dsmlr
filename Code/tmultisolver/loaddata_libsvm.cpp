/*
 * Copyright (c) 2019 Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#include "loaddata_libsvm.hpp"
#include <cassert>
#include <algorithm>

// The job of this function is to calculate:
// 1. number of lines (user->m)
// 2. number of nnz for all lines (user->nnz_array)
// 3. number of features (user->maxnnz)
// 4. check if minimum index is 0 or 1
PetscErrorCode parse_file(FILE* input, AppCtx_file* fuser) {
  PetscErrorCode info;
  size_t         len = 0;       // buffer length
  int            llen = 0;      // number of characters in a line
  char*          line = 0;
  PetscInt       m_guess = 128; 
  
  PetscFunctionBegin;
  fuser->maxlen = 0;
  fuser->nnz_array = 0;
  // Guess the number of training examples in my process
  info = PetscMalloc(m_guess * sizeof(PetscInt), &fuser->nnz_array);CHKERRQ(info);

  fuser->dim = -1;
  fuser->m = 0;
  fuser->maxnnz = 0;
  // parse input file line by line
  while ((llen = getline(&line, &len, input)) !=  -1) {

    if (llen > fuser->maxlen)
      fuser->maxlen = llen;

    // Read and parse label
    char *plabel = strtok(line, " \t");
    PetscInt label = (PetscInt) strtol(plabel, NULL, 10);

    PetscInt nnz = 0;
    // Now process the rest of the line
    while (1) {  
      // parse id:val pairs of feature
      char* pidx = strtok(NULL, ":");
      char* pval = strtok(NULL, " \t");

      if (pval == NULL)
        break;
      PetscInt idx = (PetscInt) strtol(pidx, NULL, 10);
      if (idx > fuser->dim)
        fuser->dim = idx;
      nnz++;
    }

    if (fuser->m >=  m_guess) {
      PetscInt* nnz_array_temp = 0;
      info = PetscMalloc(m_guess * 2 * sizeof(PetscInt), &nnz_array_temp); CHKERRQ(info);
      info = PetscMemcpy(nnz_array_temp, fuser->nnz_array, m_guess*sizeof(PetscInt)); CHKERRQ(info);
      info = PetscFree(fuser->nnz_array); CHKERRQ(info);
      fuser->nnz_array = nnz_array_temp;
      m_guess *= 2;
    }

    // Remember maximum number of nonzeros in any row 
    if (nnz > fuser->maxnnz)
      fuser->maxnnz = nnz;
    fuser->nnz_array[fuser->m] = nnz;

    fuser->m++;
  }

  if (line)    free(line);
  
  // dimensions = max idx + 1
  fuser->dim++;

  // PetscPrintf(PETSC_COMM_SELF, "dim = %D\n", fuser->dim);

  // adjust to finalize the array of size
  PetscInt* nnz_array_temp;
  info = PetscMalloc(fuser->m * sizeof(PetscInt), &nnz_array_temp); CHKERRQ(info);
  info = PetscMemcpy(nnz_array_temp, fuser->nnz_array, fuser->m*sizeof(PetscInt)); CHKERRQ(info);
  info = PetscFree(fuser->nnz_array); CHKERRQ(info);
  fuser->nnz_array = nnz_array_temp;  

  PetscFunctionReturn(0);
}


// Given a line, retrieve the label, and the idx:val pair of features
void parse_line(char* line, 
                PetscInt* idxs, 
                PetscScalar* vals, 
                PetscScalar* label, 
                int m, 
                AppCtx_file* fuser) {
  // Read the label
  char *plabel = strtok(line, " \t");
  *label = strtol(plabel, NULL, 10);
  // process the line
  PetscInt nnz = 0;
  while (1) {
    char* pidx = strtok(NULL, ":");
    char* pval = strtok(NULL, " \t");
    if (pval == NULL)
      break;
    vals[nnz] = strtod(pval, NULL);
    idxs[nnz] = (PetscInt) strtol(pidx, NULL, 10);
    nnz++;
  }
  assert(fuser->nnz_array[m] == nnz);
}


// For uniprocessor, fill in the values of the data matrix and label vector
// only take lines from begin to end-1
PetscErrorCode assemble_matrix(FILE* input, 
                               PetscInt begin, 
                               PetscInt end, 
                               AppCtx_file* fuser) {  
  PetscErrorCode info;
  size_t         len = fuser->maxlen;
  char*          line = (char *) malloc(len*sizeof(char));
  PetscInt       m = 0;
  PetscInt       ii = 0;
  PetscScalar    label = 0;
  PetscScalar*   vals = 0;
  PetscInt*      idxs = 0;

  PetscFunctionBegin;

  info = PetscMalloc(fuser->maxnnz*sizeof(PetscScalar), &vals);CHKERRQ(info);
  info = PetscMalloc(fuser->maxnnz*sizeof(PetscInt), &idxs);CHKERRQ(info);

  while (getline(&line, &len, input) !=  -1) {
    //skip the lines for which this processor is not responsible
    if (m < begin) {
      m++;  continue;
    } else if (m >=  end) 
      break;
    parse_line(line, idxs, vals, &label, m, fuser);
    info = VecSetValues(*fuser->labels, 1, &m, &label, INSERT_VALUES);CHKERRQ(info);
    info = MatSetValues(*fuser->data, 1, &m, fuser->nnz_array[m], idxs, vals, INSERT_VALUES);CHKERRQ(info);
    m++;    ii++;
  }

  free(line);
  info = PetscFree(vals); CHKERRQ(info);
  info = PetscFree(idxs); CHKERRQ(info);

  info = VecAssemblyBegin(*fuser->labels);CHKERRQ(info);
  info = VecAssemblyEnd(*fuser->labels);CHKERRQ(info);
  info = MatAssemblyBegin(*fuser->data, MAT_FINAL_ASSEMBLY);CHKERRQ(info);
  info = MatAssemblyEnd(*fuser->data, MAT_FINAL_ASSEMBLY);CHKERRQ(info);
  PetscFunctionReturn(0);
}


// For uniprocessor, create and fill in the data matrix
PetscErrorCode fill_arrays_uni(FILE* input, AppCtx_file* fuser) {
  PetscErrorCode info;
  
  PetscFunctionBegin;
  // Allocate space for the labels
  info = VecCreate(PETSC_COMM_WORLD, fuser->labels);CHKERRQ(info);
  info = VecSetSizes(*(fuser->labels), PETSC_DECIDE, fuser->m);CHKERRQ(info);
  info = VecSetFromOptions(*(fuser->labels));CHKERRQ(info);

  // Allocate space for the data matrix
  info = MatCreate(PETSC_COMM_WORLD, fuser->data);CHKERRQ(info);
  info = MatSetSizes(*(fuser->data), PETSC_DECIDE, PETSC_DECIDE, fuser->m, fuser->dim);CHKERRQ(info);
  info = MatSetFromOptions(*(fuser->data));CHKERRQ(info);

  info = MatSetType(*(fuser->data), MATSEQAIJ);CHKERRQ(info);
  info = MatSeqAIJSetPreallocation(*(fuser->data), 0, fuser->nnz_array); CHKERRQ(info);

  assemble_matrix(input, 0, fuser->m, fuser);
  PetscFunctionReturn(0);
}


// For multi-processor, collect statistics (nnz) related to diagonal and off-diagonal
PetscErrorCode reparse_file(FILE* input, 
                            PetscInt* diag_nnz, 
                            PetscInt* offdiag_nnz, 
                            AppCtx_file* fuser) {
  PetscErrorCode info;
  size_t len = fuser->maxlen;
  char* line = (char *) malloc(len*sizeof(char));

  PetscFunctionBegin;

  PetscInt begin, end, localsize = PETSC_DECIDE;
  PetscSplitOwnership(PETSC_COMM_WORLD, &localsize, &fuser->m);
  MPI_Scan(&localsize, &end, 1, MPIU_INT, MPI_SUM, PETSC_COMM_WORLD);
  begin = end - localsize;

  PetscInt cbegin, cend, clocalsize = PETSC_DECIDE;
  PetscSplitOwnership(PETSC_COMM_WORLD, &clocalsize, &fuser->dim);
  MPI_Scan(&clocalsize, &cend, 1, MPIU_INT, MPI_SUM, PETSC_COMM_WORLD);
  cbegin = cend - clocalsize;

  PetscInt m = 0;
  PetscInt ii = 0;

  for (PetscInt i = 0; i<localsize; i++)
    diag_nnz[i] = 0;

  while (getline(&line, &len, input) !=  -1) {
    //skip the lines for which this processor is not responsible
    if (m < begin) {
      m++;  
      continue;
    } else if (m >=  end) 
      break;
    
    // Read and ignore the label
    strtok(line, " \t");
    // Now process the rest of the line
    while (1) {
      char* pidx = strtok(NULL, ":");
      char* pval = strtok(NULL, " \t");

      if (pval == NULL)
        break;
      PetscInt idx = (PetscInt) strtol(pidx, NULL, 10);
      if (idx >= cbegin && idx<cend)
        diag_nnz[ii]++;
    }

    offdiag_nnz[ii] = fuser->nnz_array[m] - diag_nnz[ii];
    m++;    ii++;
  }
  if (line)    
    free(line);

  PetscFunctionReturn(0);
}


PetscErrorCode fill_arrays_parallel(FILE* input, 
                                    AppCtx_file* fuser) {
  PetscErrorCode info;
  
  PetscFunctionBegin;
  // Create labels vector 
  info = VecCreate(PETSC_COMM_WORLD, fuser->labels);CHKERRQ(info);
  info = VecSetSizes(*(fuser->labels), PETSC_DECIDE, fuser->m);CHKERRQ(info);
  info = VecSetFromOptions(*(fuser->labels));CHKERRQ(info);
  info = VecSetType(*(fuser->labels), VECMPI);

  // Create data matrix 
  info = MatCreate(PETSC_COMM_WORLD, fuser->data);CHKERRQ(info);
  info = MatSetSizes(*(fuser->data), PETSC_DECIDE, PETSC_DECIDE, fuser->m, fuser->dim);CHKERRQ(info);
  info = MatSetFromOptions(*(fuser->data));CHKERRQ(info);
  info = MatSetType(*(fuser->data), MATMPIAIJ); CHKERRQ(info);    

  // Allocate space for the data 
  PetscInt begin, end, localsize = PETSC_DECIDE;
  PetscSplitOwnership(PETSC_COMM_WORLD, &localsize, &fuser->m);
  MPI_Scan(&localsize, &end, 1, MPIU_INT, MPI_SUM, PETSC_COMM_WORLD);
  begin = end - localsize;

  PetscInt *diag_nnz, *offdiag_nnz;
  info = PetscMalloc(localsize*sizeof(PetscInt), &diag_nnz);CHKERRQ(info);
  info = PetscMalloc(localsize*sizeof(PetscInt), &offdiag_nnz);CHKERRQ(info);

  reparse_file(input, diag_nnz, offdiag_nnz, fuser);
  rewind(input);
  info = MatMPIAIJSetPreallocation(*(fuser->data), 0, diag_nnz, 0, offdiag_nnz);CHKERRQ(info);
  info = PetscFree(diag_nnz); CHKERRQ(info);
  info = PetscFree(offdiag_nnz); CHKERRQ(info);

  assemble_matrix(input, begin, end, fuser);
  PetscFunctionReturn(0);
}


#undef __FUNCT__
#define __FUNCT__ "LoadDataLibsvm"
// Load from data files in LIBSVM format
// Always add bias
PetscErrorCode LoadDataLibsvm(AppCtx *user) {
  PetscErrorCode    info;
  PetscBool         flag;
  AppCtx_file       fuser; 
  AppCtx_file       tfuser; 

  PetscInt       imin, imax;
  PetscReal      cmin, cmax;
  
  FILE              *data_file;
  FILE              *tdata_file;

  PetscFunctionBegin;

  // First parse the training and test data file to collect statistics
  fuser.data = &fuser.data_parts;
  fuser.labels = &user->labels;
  tfuser.data = &tfuser.data_parts;
  tfuser.labels = &user->tlabels;

  // Master node parses the training data file to collect statistics
  // PetscPrintf(PETSC_COMM_WORLD, "Start parsing training data\n");
  info = PetscFOpen(PETSC_COMM_SELF, user->data_path, "r", &data_file); CHKERRQ(info); 
  fuser.nnz_array = NULL;
  if (user->rank == 0) {    
    parse_file(data_file, &fuser);    
    rewind(data_file);   // Set file pointer to beginning of file
  }
    
  // Master node parses the test data file to collect statistics
  // PetscPrintf(PETSC_COMM_WORLD, "Start parsing test data\n");
  tfuser.nnz_array = NULL;
  info = PetscFOpen(PETSC_COMM_SELF, user->tdata_path, "r", &tdata_file); CHKERRQ(info);
  if (user->rank == 0) {   
    parse_file(tdata_file, &tfuser);    
    rewind(tdata_file);   // Set file pointer to beginning of file
      
    // Set the final dimension to be the larger of the two dimensions    
    tfuser.dim = fuser.dim = std::max(fuser.dim, tfuser.dim);
  }
    
  // Propagate the statistics of the dataset
  MPI_Bcast(&tfuser.dim, 1, MPIU_INT, 0, PETSC_COMM_WORLD);  
  MPI_Bcast(&tfuser.m, 1, MPIU_INT, 0, PETSC_COMM_WORLD);
  MPI_Bcast(&tfuser.maxnnz, 1, MPIU_INT, 0, PETSC_COMM_WORLD);    // slave nodes need it to load data
  MPI_Bcast(&tfuser.maxlen, 1, MPIU_INT, 0, PETSC_COMM_WORLD);    // slave nodes need it to load data
  if (user->rank != 0) {
    info = PetscMalloc(tfuser.m * sizeof(PetscInt), &tfuser.nnz_array); CHKERRQ(info);
  }
  MPI_Bcast(tfuser.nnz_array, tfuser.m, MPIU_INT, 0, PETSC_COMM_WORLD);

  MPI_Bcast(&fuser.dim, 1, MPIU_INT, 0, PETSC_COMM_WORLD);  
  MPI_Bcast(&fuser.m, 1, MPIU_INT, 0, PETSC_COMM_WORLD);
  MPI_Bcast(&fuser.maxnnz, 1, MPIU_INT, 0, PETSC_COMM_WORLD);    // slave nodes need it to load data
  MPI_Bcast(&fuser.maxlen, 1, MPIU_INT, 0, PETSC_COMM_WORLD);    // slave nodes need it to load data
  if (user->rank != 0) {
    info = PetscMalloc(fuser.m * sizeof(PetscInt), &fuser.nnz_array); CHKERRQ(info);
  }
  MPI_Bcast(fuser.nnz_array, fuser.m, MPIU_INT, 0, PETSC_COMM_WORLD);

  // Really start to load the training data
  // PetscPrintf(PETSC_COMM_WORLD, "Start loading the training data\n");
  if (user->size == 1)   // uniprocessor
    fill_arrays_uni(data_file, &fuser);
  else                      // multiprocessor
    fill_arrays_parallel(data_file, &fuser);
  info = PetscFree(fuser.nnz_array); CHKERRQ(info);
  PetscFClose(PETSC_COMM_SELF, data_file); 

  // Really load the test data
  // PetscPrintf(PETSC_COMM_WORLD, "Start loading the test data\n");
  if (user->size == 1)   // uniprocessor
    fill_arrays_uni(tdata_file, &tfuser);
  else                      // multiprocessor
    fill_arrays_parallel(tdata_file, &tfuser);

  info = PetscFree(tfuser.nnz_array); CHKERRQ(info);
  PetscFClose(PETSC_COMM_SELF, tdata_file); 

  info = VecMin(user->labels, &imin, &cmin);CHKERRQ(info);
  info = VecMax(user->labels, &imax, &cmax);CHKERRQ(info);
  user->nclasses = cmax - cmin + 1;
  
  if (cmin != 0) {
    info = VecShift(user->labels, -cmin);CHKERRQ(info);
    info = VecShift(user->tlabels, -cmin);CHKERRQ(info);
  }

  info = MatGetSize(fuser.data_parts, &user->m, &user->dim); CHKERRQ(info);
  info = MatCreateMAIJ(fuser.data_parts, user->nclasses, &user->data);CHKERRQ(info);

  info = MatGetSize(tfuser.data_parts, &user->tm, &user->tdim); CHKERRQ(info);
  info = MatCreateMAIJ(tfuser.data_parts, user->nclasses, &user->tdata);CHKERRQ(info);

  info = MatDestroy(&fuser.data_parts); CHKERRQ(info);
  info = MatDestroy(&tfuser.data_parts); CHKERRQ(info);
  
  PetscFunctionReturn(0);
}
