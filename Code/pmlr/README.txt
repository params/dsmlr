
How to run PMLR:
================

$ ./pmlr --train ../../Data/CLEF/train2.txt 
         --test ../../Data/CLEF/test2.txt 
         -l 1.0000e-04 
         -m 10000 

$ ./pmlr --train ../../Data/NEWS20/train2.txt 
         --test ../../Data/NEWS20/test2.txt 
         -l 8.8810e-05 
         -m 10000

$ ./pmlr --train ../../Data/LSHTC1-small/train2.txt 
         --test ../../Data/LSHTC1-small/test2.txt 
         -l 2.2406e-07 
         -m 10000

Notes:
=====

-l is the regularization value
-m is the number of max iterations to run

PMLR uses L-BFGS as underlying inner optimizer from the 
ALGLIB package (included in the code).
