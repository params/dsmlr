/*
 * Copyright (c) 2019 Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#include "pmlrConfig.h"

#include <iostream>
#include <fstream> 
#include <string> 
#include <vector> 
#include <iterator>

#undef SEEK_SET
#undef SEEK_END 
#undef SEEK_CUR
#include "mpi.h"

#include "parameters.hpp"
#include "pmlr.hpp"

using namespace std; 

int main(int argc, char **argv){

  parameters p(argc, argv);

  // retrieve MPI task info  
  int numtasks, rank, hostname_len;
  char hostname[MPI_MAX_PROCESSOR_NAME];
  int mpi_thread_provided;
  //MPI_Init_thread(nullptr, nullptr, MPI_THREAD_MULTIPLE, &mpi_thread_provided);
  MPI_Init(nullptr, nullptr);
  /*if (mpi_thread_provided != MPI_THREAD_MULTIPLE) {
    cerr << "multi-threaded MPI not available!!! ("
         << mpi_thread_provided << " != " << MPI_THREAD_MULTIPLE << ")" << endl;
    exit(1);
  }*/
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
  MPI_Get_processor_name(hostname, &hostname_len);
  
  PMLR m(p, numtasks, rank);
  m.optimize(p.max_iter, p.model_file);
  return 0;
}
