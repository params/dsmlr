/*
 * Copyright (c) 2019 Parameswaran Raman, Sriram Srinivasan, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */


#ifndef DSMLR_BODY_HPP_
#define DSMLR_BODY_HPP_

#include <iostream>
#include <vector>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <numeric>

#include <boost/filesystem.hpp>

#include "../nomad.hpp"
#include "../NomadBodyScheduled.hpp"
#include "DSMLRUpdater.hpp"
#include "PrimalParam.hpp"
#include "MulticlassDataLoader.hpp"
#include "DSMLRDataOption.hpp"

using std::cout;
using std::cerr;
using std::endl;

using std::string;
using std::vector;
using std::ifstream;
using std::ios;

using nomad::NomadOption;

class DSMLRBody: public NomadBody {

  protected:
    SeqSender* train_sender_;
    ParamMemPool* param_mem_pool_;
    int unit_bytenum_;
    long long msg_bytenum_;
    DSMLRDataOption* option_ = nullptr;
    MetaData* metadata_ = nullptr;
    MulticlassDataLoader* loader_ = nullptr;
    PrimalParamGenerator* v_p_gen_ = nullptr;
    vector<IUpdater*> updaters_;
    Logger *logger_;

    bool initialized_ = false;
    int *train_count_errors;
    real *train_sum_errors;
    int *test_count_errors;
    real *test_sum_errors;
    scalar *local_loss;


    void ensure_initialization(){
      if(!initialized_){
        cout<<"ERROR: initialize not yet called."<<endl;
        exit(2);
      }
    }

  public:
    virtual IParamGenerator* get_param_generator(){
      ensure_initialization();
      return (IParamGenerator*)v_p_gen_;
    }

    virtual int get_num_cols() {
      ensure_initialization();
      //return loader_->get_num_cols();
      return metadata_->global_num_classes_;
    }

    virtual ISender* get_train_sender(){
      ensure_initialization();
      return (ISender*)train_sender_;
    }

    virtual void initialize_options(int argc, char **argv){
      option_ = new DSMLRDataOption();
      if (option_->parse_command(argc, argv) == false) {
        logger_->error("ERROR: problem parsing command.");
        exit(2);
      }
    }

    virtual int get_unit_bytenum(){
        ensure_initialization();
        return unit_bytenum_;
    }

    virtual long long get_msg_bytenum(){
        ensure_initialization();
        return msg_bytenum_;
    }

    virtual ParamMemPool* get_param_mem_pool(){
        ensure_initialization();
        return param_mem_pool_;
    }

    virtual void initialize(){
      logger_ = Logger::get_logger();

      //create data loader.
      load_data_loader();

      //read metadata
      metadata_ = new MetaData();
      loader_->read_meta_data(metadata_, option_->train_file_name_, 
          option_->test_file_name_, option_->meta_file_name_);

      //initialize parameter generator
      int seed = option_->seed_ + rank * 131 + 139;
      v_p_gen_ = new PrimalParamGenerator(metadata_->global_num_features_, seed);

      //Create updaters.
      for(size_t i = 0; i < option_->num_threads_ ; i++){
        updaters_.push_back((IUpdater*)new DSMLRUpdater(option_, metadata_, loader_));
      }
      // define constants needed for network communication
      IParam* temp_dummy_par = v_p_gen_->generate_parameter();
      unit_bytenum_ = temp_dummy_par->get_bytes();
      delete temp_dummy_par;

      // number of columns + columns
      msg_bytenum_ = sizeof(int) * 1 + unit_bytenum_ * option_->units_per_msg_;
      //memory pool.
      long global_num_cols = metadata_->global_num_classes_;
      param_mem_pool_ = new ParamMemPool((IParamGenerator*)v_p_gen_, option_->mempool_size_ * global_num_cols);
      //train sender
      train_sender_ = new SeqSender(option_, param_mem_pool_, rank, global_num_cols, unit_bytenum_, msg_bytenum_, numtasks);

      //statistics holders
      train_count_errors = new int[option_->num_threads_];
      train_sum_errors = new real[option_->num_threads_];
      test_count_errors = new int[option_->num_threads_];
      test_sum_errors = new real[option_->num_threads_];
      local_loss = new scalar[option_->num_threads_];

      initialized_ = true;
    }

    virtual MulticlassDataLoader* get_data_loader(){
      ensure_initialization();
      return loader_;
    }

    virtual NomadOption *get_option() {
      if(option_ == nullptr){
        cout<<"initialize_options() not called yet."<<endl;
        exit(2);
      }
      return (NomadOption*)option_;
    }

    virtual IParam* get_initial_param(int col_index){
      ensure_initialization();
      return v_p_gen_->generate_parameter(col_index);
    }

    ~DSMLRBody(){
      for (auto u : updaters_){
        delete u;
      }
      updaters_.clear();
      delete param_mem_pool_;
      delete train_sender_;
      delete option_;
      delete metadata_;
      delete loader_;
      delete v_p_gen_;
      delete[] train_count_errors;
      delete[] test_count_errors;
      delete[] train_sum_errors;
      delete[] test_sum_errors;
      delete[] local_loss;
    }

    void load_data_loader(){
      if(loader_ == nullptr){
        loader_ = new MulticlassDataLoader();
      }
    }

    virtual vector<IUpdater*> get_updaters(){
      ensure_initialization();
      return updaters_;
    }

    virtual void finalize(vector<IParam*> saved_columns){
/*
      if (option_->output_path_.length() > 0) {
        for (size_t task_iter = 0; task_iter < numtasks; task_iter++) {
          if (task_iter == rank) {
            ofstream ofs(option_->output_path_ + boost::lexical_cast<string>(rank), ofstream::out | ofstream::app);
            for (IParam *ip_col : saved_columns) {
              PrimalParam* p_col = (PrimalParam*)ip_col;
              ofs << "column," << (p_col->get_col_index());
              for (size_t t = 0; t < metadata_->global_num_features_; t++) {
                ofs << "," << p_col->get_param()[t];
              }
              ofs << endl;
            }
            ofs.close();
          }
          MPI_Barrier(MPI_COMM_WORLD);
        }
      }
*/
    }

    scalar compute_local_loss(MetaData*& metadata,
        Data*& data,
        vector<IParam*> w,
        vector<ai_plus> vec_ai, int thread_id){

      // Compute loss and objective function on train data 
      scalar loss = SCALAR_ZERO;
      size_t local_N = data->local_train_num_instances_;
      size_t N = metadata->global_train_num_instances_;

      for(size_t local_i = 0; local_i < local_N; ++local_i){
        const ai_plus ai = vec_ai[local_i];
        //logger_->info("<params> ai: " + to_string(ai.ai_));
        loss += log(ai.ai_) + ai.max_fx_ - ai.fxy_;
      }
      loss /= N;
      logger_->info("local loss: " + to_string(loss), thread_id);

      return loss;
    }

    scalar compute_dot(const Instance &xi, scalar* wk){
      scalar dot = SCALAR_ZERO;
      const size_t omega_i = xi.idx_.size();
      for(size_t it = 0; it < omega_i; ++it){
        const size_t j = xi.idx_[it];
        const scalar xij = xi.val_[it];
        const scalar wkj = wk[j];
        dot += wkj * xij;
      }
      return dot;
    }
    
    scalar compute_dot_new(const Instance &xi, PrimalParam* p)
    {
      // get pointer to wk
      scalar* wk = p->get_param();
      scalar dot = SCALAR_ZERO;
      const size_t omega_i = xi.idx_.size();
      for(size_t it = 0; it < omega_i; ++it){
        const size_t j = xi.idx_[it];
        const scalar xij = xi.val_[it];
        const scalar wkj = wk[j];
        dot += wkj * xij;
      }
      return dot;
    }
    
    scalar compute_local_mrr(MetaData*& metadata,
        const vector<Instance> &data,
        vector<IParam*> w,
        vector<ai_plus> vec_ai, int thread_id, ofstream& ofs){

      // Compute loss and objective function on train data 
      scalar mrr = SCALAR_ZERO;
      size_t K = metadata->global_num_classes_;

      vector<scalar> xTwk;
      xTwk.resize(K);
      std::vector<size_t>::iterator it;
      size_t count = 0;
      for(auto &xi: data){
        std::fill(xTwk.begin(), xTwk.end(), SCALAR_ZERO);
        const size_t yi = xi.label_;
        std::vector<size_t> y(xTwk.size());  
        for(size_t local_k=0; local_k < K; ++local_k){
          const size_t yi = xi.label_;
          PrimalParam* pp = ((PrimalParam*) w[local_k]);
          size_t colindex = pp->get_col_index();
          //cout << "\nrank: " << rank << ", colindex: " << colindex << endl;
          //cout << "\nrank: " << rank << ", local_k: " << local_k << endl;
          xTwk[colindex] = compute_dot_new(xi, pp);
          y[local_k] = colindex;
          //logger_->info("local_k: " + to_string(local_k) + ", colindex: " + to_string(pp->get_col_index()), thread_id);
        }
        
        auto comparator = [&xTwk](size_t a, size_t b){ return xTwk[a] > xTwk[b]; };
        std::sort(y.begin(), y.end(), comparator);
        it = std::find(y.begin(), y.end(), yi);
        size_t pred_rank = std::distance(y.begin(), it);
        //if(count < 5)
        //  logger_->info("local rank: " + to_string(pred_rank), thread_id);
        if(rank == 0) 
          ofs << pred_rank << " "; 
        mrr += 1.0 / (1+pred_rank);
        count++;
      }
      //logger_->info("local mrr: " + to_string(mrr), thread_id);

      return mrr;
    }


    void calc_f1_measures(size_t *tp,
        size_t *fp,
        size_t *fn,
        scalar &micro_f1,
        scalar &macro_f1,
        size_t &nr_precision,
        size_t &dr_precision,
        size_t &dr_recall,
        size_t &num_labels){

      for(size_t k = 0; k < num_labels; ++k){
        nr_precision += tp[k];
        dr_precision += tp[k] + fp[k];
        dr_recall += tp[k] + fn[k];

        scalar p = 1.0;
        scalar r = 1.0;

        if(tp[k]+fp[k] > 0)
          p = (scalar)tp[k]/(tp[k] + fp[k]);
        if(tp[k]+fn[k] > 0)
          r = (scalar)tp[k]/(tp[k] + fn[k]);

        if(p or r)
          macro_f1 += (2*p*r)/(p+r);
      }
      macro_f1 /= num_labels;
      scalar p = (scalar) nr_precision / dr_precision;
      scalar r = (scalar) nr_precision / dr_recall;
      if(p or r)
        micro_f1 = (2 * p * r) / (p + r);
      logger_->info("train precision: " + to_string(p) 
          + ", train recall: " + to_string(r));
      return;
    }


    void compute_global_f1(size_t K, scalar &micro_f1,
        scalar &macro_f1,
        size_t *local_buf){

      size_t global_buf[3*K];
      std::fill(global_buf, global_buf + (3*K), SCALAR_ZERO);
      MPI_Allreduce(&local_buf[0], &global_buf[0], 3*K,
          MPI_SIZE_T, MPI_SUM, MPI_COMM_WORLD);

      size_t *tp = &global_buf[0];
      size_t *fp = &global_buf[K];
      size_t *fn = &global_buf[2*K];

      size_t nr_precision = 0;
      size_t dr_precision = 0;
      size_t dr_recall = 0;

      calc_f1_measures(tp, fp, fn,
          micro_f1, macro_f1,
          nr_precision, dr_precision, dr_recall, K);

    }

    void compute_local_f1(size_t K,
        size_t num_parts,
        const vector<Instance> &data,
        const vector<ai_plus>  &a,
        size_t *local_buf){

      // Collect f1 stats
      //size_t local_buf[3*K];
      //std::fill(local_buf, local_buf + (3*K), SCALAR_ZERO);
      size_t *tp=&local_buf[0];
      size_t *fp=&local_buf[K];
      size_t *fn=&local_buf[2*K];

      for(auto &x: data){
        const size_t yi = x.label_;
        const size_t local_i = x.id_/num_parts;
        const size_t yhati = a[local_i].yhat_;

        if(yi == yhati) 
          tp[yhati]++;
        else {
          fp[yhati]++;
          fn[yi]++;
        }
      }

      return;
    }

    virtual void compute_statistics(vector<IParam*> saved_columns, int iter_num, double time_iter){

      DSMLRDataOption &option = *option_;

      scalar train_micro_f1 = SCALAR_ZERO;
      scalar train_macro_f1 = SCALAR_ZERO;
      scalar test_micro_f1 = SCALAR_ZERO;
      scalar test_macro_f1 = SCALAR_ZERO;

      size_t Ntest = metadata_->global_test_num_instances_;
      size_t D = metadata_->global_num_features_;
      scalar reg = SCALAR_ZERO;
      //BUGBUG: do the below norm computation
      //more efficiently using std:: operations
      for (IParam* p : saved_columns) {
        for (size_t i = 0; i < D; ++i) {
          //reg +=  (w[k][i] * w[k][i]);
          reg +=  (((PrimalParam*) p)->get_param()[i]) * (((PrimalParam*) p)->get_param()[i]);
        }
      }
      reg *= 0.5*option_->lambda_;

      logger_->info("local regularizer: " + to_string(reg));

      // compute local obj on each thread
      for (size_t i = 0; i < option.num_threads_; i++) {
        DSMLRUpdater* updater = (DSMLRUpdater*) updaters_[i];
        local_loss[i] = compute_local_loss(metadata_, updater->data_, saved_columns, updater->vec_ai_, i);
      }

      scalar machine_local_loss = std::accumulate(local_loss, local_loss + option.num_threads_, 0.0);
      // aggregate local obj per machine 
      double machine_local_obj = machine_local_loss + reg;
      logger_->info("local obj = loss + regularizer: " + to_string(machine_local_obj));


      // compute global obj 
      double global_obj = SCALAR_ZERO;
      MPI_Allreduce(&machine_local_obj, &global_obj, 1,
          MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      MPI_Barrier(MPI_COMM_WORLD);

      //Compute local train f1 stats on each thread
      const int K = metadata_->global_num_classes_;
      size_t *local_buf = new size_t[option.num_threads_*3*K];
      std::fill(local_buf, local_buf + (option.num_threads_*3*K), SCALAR_ZERO);
      for (size_t i = 0; i < option.num_threads_; i++) {
        DSMLRUpdater* updater = (DSMLRUpdater*) updaters_[i];
        compute_local_f1(K, updater->num_parts, (updater->data_)->local_train_instances_, updater->vec_ai_, &local_buf[i*3*K]);
      }

      // aggregate from each thread for each machine
      // colwise sum
      for (size_t j = 0; j < 3*K; j++){
        for (size_t i = 0; i < option.num_threads_; i++) {
          local_buf[j] += local_buf[i*3*K + j];
        }
      }

      //Compute global train f1 stats from the machines
      compute_global_f1(K, train_micro_f1, train_macro_f1, &local_buf[0]);

      //Compute local test f1 stats on each thread
      std::fill(local_buf, local_buf + (option.num_threads_*3*K), SCALAR_ZERO);
      for (size_t i = 0; i < option.num_threads_; i++) {
        DSMLRUpdater* updater = (DSMLRUpdater*) updaters_[i];
        compute_local_f1(K, updater->num_parts, (updater->data_)->local_test_instances_, updater->vec_ai_test_, &local_buf[i*3*K]);
      }

      // aggregate from each thread for each machine
      // colwise sum
      for (size_t j = 0; j < 3*K; j++){
        for (size_t i = 0; i < option.num_threads_; i++) {
          local_buf[j] += local_buf[i*3*K + j];
        }
      }

      //Compute global test f1 stats from the machines
      compute_global_f1(K, test_micro_f1, test_macro_f1, &local_buf[0]);

      //Compute rank of prediction
      double global_mrr = SCALAR_ZERO;
      if (option_->output_path_pred_.length() > 0){ 
        ofstream ofs(option_->output_path_pred_, ofstream::out | ofstream::app);
        scalar *local_mrr = new scalar[option_->num_threads_];
        for (size_t i=0; i<option.num_threads_; i++){
          DSMLRUpdater* updater = (DSMLRUpdater*) updaters_[i];
          local_mrr[i] = compute_local_mrr(metadata_, (updater->data_)->local_test_instances_, 
                                             saved_columns, updater->vec_ai_, i, ofs);
        }
        ofs << endl;
        ofs.close();
        scalar machine_local_mrr = std::accumulate(local_mrr, local_mrr + option.num_threads_, 0.0);
        MPI_Allreduce(&machine_local_mrr, &global_mrr, 1,
            MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
        MPI_Barrier(MPI_COMM_WORLD);
        global_mrr = machine_local_mrr;
      
        delete[] local_mrr;
      }

      // Print statistics 
      if (rank == 0) {
        logger_->output("grepthis,"
            + to_string(iter_num) + ","
            + to_string(global_obj) + ","
            + to_string(time_iter) + ","
            + to_string(train_macro_f1) + ","
            + to_string(train_micro_f1) + ","
            + to_string(test_macro_f1) + ","
            + to_string(test_micro_f1) + ","
            + to_string(global_mrr/Ntest)
            );

      }
    
      //Save weights to disk after every iteration / periodically
      
      if (option_->output_path_.length() > 0) {
        //for (size_t task_iter = 0; task_iter < numtasks; task_iter++) {
	    string dirname = option_->output_path_ + "/it" + boost::lexical_cast<string>(iter_num);
          if (rank == 0) {
	    boost::filesystem::path dir(option_->output_path_);
	    if(!(boost::filesystem::exists(dir))){
              cout << "dir doesn't exist.." << endl;

              if (boost::filesystem::create_directory(dir))
                cout << "dir successfully created.." << endl;
    	    } 
	    
	    boost::filesystem::path subdir(dirname);
	    if(!(boost::filesystem::exists(subdir))){
              cout << "dir doesn't exist.." << endl;

              if (boost::filesystem::create_directory(subdir))
                cout << "dir successfully created.." << endl;
    	    } 
          }
          MPI_Barrier(MPI_COMM_WORLD);

	    //ofstream ofs(option_->output_path_ + "/it" + boost::lexical_cast<string>(iter_num) + "/rank" + boost::lexical_cast<string>(rank), 
	    //	ofstream::out | ofstream::app);
	    ofstream ofs(dirname + "/rank" + to_string(rank) , ofstream::out);
            for (IParam *ip_col : saved_columns) {
              PrimalParam* p_col = (PrimalParam*)ip_col;
              ofs << "column," << (p_col->get_col_index());
              for (size_t t = 0; t < metadata_->global_num_features_; t++) {
                ofs << "," << p_col->get_param()[t];
              }
              ofs << endl;
            }
            ofs.close();
          MPI_Barrier(MPI_COMM_WORLD);
        //}
      }

      
      //reset vec_ai before next iteration
      //also a place to test exact bi computation version for now
      for (size_t i = 0; i < option.num_threads_; i++) {
        DSMLRUpdater* updater = (DSMLRUpdater*) updaters_[i];
        size_t local_N = (updater->data_)->local_train_num_instances_;
        size_t local_Ntest = (updater->data_)->local_test_num_instances_;

        bool succeed = (updater->data_)->apply_to_train_block(
          [&](Instance & instance)->bool{
            const Instance &xi = instance;
            const size_t local_i = xi.id_/updater->num_parts;
            
            // Compute bi exactly once testing is done 
            //scalar &bi = (updater->local_params)[local_i];
            //bi = -log((updater->vec_ai_)[local_i].ai_) - (updater->vec_ai_)[local_i].max_fx_;

            (updater->vec_ai_)[local_i].reset(K);
        });
      }

      delete[] local_buf;


    }

};

#endif
