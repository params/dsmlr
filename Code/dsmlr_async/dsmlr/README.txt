How to compile:
===============
From the directory dsmlr/Code/dsmlr_async, run the following commands
to cleanup existing binaries and re-build (this first compiles the nomad
framework code which is used for underlying asynchronous communication by 
DS-MLR, followed by compilation of DS-MLR).

  $ ./clean.sh

  $ ./build.sh


How to run:
===========

>> single-core single-thread

  $ mpirun -np 1 ./dsmlr_double --train ../../../Data/CLEF/train2.txt 
                              --test ../../../Data/CLEF/test2.txt 
                              --meta ../../../Data/CLEF/meta.txt 
                              --nthreads 1 
                              -t 10 
                              -l 0.0001 
                              -e 100|grep grep

>> single-core multiple-threads

  $ mpirun -np 1 ./dsmlr_double --train ../../../Data/CLEF/train2.txt 
                              --test ../../../Data/CLEF/test2.txt 
                              --meta ../../../Data/CLEF/meta.txt 
                              --nthreads 2 
                              -t 10 
                              -l 0.0001 
                              -e 100|grep grep

>> multiple-cores multiple-threads

  $ mpirun -np 2 ./dsmlr_double --train ../../../Data/CLEF/train2.txt 
                              --test ../../../Data/CLEF/test2.txt 
                              --meta ../../../Data/CLEF/meta.txt 
                              --nthreads 2 
                              -t 10 
                              -l 0.0001 
                              -e 100|grep grep

How to dump weights per iteration:
=================================

  $ mpirun -np 2 ./dsmlr_double --train ../../../Data/CLEF/train2.txt 
                              --test ../../../Data/CLEF/test2.txt 
                              --meta ../../../Data/CLEF/meta.txt 
                              --output weights
                              --nthreads 2 
                              -t 10 
                              -l 0.0001 
                              -e 100|grep grep


How to dump ranks:
=================

  $ mpirun -np 1 ./dsmlr_double --train ../../../Data/CLEF/train2.txt 
                              --test ../../../Data/CLEF/test2.txt 
                              --meta ../../../Data/CLEF/meta.txt 
                              --output_pred ranks.txt
                              --nthreads 1 
                              -t 10 
                              -l 0.0001 
                              -e 100|grep grep

