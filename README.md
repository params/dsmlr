# dsmlr

DS-MLR (Doubly-Separable Multinomial Logistic Regression) is a hybrid-parallel (simultaneously data and model parallel) 
algorithm to solve Multinomial Logistic Regression for very large data (large # of examples and large # of classes, i.e. 
Extreme Multiclass classification). 

Please refer to the paper [1] for the DS-MLR formulation, description of the synchronous and asynchronous algorithms. 

Each source-code (for both DS-MLR and the baselines used in the paper) directory
contains the corresponding CMakeLists.txt or MakeFile required to build the
code. Invidividual README files for running the code on small-scale datasets are provided.

====

[1] Scaling Multinomial Logistic Regression via Hybrid-Parallelism (Parameswaran
Raman, Sriram Srinivasan, Shin Matsushima, Xinhua Zhang, Hyokun Yun, S.V.N.
Vishwanathan), KDD 2019.
