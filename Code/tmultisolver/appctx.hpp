/*
 * Copyright (c) 2019 Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef _APPCTX_HPP_
#define _APPCTX_HPP_

#include "tao.h"
#include "timer.hpp"

// User-defined application context - contains data needed by the
// application-provided call-back routines that evaluate the function,
// and gradient.
typedef struct {

  PetscMPIInt rank;
  PetscMPIInt size;

  PetscInt solver;  // LBFGS or BMRM
  PetscInt datafmt; // LibSVM or binary  
  PetscReal tol;    // Tolerance 
  
  /// training dataset and its statistics
  PetscInt dim;            // Dimensions 
  PetscInt m;              // Number of data points 
  PetscInt nclasses;       // Number of classes
  Vec labels;              
  Mat data;

  /// Test dataset and its statistics
  Vec tlabels;             // Test labels
  Mat tdata;               // Test data
  PetscInt tdim;           // test Dimensions 
  PetscInt tm;             // Number of test data points 

  /// Training parameters
  PetscReal lambda;        // Regularization parameter

  PetscReal lambda_max;
  PetscReal lambda_min;
  PetscReal lambda_step;

  char data_path[PETSC_MAX_PATH_LEN], labels_path[PETSC_MAX_PATH_LEN]; 
  char tdata_path[PETSC_MAX_PATH_LEN], tlabels_path[PETSC_MAX_PATH_LEN]; 
  char model_path[PETSC_MAX_PATH_LEN];

  // Variables for intermediate storage 
  Vec fx;                  // Store predictions <w, x>
  Vec fxloc;               // locally stored fx on head node
  Vec tfx;                 // Store predictions for test data
  Vec grad;                // Store coefficients for the gradient   
  Vec gradloc;             // locally stored gradient on head node 

  Vec labelsloc;           // locally stored labels on head node  

  PetscInt num_fn_eval;
  
  // Time the optimization procedure 
  timer opt_timer;
  
  // Time obj grad calculation 
  timer objgrad_timer;

  timer matvec_timer;
  
  
} AppCtx;

typedef struct{
  PetscInt dim;     // Dimensions
  PetscInt m;       // Number of data points
  PetscInt classes; // Number of classes 
  PetscInt maxnnz;  // Maximum number of non-zeros in any row
  PetscInt maxlen;  // Maximum number of characters in any row
  Vec      *labels; // Store labels
  Mat      *data;   // Store data
  Mat      data_parts; 
  // nnz_array is used only for intermediate storage
  PetscInt      *nnz_array;

} AppCtx_file;

#endif
