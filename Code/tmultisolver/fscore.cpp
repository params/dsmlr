/*
 * Copyright (c) 2019 Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#include "fscore.hpp"

namespace evaluation{
  
  // fscore - Evaluates fscore. 
  
  // Input Parameters:
  // fx_array - function prediction
  // labels_array - labels
  // n - size of arrays
  // user - user defined application context 

  // Output parameters:
  // micro_fscore - computed micro fscore
  // macro_fscore - computed macro fscore
  
  PetscErrorCode fscore(PetscScalar *fx_array, 
                        PetscScalar *labels_array, 
                        PetscInt    n,
                        PetscScalar *macro_fscore,
                        PetscScalar *micro_fscore,
                        AppCtx      *user) {
    *micro_fscore = 0.0;
    *macro_fscore = 0.0;
    
    PetscInt prediction[n];
    // Compute function and gradient coefficients
    for (int i = 0; i < n; i++) {
      PetscInt start = i*user->nclasses;
      PetscInt end = (i+1)*user->nclasses;
      PetscScalar fx_max = fx_array[start];
      prediction[i] = 0;
      for (PetscInt idx = start; idx < end; idx++)
        if (fx_array[idx] > fx_max) {
          fx_max = fx_array[idx];
          prediction[i] = idx - start;
        }
    }

    PetscInt tp[user->nclasses];
    PetscInt fp[user->nclasses];
    PetscInt fn[user->nclasses]; 
    for (PetscInt k = 0; k < user->nclasses; k++) {
      tp[k] = 0;
      fp[k] = 0;
      fn[k] = 0;
    }
    
    for (PetscInt i = 0; i < n; i++) {
      if ((PetscInt) labels_array[i] == prediction[i]) {
        tp[prediction[i]]++;
      } else {
        fp[prediction[i]]++;
        fn[(PetscInt) labels_array[i]]++;
      }
    }
    
    PetscInt nr_precision = 0;
    PetscInt dr_precision = 0;
    PetscInt dr_recall = 0;
    for (PetscInt k = 0; k < user->nclasses; k++) {
      nr_precision += tp[k];
      dr_precision += tp[k] + fp[k];
      dr_recall += tp[k] + fn[k];
      
      PetscScalar p = 1.0;
      PetscScalar r = 1.0;
      
      if(tp[k]+fp[k] > 0)
        p = (PetscScalar) tp[k]/(tp[k] + fp[k]);
      
      if(tp[k]+fn[k] > 0)
        r = (PetscScalar) tp[k]/(tp[k] + fn[k]);
      
      if(p or r)
        *macro_fscore += (2*p*r)/(p+r);
    }
    *macro_fscore /= user->nclasses;
    PetscScalar p = (PetscScalar) nr_precision / dr_precision; 
    PetscScalar r = (PetscScalar) nr_precision / dr_recall;
    if(p or r)
      *micro_fscore = (2 * p * r) / (p + r);
    
    return 0;
  }
  
}
