#! /bin/bash

mpirun -np 1 ./dsmlr --train ~/data/wine/train2.txt --test ~/data/wine/test2.txt -l 0.0001 -e 100 --max_iter 3 --bs_period 1 --eval_period 1 --seed 1987 --solver 2
