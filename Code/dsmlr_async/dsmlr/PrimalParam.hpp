/*
 * Copyright (c) 2019 Parameswaran Raman, Sriram Srinivasan, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef PRIMAL_PARAM
#define PRIMAL_PARAM

#include <vector>
#include <iostream>
#include <random>
#include "../IParamGenerator.hpp"
#include "../IParam.hpp"
#include "../nomad.hpp"

using std::cout;
using std::endl;

class PrimalParam:public IParam{
  private:
    scalar* values_;
    int col_index_;
    int dims_;

    void unpack_from(char* ptr){
      int* i_ptr = (int*)ptr;
      col_index_ = *i_ptr;
      i_ptr++;
      scalar* d_ptr = (scalar*)i_ptr;
      std::copy(d_ptr, d_ptr+dims_, values_);
    }

    void pack_to(char* ptr){
      int* i_ptr = (int*)ptr;
      *i_ptr = col_index_;
      i_ptr++;
      scalar* d_ptr = (scalar*)i_ptr;
      std::copy(values_, values_+dims_, d_ptr);
    }

    int get_extra_bytes(){
      return (sizeof(scalar))*(dims_) + sizeof(col_index_);
      //BUGBUG: is it below instead? (in the old code)  
      //return (sizeof(scalar))*(dims) + sizeof(col_index) + sizeof(dims);
    }

  public:
    PrimalParam(int dims): dims_(dims){
      values_ = new scalar[dims_];
    }

    PrimalParam(int dims, int col_index, scalar* values):
      col_index_(col_index), values_(values),dims_(dims){}

    ~PrimalParam(){
      delete[] values_;
    }

    scalar* get_param(){
      return values_;
    }

    int get_col_index(){
      return col_index_;
    }

    int get_dims(){
      return dims_;
    }

    bool operator==(PrimalParam other){
      if(col_index_ != other.col_index_ || dims_ != other.dims_){
        return false;
      }
      for (int i = 0 ; i < dims_ ; i++) {
        if(values_[i] != other.values_[i]){
          return false;
        }
      }
      return true;
    }

    void print(){
      cout<<"--PARAMETER--"<<endl;
      cout<<"COL_INDEX: "<<col_index_<< ", DIMS: " <<dims_<<endl;
      for (int i = 0 ; i < dims_ ; i++){
        cout<<values_[i]<<", ";
      }
      cout<<"\n--END PARAMETER--"<<endl;
    }

};

class PrimalParamGenerator:IParamGenerator{
  int dims_;
  std::default_random_engine *generator_;
  std::uniform_real_distribution<scalar> *unif_distribution_;

  public:
  //BUGBUG: is below unif real distrib stuff needed?
  //BUGBUG: also add zero-initialization here
  PrimalParamGenerator(int dims, int seed): dims_(dims){
    generator_ = new std::default_random_engine(seed);
    unif_distribution_ = new std::uniform_real_distribution<scalar>(0,1.0 / sqrt(dims_));
  }

  IParam* generate_parameter(){
    return (IParam*)new PrimalParam(dims_);
  }

  IParam* generate_parameter(int col_index){
    scalar* values = new scalar[dims_];
    for(int i = 0 ; i < dims_ ; i++){
      //values[i] = (*unif_distribution_)(*generator_);
      values[i] = 0.0;
    }
    return (IParam*)new PrimalParam(dims_, col_index, values);
  }
};

#endif
