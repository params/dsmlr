
How to run tmultisolver (L-BFGS)
================================

**Pre-requisites**:

- PETSc

- TAO



$ ./multiclass-losses -data ../../Data/CLEF/train2.txt 
                      -tdata ../../Data/CLEF/test2.txt 
                      -lmax 1.0000e-04 
                      -lmin 1.0000e-04 
                      -lstep 1.0000e-04 
                      -tol 1.0000e-08 
                      -solver 0

$ ./multiclass-losses -data ../../Data/NEWS20/train2.txt 
                      -tdata ../../Data/NEWS20/test2.txt 
                      -lmax 8.8810e-05 
                      -lmin 8.8810e-05 
                      -lstep 8.8810e-05 
                      -tol 1.0000e-08 
                      -solver 0


$ ./multiclass-losses -data ../../Data/LSHTC1-small/train2.txt 
                      -tdata ../../Data/LSHTC1-small/test2.txt 
                      -lmax 2.2406e-07 
                      -lmin 2.2406e-07 
                      -lstep 2.2406e-07 
                      -tol 1.0000e-08 
                      -solver 0


Notes:
=====

-solver = 0 => L-BFGS solver
-solver = 1 => BMRM solver

-tol is the tolerance
-lmax, -lmin, -lstep are used to trying varying regularization values
(set them to the same if trying only one value)
