/*
 * Copyright (c) 2019 Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef _LOADDATA_LIBSVM_HPP_
#define _LOADDATA_LIBSVM_HPP_

#include "appctx.hpp"

PetscErrorCode parse_file(FILE* input, AppCtx_file* user);

void parse_line(char* line, PetscInt* idxs, PetscScalar* vals, PetscScalar* label, int m, AppCtx_file* user);

PetscErrorCode assemble_matrix(FILE* input, PetscInt begin, PetscInt end, AppCtx_file* user);

PetscErrorCode fill_arrays_uni(FILE* input, AppCtx_file* user);

PetscErrorCode reparse_file(FILE* input, PetscInt* diag_nnz, PetscInt* offdiag_nnz, AppCtx_file* user);

PetscErrorCode fill_arrays_parallel(FILE* input, AppCtx_file* user);

PetscErrorCode LoadDataLibsvm(AppCtx *user_main);

#endif
