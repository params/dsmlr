/*
 * Copyright (c) 2019 Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#include "tao.h"
#include <math.h>
#include <cassert>
#include <algorithm>
#include <string>

#include "appctx.hpp"
#include "loaddata_libsvm.hpp"
#include "logistic.hpp"
#include "fscore.hpp"

// optimizer
enum {LBFGS = 0, BMRM = 1};

// data format 
enum {BINARY = 0, LIBSVM = 1};

static  char help[] = "multiclass logistic regression using TAO \n\
Input parameters are:\n\
  -data    <filename> : file which contains training data\n\
  -labels  <filename> : file which contains training labels (only if training data in Petsc format)\n\
  -tdata   <filename> : file which contains test data\n\
  -tlabels <filename> : file which contains test labels (only if test data in Petsc binary format)\n\
  -model <filename> : file to store the final model\n\
  -lmax <double> : the largest regularization parameter \n\
  -lmin <double> : the smallest regularization parameter \n\
  -lstep <double> : step size \n\
  -solver  <int>      : 0 = LBFGS 1 = BMRM \n\
  -tol <double> : convergence tolerance \n\n";

// Actual loss and gradient computation happens in this function     
PetscErrorCode (*loss_grad)(PetscScalar *fx_array, 
                            PetscScalar *labels_array, 
                            PetscScalar *grad_array, 
                            PetscInt    n, 
                            PetscScalar *loss,
                            AppCtx      *user);    

PetscErrorCode (*orig_loss)(PetscScalar *fx_array, 
                            PetscScalar *labels_array, 
                            PetscInt    n, 
                            PetscScalar *loss,
                            AppCtx      *user);

// Initialize and finalize the loss 
PetscErrorCode (*init)(AppCtx *user);

PetscErrorCode (*finalize)(void);

// Forwarding function
PetscErrorCode ScalarLossGradBForce(TaoSolver tao, Vec w, double *f, Vec G, void *ctx);

// Compute f-score
PetscErrorCode Evaluate(Vec w, AppCtx* user);

// Check Generalization of intermediate solutions 
PetscErrorCode PrintFunVal(TaoSolver tao, void *ctx);

// Read in the feature and label data
PetscErrorCode LoadData(AppCtx *user);

// Parse the arguments in the command line
PetscErrorCode LoadOptions(AppCtx *user);

// Print a summary of the settings
PetscErrorCode PrintSetting(AppCtx *user);

// Allocate the space for work
PetscErrorCode AllocateWorkSpace(Vec *w, AppCtx *user);

// Destroy the work space
PetscErrorCode DestroyWorkSpace(Vec *w, AppCtx *user);

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char **argv) {
  AppCtx                     user;      
  PetscErrorCode             info;              
  Vec                        w;
  TaoSolver                  tao;                             
  TaoSolverTerminationReason reason;
  PetscViewer viewer;
  
  // Initialize TAO and PETSc
  PetscInitialize(&argc, &argv, (char *)0, help);
  TaoInitialize(&argc, &argv, (char *)0, help);
  
  MPI_Comm_rank(PETSC_COMM_WORLD, &user.rank);
  MPI_Comm_size(PETSC_COMM_WORLD, &user.size);
  
  // Check for command line arguments to override defaults
  LoadOptions(&user);

  if (user.datafmt == BINARY)
    LoadData(&user);
  else 
    LoadDataLibsvm(&user);
  
  AllocateWorkSpace(&w, &user);   // Allocate space 
  
  if (user.rank == 0) {
    init(&user);
    PrintSetting(&user);
  }

  // Start the timer 
  if (user.rank == 0)
    user.opt_timer.start();

  // TAO code begins here
  while (user.lambda >= user.lambda_min) {
    if (user.rank == 0) {
      PetscPrintf(PETSC_COMM_SELF, "\nlambda = %lf\n", user.lambda);
      PetscPrintf(PETSC_COMM_SELF, "iter fn.val gap time train_macro_f1 train_micro_f1 test_macro_f1 test_micro_f1\n", user.lambda);
    }

    info = VecSet(w, 0.0); CHKERRQ(info);
    
    // Create TAO solver with desired solution method 
    info = TaoCreate(PETSC_COMM_WORLD, &tao); CHKERRQ(info);
    
    if (user.solver == LBFGS) {
      info = TaoSetType(tao, "tao_lmvm"); CHKERRQ(info);
    } else {
      info = TaoSetType(tao, "tao_bmrm"); CHKERRQ(info);
      char lambda_buf[20];
      sprintf(lambda_buf, "%lf", user.lambda);
      PetscOptionsSetValue("-tao_bmrm_lambda", lambda_buf);
    }
    
    info = TaoSetInitialVector(tao, w); CHKERRQ(info);
    info = TaoSetObjectiveAndGradientRoutine(tao, ScalarLossGradBForce, &user); CHKERRQ(info);
    
    user.num_fn_eval = 0;
    info = TaoSetTolerances(tao, user.tol, 0, user.tol, 0, 0); CHKERRQ(info);
    info = TaoSetMaximumIterations(tao, 1000);CHKERRQ(info);
    info = TaoSetMaximumFunctionEvaluations(tao, 3000);CHKERRQ(info);
    
    // Check for TAO command line options 
    info = TaoSetFromOptions(tao); CHKERRQ(info);
    
    // Print intermediate function values 
    info = TaoSetMonitor(tao, PrintFunVal, &user, PETSC_NULL); CHKERRQ(info);
    
    // Solve the Application 
    info = TaoSolve(tao);CHKERRQ(info);
    
    // Free TAO data structures 
    info = TaoDestroy(&tao); CHKERRQ(info);
    user.lambda *= user.lambda_step;
  }
  

  // Stop the timer
  if (user.rank == 0)
    user.opt_timer.stop();
  
  // Print statistics 
  if (user.rank == 0) {
    PetscPrintf(PETSC_COMM_WORLD, "Total CPU time: %g seconds \n", user.opt_timer.cpu_total);
    PetscPrintf(PETSC_COMM_WORLD, "Total wallclock time: %g seconds \n", user.opt_timer.wallclock_total);
    PetscPrintf(PETSC_COMM_WORLD, "Obj grad CPU time: %g seconds \n", user.objgrad_timer.cpu_total);
    PetscPrintf(PETSC_COMM_WORLD, "Obj grad wallclock time: %g seconds \n", user.objgrad_timer.wallclock_total);    
  }
 
  // Print solution
  /*PetscPrintf(PETSC_COMM_WORLD, "\n\nSolution: \n");
  VecView(w, PETSC_VIEWER_STDOUT_WORLD);
  PetscPrintf(PETSC_COMM_WORLD, "\nL2-Norm: \n");
  PetscReal norm = 0;
  VecNorm(w, NORM_2, &norm);
  PetscPrintf(PETSC_COMM_WORLD, "%g\n", norm);*/

  /*PetscViewerASCIIOpen(PETSC_COMM_WORLD, user.model_path, &viewer);
  PetscViewerSetFormat(viewer, PETSC_VIEWER_DEFAULT);
  VecView(w, viewer);
  PetscViewerDestroy(&viewer);*/


  // Free some space
  DestroyWorkSpace(&w, &user);
  
  if (user.rank == 0)
    finalize();
  
  // Finalize TAO and PETSc
  info = TaoFinalize(); CHKERRQ(info);
  info = PetscFinalize(); CHKERRQ(info);
  
  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "LoadOptions"
PetscErrorCode LoadOptions(AppCtx *user) {

  PetscBool         flg;
  PetscErrorCode    info;
  PetscBool         labels_flg, tlabels_flg;

  user->lambda_max  = 1e-1; 
  user->lambda_min  = 1e-1;
  user->lambda_step = 0.1;
  user->lambda      = 1e-1;
  user->tol         = 1e-10;
  user->solver      = LBFGS;

  info = PetscOptionsGetInt(PETSC_NULL, "-solver", &user->solver, &flg); CHKERRQ(info);  
  info = PetscOptionsGetReal(PETSC_NULL, "-lmax", &user->lambda_max, &flg); CHKERRQ(info);
  info = PetscOptionsGetReal(PETSC_NULL, "-lmin", &user->lambda_min, &flg); CHKERRQ(info);
  info = PetscOptionsGetReal(PETSC_NULL, "-lstep", &user->lambda_step, &flg); CHKERRQ(info);
  info = PetscOptionsGetReal(PETSC_NULL, "-tol", &user->tol, &flg); CHKERRQ(info);

  info = PetscOptionsGetString(PETSC_NULL, "-data", user->data_path, PETSC_MAX_PATH_LEN-1, &flg); CHKERRQ(info);
  if (!flg)
    SETERRQ(PETSC_ERR_SUP, 1, "No train files specified!");
  info = PetscOptionsGetString(PETSC_NULL, "-tdata", user->tdata_path, PETSC_MAX_PATH_LEN-1, &flg); CHKERRQ(info);
  if (!flg)
    SETERRQ(PETSC_ERR_SUP, 1, "No test files specified!");
  
  info = PetscOptionsGetString(PETSC_NULL, "-labels", user->labels_path, PETSC_MAX_PATH_LEN-1, &labels_flg); CHKERRQ(info);
  info = PetscOptionsGetString(PETSC_NULL, "-tlabels", user->tlabels_path, PETSC_MAX_PATH_LEN-1, &tlabels_flg); CHKERRQ(info);
  info = PetscOptionsGetString(PETSC_NULL, "-model", user->model_path, PETSC_MAX_PATH_LEN-1, &flg); CHKERRQ(info);
  
  // Either label files specified for both or no label files 
  assert((labels_flg and tlabels_flg) or (!labels_flg and !tlabels_flg));
  user->datafmt = BINARY;
  if (!labels_flg and !tlabels_flg)
    user->datafmt = LIBSVM;
  
  assert(user->solver == LBFGS or user->solver == BMRM);
  assert(user->lambda_max > 0.0); 
  assert(user->lambda_min > 0.0); 
  assert(user->lambda_step > 0.0); 
  assert(user->lambda_step < 1.0); 

  user->lambda = user->lambda_max;
  loss_grad = logisticloss::loss_grad; 
  orig_loss = logisticloss::orig_loss; 
  init = logisticloss::init; 
  finalize = logisticloss::finalize; 

  return 0;
}  



#undef __FUNCT__
#define __FUNCT__ "LoadData"
PetscErrorCode LoadData(AppCtx *user) {
  PetscViewer    view;
  PetscErrorCode info;
  PetscBool      flg;
  Mat            data_parts, tdata_parts; 
  PetscInt       imin, imax;
  PetscInt       m_local, dim_local;
  PetscReal      cmin, cmax;

  info = PetscViewerBinaryOpen(PETSC_COMM_WORLD, user->data_path, FILE_MODE_READ, &view); CHKERRQ(info);
  info = MatCreate(PETSC_COMM_WORLD, &data_parts); CHKERRQ(info);  
  info = MatSetType(data_parts, MATAIJ); CHKERRQ(info);  
  info = MatLoad(data_parts, view); CHKERRQ(info); 
  info = PetscViewerDestroy(&view); CHKERRQ(info);


  info = PetscViewerBinaryOpen(PETSC_COMM_WORLD, user->labels_path, FILE_MODE_READ, &view); CHKERRQ(info);
  info = VecCreate(PETSC_COMM_WORLD, &user->labels); CHKERRQ(info);
  info = VecLoad(user->labels, view); CHKERRQ(info);
  info = PetscViewerDestroy(&view); CHKERRQ(info);

  info = VecMin(user->labels, &imin, &cmin);CHKERRQ(info);
  info = VecMax(user->labels, &imax, &cmax);CHKERRQ(info);
  user->nclasses = cmax - cmin + 1;

  if (cmin != 0) {
    info = VecShift(user->labels, -cmin);CHKERRQ(info);
  }

  info = MatGetSize(data_parts, &user->m, &user->dim); CHKERRQ(info);
  info = MatCreateMAIJ(data_parts, user->nclasses, &user->data);CHKERRQ(info);

  info = PetscViewerBinaryOpen(PETSC_COMM_WORLD, user->tdata_path, FILE_MODE_READ, &view); CHKERRQ(info);
  info = MatCreate(PETSC_COMM_WORLD, &tdata_parts); CHKERRQ(info);  
  info = MatSetType(tdata_parts, MATAIJ); CHKERRQ(info); 
  info = MatLoad(tdata_parts, view); CHKERRQ(info);
  info = PetscViewerDestroy(&view); CHKERRQ(info);

  info = PetscViewerBinaryOpen(PETSC_COMM_WORLD, user->tlabels_path, FILE_MODE_READ, &view); CHKERRQ(info);
  info = VecCreate(PETSC_COMM_WORLD, &user->tlabels); CHKERRQ(info);
  info = VecLoad(user->tlabels, view); CHKERRQ(info);
  info = PetscViewerDestroy(&view); CHKERRQ(info);

  if (cmin != 0) {
    info = VecShift(user->tlabels, -cmin);CHKERRQ(info);
  }

  info = MatGetSize(tdata_parts, &user->tm, &user->tdim); CHKERRQ(info);
  info = MatCreateMAIJ(tdata_parts, user->nclasses, &user->tdata);CHKERRQ(info);
  assert(user->tdim == user->dim);

  info = MatDestroy(&data_parts); CHKERRQ(info);
  info = MatDestroy(&tdata_parts); CHKERRQ(info);

  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "PrintSetting"
PetscErrorCode PrintSetting(AppCtx *user) {
  PetscErrorCode info;
  
  info = PetscPrintf(PETSC_COMM_SELF, "Processors: %D ", user->size); CHKERRQ(info);
  info = PetscPrintf(PETSC_COMM_SELF, "Solver: " ); CHKERRQ(info);
  if (user->solver == LBFGS) {
    info = PetscPrintf(PETSC_COMM_SELF, "LBFGS " ); CHKERRQ(info);
  } else {
    info = PetscPrintf(PETSC_COMM_SELF, "BMRM " ); CHKERRQ(info);
  }
    
  info = PetscPrintf(PETSC_COMM_SELF, "lmax: %e, lmin: %e, lstep: %g ", user->lambda_max, user->lambda_min, user->lambda_step); CHKERRQ(info);
  info = PetscPrintf(PETSC_COMM_SELF, "m_train: %D ", user->m); CHKERRQ(info);
  info = PetscPrintf(PETSC_COMM_SELF, "m_test: %D ", user->tm); CHKERRQ(info);
  info = PetscPrintf(PETSC_COMM_SELF, "dim: %D ", user->dim);
  info = PetscPrintf(PETSC_COMM_SELF, "classes: %D\n", user->nclasses);
  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "AllocateWorkSpace"
PetscErrorCode AllocateWorkSpace(Vec *w, AppCtx *user) {
  PetscErrorCode     info;              
  PetscInt  m_local, tm_local, dim_local;
  
  info = MatGetLocalSize(user->data, &m_local, &dim_local);CHKERRQ(info);
  info = MatGetLocalSize(user->tdata, &tm_local, &dim_local);CHKERRQ(info);

  info = VecCreate(PETSC_COMM_WORLD, w); CHKERRQ(info); 
  info = VecSetSizes(*w, dim_local, user->dim * user->nclasses); CHKERRQ(info); 
  info = VecSetFromOptions(*w); CHKERRQ(info); 
  info = PetscObjectSetName((PetscObject) *w, "Solution"); CHKERRQ(info); 
  info = VecSet(*w, 0.0); CHKERRQ(info); 
  info = VecAssemblyBegin(*w); CHKERRQ(info); 
  info = VecAssemblyEnd(*w); CHKERRQ(info); 

  info = VecCreate(PETSC_COMM_WORLD, &user->fx);CHKERRQ(info);
  info = VecSetSizes(user->fx, m_local, user->m * user->nclasses);CHKERRQ(info);
  info = VecSetFromOptions(user->fx);CHKERRQ(info);
  info = PetscObjectSetName((PetscObject) user->fx, "Function Values");CHKERRQ(info);
  info = VecAssemblyBegin(user->fx); CHKERRQ(info); 
  info = VecAssemblyEnd(user->fx); CHKERRQ(info); 

  info = VecCreate(PETSC_COMM_WORLD, &user->tfx);CHKERRQ(info);
  info = VecSetSizes(user->tfx, tm_local, user->tm * user->nclasses);CHKERRQ(info);
  info = VecSetFromOptions(user->tfx);CHKERRQ(info);
  info = PetscObjectSetName((PetscObject) user->tfx, "Test Function Values");CHKERRQ(info);
  info = VecAssemblyBegin(user->tfx); CHKERRQ(info); 
  info = VecAssemblyEnd(user->tfx); CHKERRQ(info); 
  
  info = VecDuplicate(user->fx, &user->grad);CHKERRQ(info);

  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "DestroyWorkSpace"
PetscErrorCode DestroyWorkSpace(Vec *w, AppCtx *user) {
  PetscErrorCode     info;              

  // Free some space
  info = MatDestroy(&user->data); CHKERRQ(info);
  info = VecDestroy(&user->labels); CHKERRQ(info);
  info = VecDestroy(&user->fx); CHKERRQ(info);
  info = VecDestroy(&user->grad); CHKERRQ(info);

  info = MatDestroy(&user->tdata); CHKERRQ(info);
  info = VecDestroy(&user->tlabels); CHKERRQ(info);
  info = VecDestroy(&user->tfx); CHKERRQ(info);

  info = VecDestroy(w); CHKERRQ(info);

  return 0;
}


#undef __FUNCT__
#define __FUNCT__ "PrintFunVal"
// PrintFunVal - Print intermediate function values 
 
// Input Parameters:
// tao     - tao solver
// ctx     - user defined application context
 
// Output Parameters:
// None. Display intermediate function values  
PetscErrorCode PrintFunVal(TaoSolver tao, void *ctx) {
  AppCtx *user = (AppCtx *) ctx;
  PetscErrorCode info;
  PetscScalar f, ori_f;
  PetscScalar gnorm;
  PetscInt iter;
  PetscScalar reg = 0.0;
  PetscScalar *fx_array, *labels_array;
  Vec w, wloc;
  VecScatter scatter;
  
  PetscInt partnnz, nnz, nn;
  PetscReal *wptr;
  
  if (user->rank == 0)
    user->opt_timer.stop();
  
  info = TaoGetSolutionStatus(tao, &iter, &f, &gnorm, PETSC_NULL, PETSC_NULL, PETSC_NULL);
  
  if (user->rank == 0)
    PetscPrintf(PETSC_COMM_SELF, "%d %g %g %g ", iter, f, gnorm, user->opt_timer.wallclock_total);
  
  info = TaoGetSolutionVector(tao, &w); CHKERRQ(info);
  Evaluate(w, user);
  if (user->rank == 0)
    user->opt_timer.start();

  return 0;
}

#undef __FUNCT__
#define __FUNCT__ "Evaluate"

// Evaluate - Evaluate f-score on test set

// Input Parameters:
// w       - current weight vector
// user    - Application context

// Output Parameters:
// None. Display accuracy 

PetscErrorCode Evaluate(Vec w, AppCtx* user) {  
  PetscErrorCode    info;
  PetscScalar *fx_array, *labels_array;
  PetscScalar micro_fscore, macro_fscore;
  PetscScalar *tfx_array, *tlabels_array;
  PetscScalar tmicro_fscore, tmacro_fscore;
  Vec fx_global;
  Vec tfx_global;
  Vec lbs_global;
  Vec tlbs_global;
  VecScatter scatter;
  PetscInt  m_local, tm_local, dim_local;
  
  info = MatGetLocalSize(user->data, &m_local, &dim_local);CHKERRQ(info);
  info = MatGetLocalSize(user->tdata, &tm_local, &dim_local);CHKERRQ(info);
  m_local = m_local/user->nclasses;
  tm_local = tm_local/user->nclasses;
  dim_local = dim_local/user->nclasses;
  
  info = MatMult(user->data, w, user->fx);CHKERRQ(info);
 
  //Gather local fx
  info = VecScatterCreateToZero(user->fx, &scatter, &fx_global); CHKERRQ(info);
  info = VecScatterBegin(scatter, user->fx, fx_global, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(info);
  info = VecScatterEnd(scatter, user->fx, fx_global, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(info);
  info = VecScatterDestroy(&scatter); CHKERRQ(info);
  info = VecGetArray(fx_global, &fx_array); CHKERRQ(info);
  
  //Gather local labels
  info = VecScatterCreateToZero(user->labels, &scatter, &lbs_global); CHKERRQ(info);
  info = VecScatterBegin(scatter, user->labels, lbs_global, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(info);
  info = VecScatterEnd(scatter, user->labels, lbs_global, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(info);
  info = VecScatterDestroy(&scatter); CHKERRQ(info);
  info = VecGetArray(lbs_global, &labels_array); CHKERRQ(info);

  if (user->rank == 0){
    evaluation::fscore(fx_array, labels_array, user->m, &macro_fscore, &micro_fscore, user);
    PetscPrintf(PETSC_COMM_SELF, "%f %f ", macro_fscore, micro_fscore);
  }

  info = MatMult(user->tdata, w, user->tfx);CHKERRQ(info);
  
  //Gather local tfx
  info = VecScatterCreateToZero(user->tfx, &scatter, &tfx_global); CHKERRQ(info);
  info = VecScatterBegin(scatter, user->tfx, tfx_global, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(info);
  info = VecScatterEnd(scatter, user->tfx, tfx_global, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(info);
  info = VecScatterDestroy(&scatter); CHKERRQ(info);
  info = VecGetArray(tfx_global, &tfx_array); CHKERRQ(info);
  
  //Gather local tlabels
  info = VecScatterCreateToZero(user->tlabels, &scatter, &tlbs_global); CHKERRQ(info);
  info = VecScatterBegin(scatter, user->tlabels, tlbs_global, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(info);
  info = VecScatterEnd(scatter, user->tlabels, tlbs_global, INSERT_VALUES, SCATTER_FORWARD); CHKERRQ(info);
  info = VecScatterDestroy(&scatter); CHKERRQ(info);
  info = VecGetArray(tlbs_global, &tlabels_array); CHKERRQ(info);

  if (user->rank == 0){
    evaluation::fscore(tfx_array, tlabels_array, user->tm, &tmacro_fscore, &tmicro_fscore, user);
    PetscPrintf(PETSC_COMM_SELF, "%f %f\n", tmacro_fscore, tmicro_fscore);
  }
  return 0;
}



#undef __FUNCT__
#define __FUNCT__ "ScalarLossGradBForce"
// ScalarLossGradBForce - Evaluates a scalar loss and its gradient. 

// Input Parameters:
// w       - current weight vector
// ctx     - optional user-defined context

// Output Parameters:
// G - vector containing the newly evaluated gradient
// f - function value

PetscErrorCode ScalarLossGradBForce(TaoSolver tao, Vec w, double *f, Vec G, void *ctx) {  
  AppCtx *user = (AppCtx *) ctx; 
  PetscErrorCode info;
  PetscReal reg = 0.0;
  PetscScalar *fx_array, *labels_array, *grad_array;
  PetscInt low, high;
  
  user->num_fn_eval++;
  
  if (user->rank == 0)
    user->objgrad_timer.start();

  if (user->rank == 0)
    user->matvec_timer.start();

  info = MatMult(user->data, w, user->fx);CHKERRQ(info);

  if (user->rank == 0)
    user->matvec_timer.stop();

    
  // Master node computes the function and gradient coefficients 
  double partloss = 0.0;
  PetscInt low1, high1;
  info = VecGetOwnershipRange(user->labels, &low1, &high1);CHKERRQ(info);
  info = VecGetArray(user->fx, &fx_array); CHKERRQ(info);
  info = VecGetArray(user->labels, &labels_array); CHKERRQ(info);
  info = VecGetArray(user->grad, &grad_array);  CHKERRQ(info);
    
  loss_grad(fx_array, labels_array, grad_array, high1-low1, &partloss, user);
  
  info = VecRestoreArray(user->fx, &fx_array); CHKERRQ(info);
  info = VecRestoreArray(user->labels, &labels_array); CHKERRQ(info);
  info = VecRestoreArray(user->grad, &grad_array);  CHKERRQ(info);

  MPI_Barrier(PETSC_COMM_WORLD);
  *f = 0.0;
  MPI_Allreduce(&partloss, f, 1, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD);

  // Now everybody can compute the gradient
  if (user->rank == 0)
    user->matvec_timer.start();

  info = MatMultTranspose(user->data, user->grad, G); CHKERRQ(info);

  if (user->rank == 0)
    user->matvec_timer.stop();

  // We need to add regularization only for LBFGS
  if (user->solver == LBFGS) {
    info = VecDot(w, w, &reg); CHKERRQ(info);
    reg *= user->lambda/2.0;
    *f = *f + reg;
    info = VecAXPY(G, user->lambda, w); CHKERRQ(info);
  }

  if (user->rank == 0)
    user->objgrad_timer.stop();  

  return 0;
}


