/*
 * Copyright (c) 2019 Parameswaran Raman, Sriram Srinivasan, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef I_PARAM
#define I_PARAM

/*
 * This is an abstract class for any global parameter in 
 * nomad framework. For example check: VectorParam.hpp
 */
class IParam{
    protected:
        //This parameter should not be packed as it 
        //is reset everytime it is transmitted.
        int num_visits_;
        //This is for unloading all components of Param 
        //from the char* that was recieved.
        virtual void unpack_from(char* ptr) = 0;
        //This is for loading all components of Param to
        //char* that is being passed. This is compliment
        //of unpack_from.
        virtual void pack_to(char* ptr) = 0;
        //This the amount of extra bytes that is being 
        //added by the specific parameter. This is used
        //to compute total number of bytes needed to be
        //transmitted.
        virtual int get_extra_bytes() = 0;
        IParam():num_visits_(0),flag(0){}
    public:
        //BUGBUG: flag used in test to make sure
        //to throw error if a parameter revisits 
        //a machine. This however might be a big
        //overhead if original parameter has only 
        //a single number to be transmitted.
        long flag;
        int get_visits(){
            return num_visits_;
        }
        void increment_visits(){
            num_visits_++;
        }
        void reset_visits(){
            num_visits_ = 0;
        }
        void unpack(char* ptr){
            reset_visits();
            long* l_ptr = (long*)ptr;
            flag = *l_ptr; 
            l_ptr++;
            ptr = (char*)l_ptr;
            unpack_from(ptr);
        }
        void pack(char* ptr){
            long* l_ptr = (long*)ptr;
            *l_ptr = flag; 
            l_ptr++;
            ptr = (char*)l_ptr;
            pack_to(ptr);
        }
        int get_bytes(){
            return sizeof(long) + get_extra_bytes();
        }
        virtual ~IParam(){};
};

#endif
