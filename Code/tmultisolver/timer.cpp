/*
 * Copyright (c) 2019 Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#include "timer.hpp"
#include <algorithm>

timer::timer()
  :n(0),
   cpu_cur(0),
   wallclock_cur(0),
   cpu_max(0),
   cpu_min(99999999),
   cpu_avg(0),
   cpu_total(0),
   wallclock_max(0),
   wallclock_min(99999999),
   wallclock_avg(0),
   wallclock_total(0){}

void timer::start(){
  times(&begin); 
  cpu_cur=((double)begin.tms_utime+(double)begin.tms_stime)/TIMES_TICKS_PER_SEC;
  gettimeofday(&wallclock, NULL);
  wallclock_cur=(double)wallclock.tv_sec+(double)wallclock.tv_usec/1e6;
}


void timer::stop(){  
  n++;
  
  times(&end);
  cpu_cur=(((double)end.tms_utime+(double)end.tms_stime)/TIMES_TICKS_PER_SEC)-cpu_cur;  
  cpu_total+=cpu_cur;
  cpu_max=std::max(cpu_max,cpu_cur);
  cpu_min=std::min(cpu_min,cpu_cur);
  cpu_avg=cpu_total/n;
  
  gettimeofday(&wallclock, NULL);
  wallclock_cur=((double)wallclock.tv_sec+((double)wallclock.tv_usec/1e6))-wallclock_cur;
  wallclock_total+=wallclock_cur;
  wallclock_max=std::max(wallclock_max,wallclock_cur);
  wallclock_min=std::min(wallclock_min,wallclock_cur);  
  wallclock_avg=wallclock_total/n;
}


void timer::reset(){
  n=0;
  cpu_max=0;
  cpu_min=99999999;
  cpu_avg=0;
  cpu_total=0;
  cpu_cur=0;
  
  wallclock_max=0;
  wallclock_min=99999999;
  wallclock_avg=0;
  wallclock_total=0;
  wallclock_cur=0;
}
