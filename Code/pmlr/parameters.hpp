/*
 * Copyright (c) 2019 Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */


#ifndef __PARAMETERS_HPP
#define __PARAMETERS_HPP

#include <boost/program_options.hpp> 

using namespace std; 

namespace po = boost::program_options;

class parameters{

public:
  
  string train_file;
  string test_file;
  string model_file;
  double lambda;
  size_t max_iter;
  
  parameters(int argc, char** argv){
    po::options_description desc("supported options");
    desc.add_options()
      ("help,h", "Display this help message")
      ("train", po::value<string>(&train_file)->default_value("./a1a.txt"), "Training data file")
      ("test", po::value<string>(&test_file)->default_value("./a1a.txt"), "Test data file")
      ("model", po::value<string>(&model_file)->default_value("./pmlr.model"), "Save model to file")
      ("lambda,l", po::value<double>(&lambda)->default_value(0.001), "Regularization parameter")
      ("max_iter,m", po::value<size_t>(&max_iter)->default_value(100), "Maximum outer iterations"); 
    
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).run(), vm);
    
    if(vm.count("help") ){ 
      cout << "parallel multiclass logistic regression" << std::endl 
           << desc << std::endl; 
      exit(0);
    } 
    po::notify(vm);
  }
  
};
#endif
