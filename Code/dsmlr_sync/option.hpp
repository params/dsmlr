/*
 * Copyright (c) 2019 Parameswaran Raman, Shin Matsushima, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef OPTION_HPP_
#define OPTION_HPP_

#include "dsmlr.hpp"

#include <iostream>

#include <boost/program_options.hpp>

namespace dsmlr{

  namespace po = boost::program_options;
  using OptionDescription = po::options_description;

  /// Generic option parser
  class Option{
    
  public:
    /** 
     * Option constructor
     * 
     * @param option_name the name of the option that will be printed in the help
     */
    Option(const char *option_name) : 
      opt_desc_(option_name)
    {
      opt_desc_.add_options()
        ("help", "produce help message")
        ("seed",
         po::value<rng_type::result_type>(&rng_seed_)->default_value(12345),
         "seed value of random number generator")
        ("max_iter",
         po::value<size_t>(&max_iter_)->default_value(100),
         "maximum number of iterations")
        ("eval_period",
         po::value<size_t>(&eval_period_)->default_value(1),
         "frequency of evaluation on test data")
        ("lambda,l",
         po::value<scalar>(&lambda_)->default_value(1.0),
         "regularization parameter")
        ("eta,e",
         po::value<scalar>(&eta_)->default_value(0.001),
         "learning rate")
        // svnvish: BUGBUG
        // Use an enum
        ("solver",
         po::value<int>(&solver_)->default_value(1),
         "solver type (1=exact A, 2=w latest, 3=w latest smart)")
        ("smart_init",
         po::value<bool>(&smart_init_)->default_value(false),
         "enable smart initialization?")
        ("inner_repeat",
         po::value<size_t>(&inner_repeat_)->default_value(1),
         "How many rounds of SGD per inner epoch")
        ("bs_period",
         po::value<size_t>(&bs_period_)->default_value(1),
         "frequency of bulk synchronization")
        ("shuffle",
        po::value<bool>(&shuffle_)->default_value(true),
        "shuffle local blocks of data? (default=true)")
        ("train",
         po::value<string>(&train_file_name_)->default_value(""),
         "file name of training data txt files")   
        ("test",
         po::value<string>(&test_file_name_)->default_value(""),
         "file name of testing data txt files")   
        ("meta",
         po::value<string>(&meta_file_name_)->default_value(""),
         "file name of meta data txt files")   
        ("chunk_size",
         po::value<size_t>(&chunk_size_)->default_value(100.0),
         "how many parameters to communicate per MPI send/receive call")
        ;
    }
    
    /** 
     * Parse the command line arguments
     * 
     * @param argc number of arguments
     * @param argv string array
     * 
     * @return true if succeed, false if failed
     */
    bool parse_arguments(int argc, char **argv){
      bool flag_help = false;

      po::variables_map vm;
      po::store(po::parse_command_line(argc, argv, opt_desc_), vm);
      po::notify(vm);

      if(vm.count("help")){
        flag_help = true;
      }else{
        // check whether the current option is good to go
        if(!interpret()){
          flag_help = true;
        }
      }

      if(flag_help == true){
        // only the master prints the option description
        if(Environment::get_instance().is_master()){
          std::cerr << opt_desc_ << std::endl;
        }
        return false;
      }

      return true;
    }
    
    // seed of RNG
    rng_type::result_type rng_seed_;

    // max number of iterations
    size_t max_iter_;

    // Frequency of evaluation on test data
    size_t eval_period_;

    // regularization parameter
    scalar lambda_;

    // learning rate 
    scalar eta_;

    // Type of solver. FIXME: Make me an enum
    int solver_;

    // Perform smart initialization?
    bool smart_init_;

    // How many rounds of SGD per inner epoch
    size_t inner_repeat_;

    // After how many iterations do we bulk synchronize
    size_t bs_period_;

    // Shuffle local blocks of data?
    // Note: This can be turned off to verify if two implementations
    // match (sometimes turning it on leads to slighly differing obj values
    // due to random seed issues)
    // BUGBUG: fix this later
    bool shuffle_;

    // Name of data files
    string train_file_name_;
    string test_file_name_;
    string meta_file_name_;

    // Number of parameters to communicate per MPI send/receive call
    size_t chunk_size_;
  
  protected:
    /** 
     * Interpret parsed arguments.
     * For example, convert parsed string to enumeration type.
     * 
     * @return true if arguments were valid, false otherwise
     */
    virtual bool interpret(){
      return true;
    }

    /// option descriptor from Boost library, which will do the actual parsing
    OptionDescription opt_desc_;
    
  };
  
}

#endif
