#! /bin/bash

#mpirun -np 1 ./dsmlrtest --train ~/data/wine/train2.txt --test ~/data/wine/test2.txt --nthreads 1 -t 3 -l 0.0001 -e 100

./dsmlrtest --train ~/data/wine/train2.txt --test ~/data/wine/test2.txt --meta ~/data/wine/meta.txt --nthreads 1 -t 3 -l 0.0001 -e 100
