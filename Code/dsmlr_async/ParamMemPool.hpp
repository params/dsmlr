/*
 * Copyright (c) 2019 Parameswaran Raman, Sriram Srinivasan, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef PARAM_MEM_POOL
#define PARAM_MEM_POOL

#include <tbb/tbb.h>
#include "IParamGenerator.hpp"
#include "IParam.hpp"

/*
 * This is a memory management class. This is used
 * by the framework to get memory for global parameter
 * when receiving and store memory location after 
 * sending data.
 */
class ParamMemPool{
    private:
        IParamGenerator *param_gen;
        tbb::concurrent_queue<IParam*> available_space;
        //pool_size_ < 0 indicates unlimited pool size.
        int pool_size_;
    public:
        ParamMemPool(IParamGenerator *param_gen, int pool_size):
            param_gen(param_gen),
            available_space(),
            pool_size_(pool_size)
    {
        for(int i = 0 ; i < pool_size_ ; i++){
            available_space.push(param_gen->generate_parameter());
        }
    }

        ~ParamMemPool(){
            while(true){
                IParam* temp_p_holder;
                if(available_space.try_pop(temp_p_holder)){
                    delete temp_p_holder;
                } else { 
                    break;
                }
            }
        }

        void push(IParam *p_holder){
            available_space.push(p_holder);
        }

        IParam* pop(){
            IParam* param = nullptr;
            bool success = available_space.try_pop(param);
            if (!success && pool_size_ < 0){
                param = param_gen->generate_parameter();
            }
            return param;
        }

};

#endif
