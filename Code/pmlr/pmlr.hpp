/*
 * Copyright (c) 2019 Parameswaran Raman, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */


#ifndef __PMLR_HPP
#define __PMLR_HPP

#include <numeric>
#include <cmath>
#include <limits>

#include "parameters.hpp"
#include "timer.hpp"

#include <boost/tokenizer.hpp>

#include "stdafx.h"
#include "optimization.h"

#undef SEEK_SET
#undef SEEK_END 
#undef SEEK_CUR
#include "mpi.h"

using namespace std; 
using namespace alglib;

typedef vector<size_t> labels;
typedef vector< vector<size_t> > idxvec;
typedef vector< vector<double> > valvec;

// A forwarding function which calls fun_grad of the PMLR class. This is
// a workaround for the limitation of alglib
void dummy(const real_1d_array &x, double &func, real_1d_array &grad, void *ptr);
const double epsg_min = 1e-9;
//const double epsg_min = 1e-18;
const double epsf = 0;
const double epsx = 0;
//const ae_int_t maxits = 0;
const ae_int_t maxits = 20;
const ae_int_t m = 7;
const double minus_infty = -std::numeric_limits<double>::max();

class xi_plus{
    
public:
  double max_fx_;
  double fxy_;
  size_t yhat_;
    
  xi_plus(){
    reset();
  }
    
  void reset(void){
    max_fx_ = minus_infty;
    fxy_ = minus_infty;
    yhat_ = 0;
  }
};

// struct to compute max_k <wk, xi> and the location of max_k <wk, xi>
struct max_k{ 
  double val; 
  int   rank; 
}; 

class PMLR{

private:
  // train data
  labels y;
  idxvec xidx;
  valvec xval;

  // test data
  labels ty;
  idxvec txidx;
  valvec txval;
  
  // N - Number of training data points
  // tN - Number of test data points
  // D - dimensions
  // K - number of classes
  size_t N;
  size_t tN;
  size_t D;
  size_t K; 

  // Regularization parameter
  double lambda;
  
  // MPI rank
  size_t rank;
  size_t numprocs;

  // this processor is responsible for labels from min-label to
  // max-label 
  size_t min_label;
  size_t max_label;
  
  // Store a vector of wk weights. Each of length D.
  vector<real_1d_array> vecwk;

  // Log of a (variational parameter)
  real_1d_array loga;

  // Store auxilliary info used for prediction
  vector<xi_plus> vec_xi_;
  vector<xi_plus> vec_xi_test_;

  // Pre-allocate arrays used in reducer
  double *gmaxwkx;
  double *expwkx;
  double *gexpwkx;
  int    *gmaxidx;
  real_1d_array wk;
  struct max_k *in, *out;

  // Current label which is being optimized 
  size_t cur_label;

  // tolerance for LBFGS
  double epsg;

  // time the optimization procedure 
  timer opt_timer;

  // svnvish: BUGBUG
  // Assumption: class labels are of the form 0,...,K-1
  void read_data(const string& data_file, 
                 bool train=true){
    ifstream ifs;
    ifs.open(data_file, ifstream::in);
    
    string line;
    bool max_idx_modified=false;

    while(getline(ifs, line)){
      if(train){
        xidx.push_back(vector<size_t>(0));
        xval.push_back(vector<double>(0));
      }else{
        txidx.push_back(vector<size_t>(0));
        txval.push_back(vector<double>(0));
      }
      
      boost::char_separator<char> sep(" \t:");
      boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
      size_t count = 0;
      for (const auto& t : tokens){
        count++;
        if(count == 1){
          size_t yi = stoul(t);
          if(train){
            y.push_back(yi);
            if(yi>K)
              K = yi;
          }else{
            assert(yi < K);
            ty.push_back(yi);
          }
          continue;
        }
        if(count%2){
          if(train){
            xval[N].push_back(stod(t));
          }else{
            txval[tN].push_back(stod(t));
          }
        }else{
          unsigned long idx = stoul(t);
          if(train){
            xidx[N].push_back(idx);
          }else{
            txidx[tN].push_back(idx);
          }
          if(idx > D){
            D = idx;
            max_idx_modified = true;
          }
        }
      }
      if(train)
        N++;
      else
        tN++;
    }
    if(train){
      K++;
    }
    if(max_idx_modified){
      D++;
    }
    ifs.close();
    return;
  }

  void compute_fscore(const int* prediction, 
                      const labels& y, 
                      const size_t& N){
    vector<int> tp(K, 0);
    vector<int> fp(K, 0);
    vector<int> fn(K, 0);
    
    for(size_t i = 0; i < N; i++){
      if(y[i] == (size_t) prediction[i]){
        tp[prediction[i]]++;
      }else{
        fp[prediction[i]]++;
        fn[y[i]]++;
      }
    }
    
    double micro_f1 = 0.0;
    double macro_f1 = 0.0;
    int nr_precision = 0;
    int dr_precision = 0;
    int dr_recall = 0;
    for(size_t k = 0; k < K; k++){
      nr_precision += tp[k];
      dr_precision += tp[k] + fp[k];
      dr_recall += tp[k] + fn[k];
      
      double p = 1.0;
      double r = 1.0;
      
      if(tp[k]+fp[k] > 0)
        p = (double)tp[k]/(tp[k] + fp[k]);
      
      if(tp[k]+fn[k] > 0)
        r = (double)tp[k]/(tp[k] + fn[k]);
      
      if(p or r)
        macro_f1 += (2*p*r)/(p+r);
    }
    macro_f1 /= K;
    double p = (double) nr_precision / dr_precision; 
    double r = (double) nr_precision / dr_recall;
    if(p or r)
      micro_f1 = (2 * p * r) / (p + r);
    cout << " " << macro_f1 << " " << micro_f1;
    return;
  }

  void compute_maxwkx(const size_t& N, 
                      bool istrain){
    // first compute max_k <wk, xi> locally
    // next compute max_k <wk, xi> and the location of max_k <wk, xi>
    for(size_t i=0; i<N; i++){
        if(istrain) in[i].val = vec_xi_[i].max_fx_;
        else in[i].val = vec_xi_test_[i].max_fx_;
        in[i].rank = rank; 
    }

    MPI_Allreduce(in, out, N, MPI_DOUBLE_INT, MPI_MAXLOC, MPI_COMM_WORLD);
    
    for(size_t i = 0; i < N; i++){
      gmaxwkx[i] = out[i].val;
      gmaxidx[i] = out[i].rank;
    }
    return;
  }
  
  void compute_prediction(const size_t& N,
                          int* prediction,
                          bool istrain){
    compute_maxwkx(N, istrain);
   
    int *argmax = new int[N];
    for (size_t i=0; i<N; i++){
        argmax[i] = 0;
        if(rank == (size_t) gmaxidx[i]){
          argmax[i] = -1;
          if(istrain) argmax[i] = vec_xi_[i].yhat_;
          else argmax[i] = vec_xi_test_[i].yhat_;
        }
    } 
    MPI_Reduce(argmax, prediction, N, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    delete[] argmax;
    
    return;
  }

  void mapper(const size_t& iter){
    epsg = max(pow(0.1, (iter+1)/10.0), epsg_min);
    for(cur_label = min_label; cur_label < max_label; cur_label++){
      minlbfgsstate state;
      minlbfgsreport rep;
      minlbfgscreate(m, vecwk[cur_label - min_label], state);
      minlbfgssetcond(state, epsg, epsf, epsx, maxits);
      minlbfgsoptimize(state, &dummy, nullptr, this);      
      minlbfgsresults(state, vecwk[cur_label - min_label], rep);
      
      //Recompute vecwkx after LBFGS completes
      for(size_t i=0; i<N; i++){
        vector<size_t> &idx = xidx[i];
        vector<double> &val = xval[i];
        size_t nnz = idx.size();
        double wkxi = 0.0;
        for(size_t j = 0; j < nnz; j++)
          wkxi += val[j]*vecwk[cur_label-min_label][idx[j]];
        
        if(wkxi > vec_xi_[i].max_fx_){
          vec_xi_[i].max_fx_ = wkxi;
          vec_xi_[i].yhat_ = cur_label;
        }
      }
    }
    return;
  }

  void reducer(const size_t& iter){
    compute_maxwkx(N, true);
    fill(expwkx, expwkx + N, 0.0);
    for(cur_label = min_label; cur_label < max_label; cur_label++){
      wk = vecwk[cur_label - min_label]; 
      for(size_t i = 0; i < N; i++){
        vector<size_t> &idx = xidx[i];
        vector<double> &val = xval[i];
        size_t nnz = idx.size();
        double wkxi = 0.0;
        for(size_t j = 0; j < nnz; j++)
          wkxi += val[j]*wk[idx[j]];
        
        expwkx[i] += exp(wkxi - gmaxwkx[i]);
      }
    }
    
    fill(gexpwkx, gexpwkx + N, 0.0);
    MPI_Allreduce(expwkx, gexpwkx, N, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    
    for(size_t i = 0; i < N; i++){
      loga[i] = -(log(gexpwkx[i]) + gmaxwkx[i]);
      assert(std::isfinite(loga[i]));
    }
  }
  
  void eval(const size_t& iter){
    //reset before every test phase
    for(size_t i = 0; i < N; i++)
        vec_xi_[i].reset();
    for(size_t i=0; i < tN; i++)
        vec_xi_test_[i].reset();

    double func = 0.0;
    double gfunc = 0.0;
    
    for(cur_label = min_label; cur_label < max_label; cur_label++){
      wk = vecwk[cur_label - min_label]; 
      func += lambda * 0.5 * inner_product(wk.getcontent(), wk.getcontent() + D, wk.getcontent(), 0.0);
      for(size_t i = 0; i < N; i++){
        vector<size_t> &idx = xidx[i];
        vector<double> &val = xval[i];
        size_t nnz = idx.size();
        double wkxi = 0.0;
        for(size_t j = 0; j < nnz; j++)
          wkxi += val[j]*wk[idx[j]];

        if(wkxi > vec_xi_[i].max_fx_){
          vec_xi_[i].max_fx_ = wkxi;
          vec_xi_[i].yhat_ = cur_label;
        }
        
        if(y[i] == cur_label){
          func -= wkxi/N;
        }
      }
    }
    
    MPI_Reduce(&func, &gfunc, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    
    if(rank==0){
      gfunc -= (accumulate(loga.getcontent(), loga.getcontent() + N, 0.0)/N);
      cout << iter << " " << gfunc << " " << opt_timer.wallclock_total;
    }
    
    int prediction[N];
    compute_prediction(N, prediction, true);
    if(rank == 0){
      compute_fscore(prediction, y, N);
    }

    for(cur_label = min_label; cur_label < max_label; cur_label++){
      wk = vecwk[cur_label - min_label];
      for(size_t i = 0; i < tN; i++){
        vector<size_t> &idx = txidx[i];
        vector<double> &val = txval[i];
        size_t nnz = idx.size();
        double wkxi = 0.0;
        for(size_t j = 0; j < nnz; j++)
          wkxi += val[j]*wk[idx[j]];
        
        if(wkxi > vec_xi_test_[i].max_fx_){
          vec_xi_test_[i].max_fx_ = wkxi;
          vec_xi_test_[i].yhat_ = cur_label;
        }
      }
    }
    int tprediction[tN];
    compute_prediction(tN, tprediction, false);
    if(rank == 0){
      compute_fscore(tprediction, ty, tN);
      cout << endl;
    }
  }

  void dump_wx(const string& data_file){
    ofstream ofile;
    ofile.open(data_file);
    if(ofile.is_open()){
      for(auto& wk : vecwk){
        for(size_t i = 0; i < D; i++){
          ofile << i << ":" << wk[i] << " ";
        }
        ofile << endl;
      }
      ofile.close();
    }
    else{
      cout << "Can't open the file!" << endl;
    }
  }
  
public:
  PMLR(parameters& p, 
       size_t numprocs, 
       size_t rank): 
    N(0), tN(0), D(0), K(0), 
    lambda(p.lambda), rank(rank), numprocs(numprocs), epsg(epsg_min){
    read_data(p.train_file);
    if(rank == 0)
      cout << "N: " << N << ", D: " << D << ", K: " << K << ", numprocs: " << numprocs << endl;
    assert(K >= (size_t)numprocs);
    read_data(p.test_file, false);

    // initialize the wk's for which this processor is responsible
    const size_t labels_per_part = K/numprocs + ((K%numprocs > 0) ? 1 : 0);
    min_label = labels_per_part * rank;
    max_label = min(min_label + labels_per_part, K);    
//    cout << "rank: " << rank 
//         << " min_label: " << min_label 
//         << " max_label: " << max_label << endl;

    wk.setlength(D);
    fill(wk.getcontent(), wk.getcontent() + D, 0.0);
    vecwk = vector<real_1d_array>(max_label-min_label, wk);

    loga.setlength(N);
    fill(loga.getcontent(), loga.getcontent() + N, log(1.0/K));
    
    vec_xi_.resize(N);
    vec_xi_test_.resize(tN);

    gmaxwkx = new double[N];
    fill(gmaxwkx, gmaxwkx + N, 0.0);
    expwkx = new double[N];
    fill(expwkx, expwkx + N, 0.0);
    gexpwkx = new double[N];
    fill(gexpwkx, gexpwkx + N, 0.0);
    gmaxidx = new int[N];
    fill(gmaxidx, gmaxidx + N, 0.0);
    in = new max_k[N];
    out = new max_k[N];

    if(rank == 0){
      cout << "processors: " << numprocs << " ";
      cout << "lambda: " << lambda << " ";
      cout << "m_train: " << N << " ";
      cout << "m_test: " << tN << " ";
      cout << "dim: " << D << " ";
      cout << "classes: " << K << endl;
    }
  }

  ~PMLR(){
    delete[] gmaxwkx;
    delete[] expwkx;
    delete[] gexpwkx;
    delete[] gmaxidx;
    delete[] in;
    delete[] out;
  }

  
  void fun_grad(const real_1d_array& wk, double& func, real_1d_array& grad){
   double multi = N;
    func = multi*lambda * 0.5 * inner_product(wk.getcontent(), wk.getcontent() + D, wk.getcontent(), 0.0);
   
    for(size_t i = 0; i < D; i++)
      grad[i] = multi*lambda * wk[i];
    
    double total=0.0;
    double max_tmp = -std::numeric_limits<double>::max();
    double min_tmp = -max_tmp;
    for(size_t i = 0; i < N; i++){
      vector<size_t> &idx = xidx[i];
      vector<double> &val = xval[i];
      size_t nnz = idx.size();
      double wkxi = 0.0;
      for(size_t j = 0; j < nnz; j++)
        wkxi += val[j]*wk[idx[j]];
      
      double tmp = wkxi + loga[i] - log(N) + log(multi);
      
      if (tmp > max_tmp)
        max_tmp = tmp;
      if (tmp < min_tmp)
        min_tmp = tmp;
      
      tmp = exp(tmp);
      func += tmp;
      
      if(std::isfinite(func)){
        if(y[i] == cur_label){
          func -= wkxi/N*multi;
          tmp -= 1.0/N*multi;
        }
        for(size_t j = 0; j < nnz; j++)
          grad[idx[j]] += tmp*val[j];
      }else{
        cout << "truncated: " << cur_label << " " << i << " " << wkxi << " returning ..." << endl;
        func=1e300;
        for(size_t i = 0; i < D; i++)
          grad[i] = 0.0;
        return;
      }
    }

    return;
  }
  
  void optimize(const size_t& max_iter, const string& model_file){

    if(rank == 0)
      cout << "iter fn.val time train_macro_f1 train_micro_f1 test_macro_f1 test_micro_f1" << endl;
    for(size_t iter = 0; iter < max_iter; iter++){
      if (rank == 0)
        opt_timer.start();
      
      //reset at beginning of every iteration
      for(size_t i = 0; i < N; i++)
          vec_xi_[i].reset();
      
     
      // optimize the objective function with current value of a_i
      mapper(iter);
      
      // compute new values for a_i
      reducer(iter);
      
      if (rank == 0)
        opt_timer.stop();
      
      eval(iter);
//       if(iter % 10000 == 0 or iter == max_iter - 1)
//         dump_wx(model_file + std::to_string(iter));
    }
  }
  
};

void dummy(const real_1d_array &x, double &func, real_1d_array &grad, void *ptr){
  PMLR* m = static_cast<PMLR*>(ptr);
  m->fun_grad(x, func, grad);
  return;
}

#endif
