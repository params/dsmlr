/*
 * Copyright (c) 2019 Parameswaran Raman, Sriram Srinivasan, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef DSMLR_DATA_OPTION
#define DSMLR_DATA_OPTION

#include "../NomadOption.hpp"
#include "../nomad.hpp"
#include <iostream>

using std::cout;
using std::cerr;
using std::endl;
using nomad::NomadOption;
using std::string;

namespace po = boost::program_options;
using OptionDescription = po::options_description;

class DSMLRDataOption: public NomadOption {

  public:

    DSMLRDataOption(): NomadOption("dsmlr") {
      option_desc_.add_options()
        ("max_iter",
         po::value<size_t>(&max_iter_)->default_value(100),
         "maximum number of iterations")
        ("eval_period",
         po::value<size_t>(&eval_period_)->default_value(1),
         "frequency of evaluation on test data")
        ("lambda,l",
         po::value<scalar>(&lambda_)->default_value(1.0),
         "regularization parameter")
        ("eta,e",
         po::value<scalar>(&eta_)->default_value(0.001),
         "learning rate")
        ("drate",
         po::value<scalar>(&decay_rate_)->default_value(0.1),
         "decay rate")
        ("solver",
         po::value<int>(&solver_)->default_value(1),
         "solver type (1=exact A, 2=w latest, 3=w latest smart)")
        ("smart_init",
         po::value<bool>(&smart_init_)->default_value(false),
         "enable smart initialization?")
        ("inner_repeat",
         po::value<size_t>(&inner_repeat_)->default_value(1),
         "How many rounds of SGD per inner epoch")
        ("bs_period",
         po::value<size_t>(&bs_period_)->default_value(1),
         "frequency of bulk synchronization")
        ("shuffle",
         po::value<bool>(&shuffle_)->default_value(true),
         "shuffle local blocks of data?")
        ("train",
         po::value<string>(&train_file_name_)->default_value(""),
         "file name of training data txt files")   
        ("test",
         po::value<string>(&test_file_name_)->default_value(""),
         "file name of testing data txt files") 
        ("meta",
         po::value<string>(&meta_file_name_)->default_value(""),
         "file name of meta data txt files") 
        ("chunk_size",
         po::value<size_t>(&chunk_size_)->default_value(100.0),
         "how many parameters to communicate per MPI send/receive call")
        ("output_local", po::value<string>(&output_path_local_)->default_value(""), 
         "path of the file the local params will be printed into")
        ("output_pred", po::value<string>(&output_path_pred_)->default_value(""), 
         "path of the file the predicted rank will be printed in");
    }

    // max number of iterations
    size_t max_iter_;

    // Frequency of evaluation on test data
    size_t eval_period_;

    // regularization parameter
    scalar lambda_;

    // learning rate 
    scalar eta_;

    // decay rate 
    scalar decay_rate_;

    // Type of solver. FIXME: Make me an enum
    int solver_;

    // Perform smart initialization?
    bool smart_init_;

    // How many rounds of SGD per inner epoch
    size_t inner_repeat_;

    // After how many iterations do we bulk synchronize
    size_t bs_period_;

    // Shuffle local blocks of data?
    // Note: This can be turned off to verify if two implementations
    // match (sometimes turning it on leads to slighly differing obj values
    // due to random seed issues)
    // BUGBUG: fix this later
    bool shuffle_;

    // Name of data files
    string train_file_name_;
    string test_file_name_;
    string meta_file_name_;

    // Number of parameters to communicate per MPI send/receive call
    size_t chunk_size_;

    // Path of the output file 
    string output_path_local_;

    // Path of the output pred rank 
    string output_path_pred_;

    /*static DSMLRDataOption* get_options(int &argc, char **& argv){
      if(option == NULL){
      option = new DSMLRDataOption();
      if(!option->parse_command(argc, argv)){
      exit(1);
      }
      }
      return option;
      }

      static DSMLRDataOption* get_options(){
      if(option == NULL){
      std::cerr<<"OPTIONS NEED TO BE INITIALIZED WITH args.\n";
      exit(1);
      }
      return option;
      }*/

    virtual bool is_option_OK() {
      if (train_file_name_.length() <= 0 || meta_file_name_.length() <= 0) {
        cerr << "--train and --meta has to be specified." << endl;
        return false;
      }
      return NomadOption::is_option_OK();
    }
};

#endif
