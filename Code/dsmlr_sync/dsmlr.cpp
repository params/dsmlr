#include "dsmlr.hpp"
#include "data.hpp"
#include "mlr_optimizer.hpp"

#include <iostream>

enum solvers {onedelay = 1, zerodelay = 2, mixeddelay = 3};

int main(int argc, char **argv) {

  // although we support lazy evaluation, for clarity just initialize here
  dsmlr::Environment& env = dsmlr::Environment::get_instance();
  const auto rank = env.get_rank();

  dsmlr::master_log("Starting MLR solver!");

  // parse the option
  dsmlr::Option option("DS-MLR options");
  if (!option.parse_arguments(argc, argv)) {
    return 1;
  }

  // seed the RNG
  env.get_rng().seed(option.rng_seed_ + rank * 137);

  // load data
  dsmlr::Data data;
  bool succeed = data.read<int>(option.train_file_name_, option.test_file_name_, option.meta_file_name_);
  if(!succeed){
    return 11;
  }
  dsmlr::MlrOptimizer optimizer(option, data);
  bool ret_code = false;
  
  //svnvish: BUGBUG
  // use enum
  switch(option.solver_){
  case onedelay:
    ret_code = optimizer.one_delay();
    break;
  case zerodelay:
    ret_code = optimizer.zero_delay();
    break;
  case mixeddelay:
    ret_code = optimizer.mixed_delay();
    break;
  default:
    dsmlr::print_error(dsmlr::format("Unknown optimizer"));      
    break;
  }
  
  if(!ret_code)
    dsmlr::print_error(dsmlr::format("Optimizer failed"));      
  
  env.print_log("Terminating...");
  
  return 0;

}

