/*
 * Copyright (c) 2019 Parameswaran Raman, Sriram Srinivasan, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef I_UPDATER
#define I_UPDATER

#include <iostream>
#include "nomad.hpp"
#include "IParam.hpp"

/*
 * This is an interface for writing any type of updater. For example check MFUpdater.hpp.
 * Any ML algorithm that wants to use nomad framework can be written by writing an updater
 * that inherits from this class and configure runner with it.
 */
class IUpdater{
    public:
        //Initialize local parameter and load data.
        virtual void initialize(int task_id, int num_tasks, int thread_id, int num_threads) = 0;
        //Run the updater that updates both global parameters 
        //that is passed to it and local parameters.
        virtual void update(IParam *param) = 0;
        //Run before update is called.
        virtual void prepare_update(){}
        //Called after update is complete
        virtual void post_update(){}
        //Called after test.
        virtual void post_test(){}
        //Run test to compute and save error or other information
        //about the train and test data.
        virtual void test(IParam *param) = 0;
        //Called after everything is done and before destruction.
        virtual void finalize() = 0;
        //Return how many data points are present that will affect 
        //the global parameter for the specific column. Can be used
        //to do load balancing among threads, etc.
        //Example:
        //in matrix factorization which columns have nonzero data.
        virtual int get_num_data_for_col(int col_index) = 0;
        //Total number of times parameters were updated.
        virtual long long get_num_updates() = 0;
        //This is called everytime before starting testing phase.
        //i.e everytime before test is called for all parameters.
        virtual void prepare_test() = 0;

};

#endif
