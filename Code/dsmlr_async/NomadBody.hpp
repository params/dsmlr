/*
 * Copyright (c) 2019 Parameswaran Raman, Sriram Srinivasan, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */


#ifndef NOMAD_NOMAD_BODY_HPP_
#define NOMAD_NOMAD_BODY_HPP_

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <condition_variable>
#include "tbb/tbb.h"
#include <tbb/compat/thread>

#include "nomad.hpp"
#include "NomadOption.hpp"
#include "ParamMemPool.hpp"
#include "IUpdater.hpp"
#include "IParam.hpp"
#include "Logger.hpp"


using std::cout;
using std::cerr;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::endl;
using std::ios;
using std::to_string;

using std::vector;
using std::pair;
using std::string;

using tbb::atomic;
using tbb::tick_count;

using nomad::NomadOption;

typedef tbb::concurrent_queue<IParam*> colque;


/*
 * This is the main class that runs the algorithm.
 * Any new algorithm needs to inherit from this and 
 * provide the required implementation for all functions.
 * For example check MFBody.hpp
 */
class NomadBody {

    protected:
        //Command line arguments will be read via this options object.
        virtual NomadOption *get_option() = 0;

        //Number of updaters should be same as number of threads being
        //used. If not an exception will be thrown.
        virtual vector<IUpdater*> get_updaters() = 0;

        //This will be used in mempry pool to create memory on demand.
        virtual IParamGenerator* get_param_generator() = 0;

        //Number of global parameter that exist in total.
        virtual int get_num_cols() = 0;

         //Initialize options. It is called in run before everything.
        virtual void initialize_options(int argc, char **argv) = 0;

        //This will be called first after initializing MPI. Ideally you 
        //would want to initialize all your parameters here and use later.
        virtual void initialize() = 0;

        //Will be called once everything is finished. before clearing memory.
        //Usually you save parameters to disc here.
        virtual void finalize(vector<IParam*> saved_columns) = 0;

        //This will be called at end of every iteration after running test in 
        //IUpdater. You can compute your own statistics and print or save here.
        virtual void compute_statistics(vector<IParam*> saved_columns, int iter_num, double time_iter) = 0;

        //This is required to initialize the global parameters.
        virtual IParam* get_initial_param(int col_index) = 0;

        //MPI variables that is available to the original algorithm.
        int numtasks, rank, hostname_len;
        char hostname[MPI_MAX_PROCESSOR_NAME];

    public:
        int run(int argc, char **argv) {

            initialize_options(argc, argv);

            Logger &logger = *Logger::get_logger();
            nomad::NomadOption *p_option = get_option();
            nomad::NomadOption &option = *p_option;

            if(option.debug_){
                logger.enable_debug();
            } else {
                logger.disable_debug();
            }
            if(option.info_){
                logger.enable_info();
            } else {
                logger.disable_info();
            }

            // check whether MPI provides multiple threading
            int mpi_thread_provided;
            MPI_Init_thread(nullptr, nullptr, MPI_THREAD_MULTIPLE, &mpi_thread_provided);
            if (mpi_thread_provided != MPI_THREAD_MULTIPLE) {
                cerr << "MPI multiple thread not provided!!! (" << mpi_thread_provided << " != " << MPI_THREAD_MULTIPLE << ")" << endl;
                exit(1);
            }

            // retrieve MPI task info
            MPI_Comm_rank(MPI_COMM_WORLD, &rank);
            MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
            MPI_Get_processor_name(hostname, &hostname_len);
            Logger::set_machine_num(rank);

            initialize();
            logger.info((boost::format("processor name: %s, number of tasks: %d, rank: %d") % hostname % numtasks % rank).str(), -1);

            // read number of columns
            int global_num_cols = get_num_cols();

            // create a column pool with big enough size
            // this serves as a memory pool. global_num_cols * 3 / num_parts is arbitrary big enough number.
            // when the capacity is exceeded, it automatically assigns additional memory.
            // therefore no need to worry too much
            int seed = option.seed_ + rank * 131 + 139;
            IParamGenerator* p_gen = get_param_generator();
            ParamMemPool column_pool(p_gen, option.mempool_size_ * global_num_cols);

            //Get updaters and check there thread number of updaters.
            vector<IUpdater*> updaters = get_updaters();
            if(updaters.size() != option.num_threads_){
                logger.error("number of updaters initialized not same as number of threads.", -1);
                exit(2);
            }

            // setup initial queues of columns
            // each thread owns each queue with corresponding access
            colque *job_queues = new colque[option.num_threads_];

            // a queue of columns to be sent to other machines via network
            colque send_queue;

            // count the number of threads in the machine which initial setup for training is done
            atomic<int> count_setup_threads;
            count_setup_threads = 0;

            // this flag will be turned on when all threads are ready for training
            atomic<bool> flag_train_ready;
            flag_train_ready = false;

            // this flag will be used to send signals to all threads that it has to stop training
            atomic<bool> flag_train_stop;
            flag_train_stop = false;

            // this flag will be turned on when all threads are ready for testing
            atomic<bool> flag_test_ready;
            flag_test_ready = false;

            // this flag will be used to send signals to all threads that it has to stop testing
            atomic<bool> flag_test_stop;
            flag_test_stop = false;

            // maintain the number of updates and the number of pop failures for each thread
            long long *num_updates = new long long[option.num_threads_];
            long long *num_failures = new long long[option.num_threads_];

            std::fill_n(num_updates, option.num_threads_, 0);
            std::fill_n(num_failures, option.num_threads_, 0);

            // used to compute the number of empty columns inside a machine
            // BUGBUG: right now it does not have any purpose other than computing statistics
            // we may enhance the efficiency of communication by taking advantage of this information
            atomic<bool> *is_column_empty = new atomic<bool>[global_num_cols];
            for (int i = 0; i < global_num_cols; i++) {
                is_column_empty[i] = true;
            }

            // array used to remember the sizes of send_queue in each machine
            atomic<int> *machine_queue_current_sizes = new atomic<int>[numtasks];
            for (int i = 0; i < numtasks; i++) {
                machine_queue_current_sizes[i] = 0;
            }

            atomic<int> wait_number;
            wait_number = 0;

            /********************************************************/
            /* Define Updater Thread                                */
            /********************************************************/
            std::function<void(int)> updater_func = [&](int thread_index)->void {
                //Initialize updater.
                updaters[thread_index]->initialize(rank, numtasks, thread_index, option.num_threads_);

                //Update is_column_empty with if column has values to be updated or not.
                for (int i=0; i < global_num_cols; i++) {
                    if (updaters[thread_index]->get_num_data_for_col(i) > 0) {
                        is_column_empty[i].compare_and_swap(false, true);
                    }
                }

                const double prob_reuse = option.prob_reuse_;
                long long local_num_failures = 0;
                const int num_threads = option.num_threads_;
                // notify that the thread is ready to run
                count_setup_threads++;

                for (unsigned int timeout_iter = 0; timeout_iter < option.timeouts_.size(); timeout_iter++) {

                    logger.info("Ready to train.", thread_index);

                    // wait until all threads are ready
                    while (flag_train_ready == false) {
                        std::this_thread::yield();
                    }
                    updaters[thread_index]->prepare_update();

                    /////////////////////////////////////////////////////////
                    // Training
                    /////////////////////////////////////////////////////////

                    int num_units = -1;
                    int smallest_job_queue_index = 0;

                    while (flag_train_stop == false) {

                        IParam *ip_col;
                        bool pop_succeed = job_queues[thread_index].try_pop(ip_col);

                        if (pop_succeed) { // there was an available column in job queue to process

                            updaters[thread_index]->update(ip_col);

                            ip_col->increment_visits();
                            num_units++;

                            // send to the next thread
                            if (numtasks == 1 || ip_col->get_visits() < option.num_threads_ * prob_reuse) {
                                if (num_units % option.units_per_msg_ == 0) {
                                    smallest_job_queue_index = 0;
                                    int smallest_job_queue_size = job_queues[0].unsafe_size();
                                    for (int j = 1; j < option.num_threads_; j++) {
                                        int job_queue_size = job_queues[j].unsafe_size();
                                        if (smallest_job_queue_size > job_queue_size) {
                                            smallest_job_queue_size = job_queue_size;
                                            smallest_job_queue_index = j;
                                        }
                                    }
                                }
                                job_queues[smallest_job_queue_index].push(ip_col);
                            }
                            else {
                                send_queue.push(ip_col);
                            }
                        }
                        else {
                            local_num_failures++;
                            std::this_thread::yield();
                        }
                    }
                    updaters[thread_index]->post_update();

                    num_updates[thread_index] = updaters[thread_index]->get_num_updates();
                    num_failures[thread_index] = local_num_failures;

                    while (flag_test_ready == false) {
                        std::this_thread::yield();
                    }

                    /////////////////////////////////////////////////////////
                    // Testing
                    /////////////////////////////////////////////////////////

                    int num_col_processed = 0;

                    updaters[thread_index]->prepare_test();

                    int monitor_num = 0;
                    tbb::tick_count start_time = tbb::tick_count::now();

                    // test until every column is processed
                    while (num_col_processed < global_num_cols) {

                        double elapsed_seconds = (tbb::tick_count::now() - start_time).seconds();
                        if (monitor_num < elapsed_seconds) {
                            logger.info("Test updater alive: sec = " + to_string(monitor_num) + 
                                    ", col_processed = " + to_string(num_col_processed) + 
                                    " out of " + to_string(global_num_cols), thread_index);
                            monitor_num++;
                        }

                        IParam *ip_col;

                        if (job_queues[thread_index].try_pop(ip_col)) {
                            updaters[thread_index]->test(ip_col);
                            if (thread_index < num_threads - 1) {
                                job_queues[thread_index + 1].push(ip_col);
                            }
                            else {
                                send_queue.push(ip_col);
                            }
                            num_col_processed++;

                        }
                        else {
                            std::this_thread::yield();
                        }

                    }
                    updaters[thread_index]->post_test();

                    // notify that this thread has finished testing
                    count_setup_threads++;
                }

                //FINALIZE IS COMMENTED HERE BUT IS CALLED IN MAIN THREAD AFTER THIS THREAD JOINS MAIN.
                //updaters[thread_index]->finalize();
                return;

            };
            /********************************************************/
            /* End of Definition of Updater Thread                  */
            /********************************************************/


            // create and run updater threads
            std::thread* updater_threads = new std::thread[option.num_threads_];
            for (int i = 0; i < option.num_threads_; i++) {
                updater_threads[i] = std::thread(updater_func, i);
            }
            while (count_setup_threads < option.num_threads_) {
                // wait until data loading and initialization of rows are done in every updater thread
                std::this_thread::yield();
            }

            /////////////////////////////////////////////////////////
            // Initialize Columns
            /////////////////////////////////////////////////////////
            rng_type rng(option.seed_ + rank * 131 + 139);
            rng_type rng_temp(option.seed_ + rank + 137);
            std::uniform_int_distribution<> rand_dist(0, option.num_threads_ - 1);

            int columns_per_machine = global_num_cols / numtasks + ((global_num_cols % numtasks > 0) ? 1 : 0);
            int col_start = columns_per_machine * rank;
            int col_end = std::min(columns_per_machine * (rank + 1), global_num_cols);

            // generate columns
            for (int i = col_start; i < col_end; i++) {
                // create a column
                IParam *p_col = get_initial_param(i);

                int rand_thread_index = rand_dist(rng_temp);

                // push to the job queue
                job_queues[rand_thread_index].push(p_col);
            }

            // shihao
            logger.info("--------job_queue_current_sizes--------", -1);
            for (int j = 0; j < option.num_threads_; j++) {
                logger.info("Queue: " + to_string(j) + 
                        ", Size: " + to_string(job_queues[j].unsafe_size()), -1);
            }
            logger.info("---------------------------------------", -1);

            // define constants needed for network communication
            IParam* temp_dummy_par = p_gen->generate_parameter();
            const int unit_bytenum = temp_dummy_par->get_bytes();
            delete temp_dummy_par;

            // current send_queue size + current machine_queue_size + number of columns + columns
            const int msg_bytenum = sizeof(int) * 2 + unit_bytenum * option.units_per_msg_;

            long long local_send_count = 0;

            string timeouts_str = "Timeouts at: ";
            for (double ttt : option.timeouts_) {
                timeouts_str += to_string(ttt) + ",";
            }
            logger.info("Timeouts at: " + timeouts_str, -1);

            // save columns here, and push to job_queues again before next train starts
            vector<IParam *> saved_columns;

            char *send_message = new char[msg_bytenum];
            char *recv_message = new char[msg_bytenum];

            for (unsigned int main_timeout_iter = 0; main_timeout_iter < option.timeouts_.size(); main_timeout_iter++) {

                const double timeout = (main_timeout_iter == 0) ? option.timeouts_[0] :
                    option.timeouts_[main_timeout_iter] - option.timeouts_[main_timeout_iter - 1];

                /*******************************************************/
                /* Define Training Sender Thread                       */
                /*******************************************************/
                std::thread train_send_thread([&]() {
                    rng_type send_rng(rank * 17 + option.seed_ + option.num_threads_ + 2);
                    std::uniform_int_distribution<> target_dist(0, numtasks - 1);

                    while (flag_train_ready == false) {
                        std::this_thread::yield();
                    }

                    const tick_count start_time = tick_count::now();
                    int monitor_num = 0;

                    char *cur_pos = send_message + sizeof(int) * 2;
                    int cur_num = 0;

                    while (true) {
                        double elapsed_seconds = (tbb::tick_count::now() - start_time).seconds();
                        if (elapsed_seconds > timeout) {
                            break;
                        }

                        if (monitor_num < elapsed_seconds) {
                            logger.info("Train sender thread alive: sec = " + 
                                    to_string(monitor_num) + ", send Q size = " + 
                                    to_string(send_queue.unsafe_size()), -2);
                            monitor_num++;
                        }

                        IParam *p_col = nullptr;

                        if (send_queue.try_pop(p_col)) {
                            p_col->pack(cur_pos);

                            column_pool.push(p_col);

                            cur_pos += unit_bytenum;
                            cur_num++;

                            if (cur_num >= option.units_per_msg_) {
                                int machine_load = 0;
                                for (int i = 0; i < option.num_threads_; i++) {
                                    machine_load += job_queues[i].unsafe_size();
                                }
                                *(reinterpret_cast<int *>(send_message)) = machine_load;
                                *(reinterpret_cast<int *>(send_message) + 1) = cur_num;

                                local_send_count += cur_num;

                                /* choose destination */
                                int target_rank = std::distance(machine_queue_current_sizes, std::min_element(machine_queue_current_sizes, machine_queue_current_sizes + numtasks));

                                int rc = MPI_Ssend(send_message, msg_bytenum, MPI_CHAR, target_rank, 1, MPI_COMM_WORLD);

                                /* BUGBUG: in rank 0, arbitrary delay is added */
                                if (rank == 0 && option.rank0_delay_ > 0) {
                                    std::this_thread::sleep_for( tbb::tick_count::interval_t(option.rank0_delay_) );
                                }

                                if (rc != MPI_SUCCESS) {
                                    std::cerr << "SendTask MPI Error" << std::endl;
                                    exit(64);
                                }

                                cur_pos = send_message + sizeof(int) * 2;
                                cur_num = 0;
                            }
                        }
                        else {
                            std::this_thread::yield();
                        }
                    }

                    {
                        // send remaining columns to random machine
                        *(reinterpret_cast<int *>(send_message) + 1) = cur_num;
                        int target_rank = std::distance(machine_queue_current_sizes, std::min_element(machine_queue_current_sizes, machine_queue_current_sizes + numtasks));
                        int rc = MPI_Ssend(send_message, msg_bytenum, MPI_CHAR, target_rank, 1, MPI_COMM_WORLD);

                        local_send_count += cur_num;

                        if (rc != MPI_SUCCESS) {
                            std::cerr << "SendTask MPI Error" << std::endl;
                            exit(64);
                        }

                    }

                    // send dying message to every machine
                    *(reinterpret_cast<int *>(send_message) + 1) = -(rank + 1);
                    for (int i=0; i < numtasks; i++) {
                        int rc = MPI_Ssend(send_message, msg_bytenum, MPI_CHAR, i, 1, MPI_COMM_WORLD);

                        if (rc != MPI_SUCCESS) {
                            std::cerr << "SendTask MPI Error" << std::endl;
                            exit(64);
                        }
                    }

                    logger.info("send thread finishing", -2);

                    return;

                });
                /*******************************************************/
                /* End of Definition of Training Sender Thread         */
                /*******************************************************/


                // wait until every machine is ready
                MPI_Barrier(MPI_COMM_WORLD);

                /////////////////////////////////////////////////////////
                // Start Training
                /////////////////////////////////////////////////////////

                // now we are ready to train
                flag_train_ready = true;

                // do receiving
                {
                    const tick_count start_time = tick_count::now();
                    int monitor_num = 0;

                    int num_dead = 0;

                    MPI_Status status;

                    while (num_dead < numtasks) {

                        double elapsed_seconds = (tbb::tick_count::now() - start_time).seconds();

                        if (monitor_num < elapsed_seconds) {
                            logger.info("Receiver thread alive: sec = " + to_string(monitor_num), -4);
                            monitor_num++;
                        }

                        int rc = MPI_Recv(recv_message, msg_bytenum, MPI_CHAR, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);

                        if (rc != MPI_SUCCESS) {
                            logger.error("ReceiveTask MPI Error", -4);
                            exit(64);
                        }

                        int machine_load = *(reinterpret_cast<int *>(recv_message));
                        int num_received = *(reinterpret_cast<int *>(recv_message) + 1);

                        int source_rank = status.MPI_SOURCE;
                        machine_queue_current_sizes[source_rank] = machine_load;

                        // negative numbers are dying messages
                        if (num_received < 0) {
                            num_dead++;
                        } else {
                            char *cur_pos = recv_message + sizeof(int) * 2;

                            int smallest_job_queue_index = 0;
                            int smallest_job_queue_size = job_queues[0].unsafe_size();

                            for (int j = 1; j < option.num_threads_; j++) {
                                int job_queue_size = job_queues[j].unsafe_size();
                                if (smallest_job_queue_size > job_queue_size) {
                                    smallest_job_queue_size = job_queue_size;
                                    smallest_job_queue_index = j;
                                }
                            }

                            for (int i = 0; i < num_received; i++) {
                                IParam *p_col = column_pool.pop();
                                p_col->unpack(cur_pos);

                                job_queues[smallest_job_queue_index].push(p_col);

                                cur_pos += unit_bytenum;
                            }
                        }
                    }

                }

                train_send_thread.join();

                flag_train_stop = true;
                flag_train_ready = false;
                count_setup_threads = 0;

                // prepare for test
                {
                    // gather everything that is within the machine
                    vector<IParam *> local_columns;

                    int num_columns_prepared = 0;
                    int global_num_columns_prepared = 0;

                    while (global_num_columns_prepared < global_num_cols) {

                        for (int i = 0; i < option.num_threads_; i++) {
                            IParam *p_col;
                            while (job_queues[i].try_pop(p_col)) {
                                local_columns.push_back(p_col);
                                num_columns_prepared++;
                            }
                        }

                        {
                            IParam *p_col;
                            while (send_queue.try_pop(p_col)) {
                                local_columns.push_back(p_col);
                                num_columns_prepared++;
                            }
                        }

                        MPI_Allreduce(&num_columns_prepared, &global_num_columns_prepared, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);

                        if (rank == 0) {
                            logger.info("Num columns prepared for test: " + 
                                    to_string(global_num_columns_prepared) + 
                                    " / " + to_string(global_num_cols), -1);
                        }

                    }

                    for (IParam *p_col : local_columns) {
                        p_col->flag = 0;
                        job_queues[0].push(p_col);
                    }

                }

                // wait until every machine is ready
                MPI_Barrier(MPI_COMM_WORLD);

                // now start actual computation
                flag_test_ready = true;

                /*******************************************************/
                /* Start Testing Thread                                */
                /*******************************************************/
                std::thread test_send_thread([&]() {

                    const long mask = (1L << rank);

                    const tick_count start_time = tick_count::now();
                    int monitor_num = 0;

                    char *cur_pos = send_message + sizeof(int);
                    int cur_num = 0;

                    int send_count = 0;

                    int target_rank = rank + 1;
                    target_rank %= numtasks;

                    while (send_count < global_num_cols) {
                        double elapsed_seconds = (tbb::tick_count::now() - start_time).seconds();
                        if (monitor_num < elapsed_seconds) {
                            logger.info("Test sender thread alive: sec = " + to_string(monitor_num), -3);
                            monitor_num++;
                        }

                        IParam *p_col;

                        if (send_queue.try_pop(p_col)) {

                            /* if the column was not already processed */
                            if ((p_col->flag & mask) == 0) {

                                (p_col)->flag |= mask;
                                p_col->pack(cur_pos);

                                cur_pos += unit_bytenum;
                                cur_num++;

                                send_count++;

                                if (cur_num >= option.units_per_msg_) {

                                    *(reinterpret_cast<int *>(send_message)) = cur_num;

                                    /* choose destination */
                                    int rc = MPI_Ssend(send_message, msg_bytenum, MPI_CHAR, target_rank, 1, MPI_COMM_WORLD);

                                    if (rc != MPI_SUCCESS) {
                                        std::cerr << "SendTask MPI Error" << std::endl;
                                        exit(64);
                                    }

                                    cur_pos = send_message + sizeof(int);
                                    cur_num = 0;
                                }

                            }
                            else {
                                logger.warn("!!! should not happen! flag:" + to_string(p_col->flag) + "???", -3);
                            }

                            column_pool.push(p_col);
                        }
                        else {

                            /* even if pop was failed, if there is remaining message send it to another machine */
                            if (cur_num > 0) {

                                *(reinterpret_cast<int *>(send_message)) = cur_num;

                                /* choose destination */
                                int rc = MPI_Ssend(send_message, msg_bytenum, MPI_CHAR, target_rank, 1, MPI_COMM_WORLD);

                                if (rc != MPI_SUCCESS) {
                                    std::cerr << "SendTask MPI Error" << std::endl;
                                    exit(64);
                                }

                                cur_pos = send_message + sizeof(int);
                                cur_num = 0;

                            }
                            else {
                                std::this_thread::yield();
                            }
                        }

                    }

                    if (cur_num > 0) {
                        // send remaining columns to designated machine
                        *(reinterpret_cast<int *>(send_message)) = cur_num;
                        int rc = MPI_Ssend(send_message, msg_bytenum, MPI_CHAR, target_rank, 1, MPI_COMM_WORLD);

                        if (rc != MPI_SUCCESS) {
                            std::cerr << "SendTask MPI Error" << std::endl;
                            exit(64);
                        }
                    }

                    logger.info("test send thread finishing.", -3);

                    return;

                });
                /*******************************************************/
                /* End of Testing Thread                               */
                /*******************************************************/

                // receive columns for testing
                {
                    const tick_count start_time = tick_count::now();
                    int monitor_num = 0;

                    int recv_count = 0;

                    MPI_Status status;

                    const long mask = (1L << rank);

                    while (recv_count < global_num_cols) {

                        double elapsed_seconds = (tbb::tick_count::now() - start_time).seconds();

                        if (monitor_num < elapsed_seconds) {
                            logger.info("Receiver thread alive: sec = " + to_string(monitor_num), -5);
                            monitor_num++;
                        }

                        int rc = MPI_Recv(recv_message, msg_bytenum, MPI_CHAR, MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);

                        if (rc != MPI_SUCCESS) {
                            std::cerr << "ReceiveTask MPI Error" << std::endl;
                            exit(64);
                        }

                        int num_received = *(reinterpret_cast<int *>(recv_message));
                        // negative numbers are dying messages
                        char *cur_pos = recv_message + sizeof(int);
                        for (int i = 0; i < num_received; i++) {

                            IParam *p_col = column_pool.pop();
                            p_col->unpack(cur_pos);

                            if ((mask & p_col->flag) == 0) {
                                job_queues[0].push(p_col);
                            } else {
                                // discard the column
                                saved_columns.push_back(p_col);
                            }

                            cur_pos += unit_bytenum;

                            recv_count++;

                        }

                    }

                    logger.info("test receive done.", -5);
                }

                test_send_thread.join();

                // test done
                flag_test_stop = true;

                logger.info("waiting to join with updaters.", -1);

                while (count_setup_threads < option.num_threads_) {
                    std::this_thread::yield();
                }

                //Statistics:
                long long machine_num_updates = 0; 
                for (int i = 0; i < option.num_threads_; i++) {
                    machine_num_updates += num_updates[i];
                }
                logger.info("machine_num_updates: " + to_string(machine_num_updates), -1);

                long long machine_num_failures = 0;
                for (int i = 0; i < option.num_threads_; i++) {
                    machine_num_failures += num_failures[i];
                }
                logger.info("machine_num_failures: " + to_string(machine_num_failures), -1);

                long long global_num_updates = 0;
                MPI_Allreduce(&machine_num_updates, &global_num_updates, 1, MPI_LONG_LONG, MPI_SUM, MPI_COMM_WORLD);

                long long global_num_failures = 0;
                MPI_Allreduce(&machine_num_failures, &global_num_failures, 1, MPI_LONG_LONG, MPI_SUM, MPI_COMM_WORLD);

                long long global_send_count = 0;
                MPI_Allreduce(&local_send_count, &global_send_count, 1, MPI_LONG_LONG, MPI_SUM, MPI_COMM_WORLD);

                int machine_col_empty = 0;
#pragma simd
                for (int i = 0; i < get_num_cols(); i++) {
                    machine_col_empty = machine_col_empty + (is_column_empty[i] ? 1 : 0);
                }

                int global_col_empty = 0;
                MPI_Allreduce(&machine_col_empty, &global_col_empty, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);


                MPI_Barrier(MPI_COMM_WORLD);

                if (rank == 0) {
                    cout << "=========================OVER ALL STATISTICS============================" << endl;
                    cout << "elapsed time: " << option.timeouts_[main_timeout_iter] << endl;
                    cout << "stats-header,num_tasks,num_threads,time,num_updates,num_failures,num_empty_columns,send_count" <<endl;
                    cout << "stats," << numtasks << "," << option.num_threads_ << ","
                        << option.timeouts_[main_timeout_iter] << "," << global_num_updates << ","
                        << global_num_failures << "," << global_col_empty << "," << global_send_count << endl;
                    cout << "===========================CUSTOM STATISTICS=============================" << endl;
                }



                //Custom Statistics:
                compute_statistics(saved_columns, main_timeout_iter, option.timeouts_[main_timeout_iter]);
                if(rank == 0){
                    cout << "=============================END STATISTICS==============================" << endl;
                }

                if (option.flag_pause_) {
                    std::this_thread::sleep_for(tbb::tick_count::interval_t(5.0));
                }

                // initialize state variables
                flag_train_ready = false;
                flag_train_stop = false;
                flag_test_ready = false;
                flag_test_stop = false;

                // BUGBUG: saved_columns: do initialization and push to job queue again
                for (IParam *p_col : saved_columns) {
                    p_col->flag = 0;
                    p_col->reset_visits();

                    int rand_thread_index = rand_dist(rng_temp);

                    // push to the job queue
                    job_queues[rand_thread_index].push(p_col);
                }

                // shihao
                logger.info("--------job_queue_current_sizes--------", -1);
                for (int j = 0; j < option.num_threads_; j++) {
                    logger.info("Queue: " + to_string(j) + ", Size: " + to_string(job_queues[j].unsafe_size()), -1);
                }
                logger.info("---------------------------------------", -1);

                // if at the last iteration, do not clear this thing to print out to file
                if (main_timeout_iter < option.timeouts_.size() - 1) {
                    saved_columns.clear();
                }

            }  // end of timeout loop

            logger.info("Waiting for updater threads to join", -1);
            for (int i = 0; i < option.num_threads_; i++) {
                updater_threads[i].join();
            }

            MPI_Barrier(MPI_COMM_WORLD);

            finalize(saved_columns);

            for (int i = 0; i < option.num_threads_; i++) {
                updaters[i]->finalize();
            }

            logger.info("All done, now free memory", -1);
            delete[] job_queues;
            delete[] updater_threads;
            delete[] send_message;
            delete[] recv_message;
            delete[] num_updates;
            delete[] num_failures;
            delete[] is_column_empty;
            delete[] machine_queue_current_sizes;

            MPI_Finalize();

            return 0;

        }

};

#endif
