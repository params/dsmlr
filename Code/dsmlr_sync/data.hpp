/*
 * Copyright (c) 2019 Parameswaran Raman, Shin Matsushima, 
 * Hyokun Yun, S.V.N Vishwanathan
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
 */

#ifndef DATA_HPP_
#define DATA_HPP_

#include <iostream>
#include <fstream>
#include <boost/tokenizer.hpp>

typedef int label_t;
typedef int idx_t;
typedef double val_t;


namespace dsmlr{
  
  typedef struct{
    size_t id_;
    label_t label_;
    vector<idx_t> idx_;
    vector<val_t> val_;
  } Instance;
  
  class Data{

  private:

    size_t global_num_classes_ = 0;
    size_t local_num_classes_ = 0;
    
    size_t global_train_num_instances_ = 0;
    size_t local_train_num_instances_ = 0;
    
    size_t global_test_num_instances_ = 0;
    size_t local_test_num_instances_ = 0;
    
    size_t global_num_features_ = 0;
    
    vector<size_t> local_train_num_nnz_col_;
    vector<size_t> global_train_num_nnz_col_;
    
    vector<size_t> local_train_instances_index_;
    
    
    void read_data(const string& data_file, size_t min_instance_index, bool train=true){
      const int numtasks = Environment::get_instance().get_numtasks();

      std::ifstream ifs;
      ifs.open(data_file, std::ifstream::in);        
      string line;

      // read the only part each process need.
      for(size_t instance_index=0; instance_index< min_instance_index; ++instance_index){
        getline(ifs, line);
      }
        
      for(size_t instance_index=min_instance_index; ; ++instance_index){
          
        if(!getline(ifs, line))            
          break;
        if(instance_index % numtasks != min_instance_index)
          continue;          
          
        boost::char_separator<char> sep(" \t:");
        boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
        size_t count = 0;
        size_t local_instance_index=instance_index/numtasks;

        if(train)
          local_train_instances_[local_instance_index].id_ = instance_index;
        else
          local_test_instances_[local_instance_index].id_ = instance_index;
        for(auto beg=tokens.begin(); beg!=tokens.end(); ++beg){
          const auto& t = *beg;
          count++;
          if(count == 1){
            size_t yi = stoul(t);
            if(train){
              local_train_instances_[local_instance_index].label_= (label_t) yi ;
            }else{
              local_test_instances_[local_instance_index].label_= (label_t) yi ;
            }
            continue;
          }
          if(count%2){
            if(train){
              local_train_instances_[local_instance_index].val_.push_back(stod(t));
            }else{
              local_test_instances_[local_instance_index].val_.push_back(stod(t));
            }
          }else{
            unsigned long idx = stoul(t); // assume that it begins with 1
            if(train){
              local_train_instances_[local_instance_index].idx_.push_back(idx-1);
              local_train_num_nnz_col_[idx-1]++;
            }else{
              local_test_instances_[local_instance_index].idx_.push_back(idx-1);
            }              
          }            
        }
      }
        
      ifs.close();
      if(train)
        print_log(format("reading train finished. acquired number of instances %d") % local_train_instances_.size());
      else
        print_log(format("reading test finished. acquired number of instances %d") % local_test_instances_.size());
      return;
    }
    
  public:
    // meta information for data    
    Data(){ }
    
    ~Data(){
      local_train_num_nnz_col_.clear();
      global_train_num_nnz_col_.clear();
      local_train_instances_index_.clear();
      local_train_instances_.clear();
      local_test_instances_.clear();
    }
    
    vector<Instance> local_train_instances_;
    vector<Instance> local_test_instances_;

    // svnvish: BUGBUG
    // deprecate this routine
    vector<size_t>* get_nnz_col(){
      return &global_train_num_nnz_col_;
    }

    size_t get_omega_j(const size_t j){
      return global_train_num_nnz_col_[j];
    }
    
    size_t get_global_num_classes(){
      return global_num_classes_;
    }
    
    size_t get_local_num_classes(){
      return local_num_classes_;
    }
    
    size_t get_global_num_features(){
      return global_num_features_;
    }
    
    size_t get_global_train_num_instances(){
      return global_train_num_instances_;
    }
    
    size_t get_local_train_num_instances(){
      return local_train_num_instances_;
    }
    
    size_t get_global_test_num_instances(){
      return global_test_num_instances_;
    }
    
    size_t get_local_test_num_instances(){
      return local_test_num_instances_;
    }
    
    bool have_test(){
      return (get_local_test_num_instances() != 0);
    }

    
    void shuffle_train_instances_order(){
      std::shuffle(local_train_instances_index_.begin(),
                   local_train_instances_index_.end(),
                   Environment::get_instance().get_rng());
    }

    bool apply_to_train_block(size_t block_index,
                              std::function<bool(Instance &)> func){
      for(size_t& instance_index : local_train_instances_index_){
        if(!func(local_train_instances_[instance_index])){
          return false;
        }
      }
      return true;
    }
    
    bool apply_to_refresh_block(size_t block_index,
                                std::function<bool(Instance &)> func){
      for(Instance& instance : local_train_instances_){
        if(!func(instance)){
          return false;
        }
      }
      return true;
    }
      
    bool apply_to_test_block(size_t block_index,
                             std::function<bool(Instance &)> func){
      for(Instance& instance : local_test_instances_){
        if(!func(instance)){
          return false;
        }
      }
      return true;
    }

	long get_num_from_line(std::ifstream& mfs){
	  string line;
	  getline(mfs, line);
	  return stoul(line);
	}



    /** 
     * Read data from file
     * 
     * @return true if succeed, false otherwise
     */
    template <typename file_itype>
    bool read(string train_file_name, 
	      string test_file_name,
	      string meta_file_name){

      auto& env = Environment::get_instance();
      const int numtasks = env.get_numtasks();
      const int rank = env.get_rank();
       
  std::ifstream mfs;
  mfs.open(meta_file_name, std::ifstream::in);        
  if(mfs.good()){
    print_log(format("Reading from meta_file: %s") % meta_file_name);

    global_train_num_instances_ = get_num_from_line(mfs);
    global_test_num_instances_ = get_num_from_line(mfs);
    global_num_features_ = get_num_from_line(mfs);
    global_num_classes_ = get_num_from_line(mfs);
    string nnz;
    getline(mfs, nnz);
    boost::char_separator<char> sep(" \t:");
    boost::tokenizer<boost::char_separator<char>> tokens(nnz, sep);
    size_t count = 0;
    global_train_num_nnz_col_.resize(global_num_features_);
    size_t col_index = 0;
    for(const auto& t : tokens){
      global_train_num_nnz_col_[col_index++] = stoul(t);
    }
      master_log(format("global # classes: %d") % global_num_classes_);
      master_log(format("global # training instances: %d") % global_train_num_instances_);
      master_log(format("global # features: %d") % global_num_features_);
    
    if(!test_file_name.size()==0){
      master_log(format("global # test instances: %d") % global_test_num_instances_);
    }
 
    mfs.close();  
  }
  mfs.close();  


// COMMENT OUT THE PART BELOW  
/*      master_log("First read of training and test file");
        
      // Do the first sweep to gather meta information about the dataset
      std::ifstream ifs;
      ifs.open(train_file_name, std::ifstream::in);        
      string line;
      while(getline(ifs, line)){
        boost::char_separator<char> sep(" \t:");
        boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
        size_t count = 0;
        for(const auto& t : tokens){
          count++;
          if(count == 1){
            size_t yi = stoul(t);
            if(yi > global_num_classes_)
              global_num_classes_ = yi;
            continue;
          }
          if(count%2){
            ;
          }else{
            unsigned long idx = stoul(t);
            if(idx > global_num_features_){
              global_num_features_ = idx;
            }
          }
        }
        global_train_num_instances_++;
      }
      ifs.close();

      if(test_file_name.size()==0){
        master_log("No test data is specified.");
      }else{
        
        master_log(format("Test data is specified :%s.") % test_file_name );
        // Do the first sweep to gather meta information about the dataset
        ifs.open(test_file_name, std::ifstream::in);        
        while(getline(ifs, line)){
          boost::char_separator<char> sep(" \t:");
          boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
          size_t count = 0;
          for(const auto& t : tokens){
            count++;
            if(count == 1){
              size_t yi = stoul(t);
              if(yi > global_num_classes_)
                global_num_classes_ = yi;
              continue;
            }
            if(count%2){
              ;
            }else{
              unsigned long idx = stoul(t);
              if(idx > global_num_features_){
                global_num_features_ = idx;
              }
            }
          }
          global_test_num_instances_++;
        }
        ifs.close();
        master_log(format("global # test instances: %d") % global_test_num_instances_);
      }

      global_num_classes_++; // if class begins with 0.
      
      
      master_log(format("global # classes: %d") % global_num_classes_);
      master_log(format("global # training instances: %d") % global_train_num_instances_);
      master_log(format("global # features: %d") % global_num_features_);
*/
      local_train_num_instances_ = global_train_num_instances_ / numtasks;
      if(global_train_num_instances_ % numtasks > rank )
        local_train_num_instances_++;
      size_t min_instance_index = rank;
      print_log(format("local # of instances: %d (start with %d and obtain every %d lines ")
                % local_train_num_instances_ % min_instance_index % numtasks );
      local_train_instances_.resize(local_train_num_instances_);
      local_train_instances_index_.resize(local_train_num_instances_);
      iota(local_train_instances_index_.begin(), local_train_instances_index_.end(), 0);
      local_num_classes_ = num_per_task(global_num_classes_, numtasks);
      master_log(format("local # of classes: %d ") % local_num_classes_);

      local_train_num_nnz_col_.resize(global_num_features_);
        
      read_data(train_file_name, min_instance_index);

      if(test_file_name.size()!=0){

        local_test_num_instances_ = global_test_num_instances_ / numtasks;
        if(global_test_num_instances_ % numtasks > rank)
          local_test_num_instances_++;          
        print_log(format("local # of instances for test: %d (start with %d and obtain every %d lines)")
                  % local_test_num_instances_ % min_instance_index % numtasks);
        
        local_test_instances_.resize(local_test_num_instances_);
        
        read_data(test_file_name, min_instance_index, /*train=*/ false);
        
      }
      global_train_num_nnz_col_.resize(global_num_features_);
      MPI_Allreduce(&local_train_num_nnz_col_[0], &global_train_num_nnz_col_[0], global_num_features_,
                    MPI_SIZE_T, MPI_SUM, MPI_COMM_WORLD);
      local_train_num_nnz_col_.clear();
      return true;
    
    }
      

  };

  
}

#endif
