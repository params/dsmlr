
How to run DSMLR Async:
======================
$ ./dsmlr_double --help
dsmlr options:
  -h [ --help ]                      produce help message
  --nthreads arg (=4)                number of threads to use (0: automatic)
  -s [ --seed ] arg (=12345)         seed value of random number generator
  -t [ --timeout ] arg (=10.0)       timeout seconds until completion
  -p [ --ptoken ] arg (=1024)        number of tokens in the pipeline
  --reuse arg (=1)                   probability of reusing a column in a node
  --debug arg (=0)                   print debug statements.
  --info arg (=1)                    print info statements.
  --pause arg (=0)                   wait for 5 seconds before going to next
                                     iteration.
  --mempool arg (=-1)                Size of memory pool. Dont set this if you
                                     dont want any cap on memory. This is a
                                     multiplier for global number of elements.
  --r0delay arg (=0)                 arbitrary network delay added to
                                     communication of rank 0 machine
  --packet arg (=100)                number of units per message
  --output arg                       path of the file the result will be
                                     printed into
  --max_iter arg (=100)              maximum number of iterations
  --eval_period arg (=1)             frequency of evaluation on test data
  -l [ --lambda ] arg (=1)           regularization parameter
  -e [ --eta ] arg (=0.001)          learning rate
  --drate arg (=0.10000000000000001) decay rate
  --solver arg (=1)                  solver type (1=exact A, 2=w latest, 3=w
                                     latest smart)
  --smart_init arg (=0)              enable smart initialization?
  --inner_repeat arg (=1)            How many rounds of SGD per inner epoch
  --bs_period arg (=1)               frequency of bulk synchronization
  --shuffle arg (=1)                 shuffle local blocks of data?
  --train arg                        file name of training data txt files
  --test arg                         file name of testing data txt files
  --meta arg                         file name of meta data txt files
  --chunk_size arg (=100)            how many parameters to communicate per MPI
                                     send/receive call
  --output_local arg                 path of the file the local params will be
                                     printed into
  --output_pred arg                  path of the file the predicted rank will
                                     be printed in

Examples:
========

$ mpirun -np 2 ./dsmlr_double --train ../../Data/CLEF/train2.txt --test
../../Data/CLEF/test2.txt --meta ../../Data/CLEF/meta.txt --nthreads 2 -t 1000 -l 0.0001 -e 100|grep grep
